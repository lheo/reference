# -*- coding: utf-8 -*-
<%inherit file="../site.mako"/>

<!-- Main container -->
<div class="page-container">

    <!-- ScrollToTop Button -->
    <a class="bloc-button btn btn-d scrollToTop" onclick="scrollToTarget('1')">
        <span class="fa fa-chevron-up"></span>
    </a>
    <!-- ScrollToTop Button END-->

    <!-- bloc-12 -->
    <div class="bs-docs-header pro-bloc" id="content" tabindex="-1">
        <div class="container">
            <h1 class="mg-md text-center tc-white titre-glossaire">
                Glossaire Lhéo
            </h1>

            <p class="text-left text-glossaire">
                Ce glossaire constitue une aide à l'utilisation du Langage Harmonisé
                d'Échange d'informations sur l'Offre de formation (Lhéo),
                en définissant les termes du champ de la formation utilisés dans
                le langage.

                Certaines d’entre elles relèvent des normes AFNOR sur la
                Terminologie de la Formation Professionnelle
                (<a class="l-violetbg" href="#NFX50-750">NFX 50-750</a>,
                <a class="l-violetbg" href="#FDX50-751">FDX 50-751</a> et
                <a class="l-violetbg" href="#NFX50-760">NFX 50-760</a>).
            </p>

            <p class="text-left text-glossaire">Révision de mars 2016</p>

            <div id="pro-pix">
                <img src="/static/img/backgrounds/pro.png">
            </div>
        </div>
    </div>
    <!-- bloc-12 END -->

    <div class="container">
        <div class="row glossaire-row">
            <div class="col-md-1">
                <div data-spy="affix" data-offset-top="550" data-offset-bottom="555"
                     class="bs-sidebar hidden-print well affix-top side-menu glossaire-menu"
                     id="doc-navmenu"
                     role="complementary">
                    <ul class="nav bs-sidenav text-center">
                        <li class="active"><a href="#acquis">A</a></li>
                        <li class=""><a class="" href="#bilan-competences">B</a></li>
                        <li class=""><a class="" href="#capacite">C</a></li>
                        <li class=""><a class="" href="#date-limite-inscription">D</a></li>
                        <li class=""><a class="" href="#es-dates-fixes">E</a></li>
                        <li class=""><a class="" href="#financement-formation">F</a></li>
                        <li class=""><a class="" href="#individualisation-formation">I</a></li>
                        <li class=""><a class="" href="#lieu-formation">L</a></li>
                        <li class=""><a class="" href="#methode-pedagogique">M</a></li>
                        <li class=""><a class="" href="#niveaux">N</a></li>
                        <li class=""><a class="" href="#objectif-formation">O</a></li>
                        <li class=""><a class="" href="#parcours-formation-individualise">P</a></li>
                        <li class=""><a class="" href="#raison-sociale-formateur">R</a></li>
                        <li class=""><a class="" href="#sequence-formation">S</a></li>
                        <li class=""><a class="" href="#titre-professionnel">T</a></li>
                        <li class=""><a class="" href="#validation-acquis">V</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-11 l-bloc bgc-white tc-yale-blue" role="main" id="bloc-13">

                ~generated_content~

                <!-- bloc-26 -->
                <div id="bloc-26">
                    <div class="glossaire-container">
                        <div class="glossaire-single-box">
                            <h1 class="mg-md  tc-dark-slate-blue">
                                Annexes du glossaire
                            </h1>

                            <a class="no-pointer" name="annexeI" id="annexeI">
                                <h2 class="mg-md  tc-olive-drab-7">
                                    Liste des financeurs potentiels
                                </h2>
                            </a>
                            <ul class="list-unstyled">
                                <li>
                                    <h3 class=" mg-sm">
                                        <span class="ion ion-location"></span> AGEFIPH - Association nationale de
                                        GEstion du
                                        Fonds pour l'Insertion Professionnelle des personnes Handicapées
                                    </h3>
                                </li>
                                <li>
                                    <h3 class=" mg-sm">
                                        <span class="ion ion-location"></span> Entreprise
                                    </h3>
                                </li>
                                <li>
                                    <h3 class=" mg-sm">
                                        <span class="ion ion-location"></span> État
                                    </h3>
                                </li>
                                <li>
                                    <h3 class=" mg-sm">
                                        <span class="ion ion-location"></span> Conseil régional
                                    </h3>
                                </li>
                                <li>
                                    <h3 class=" mg-sm">
                                        <span class="ion ion-location"></span> Conseil général
                                    </h3>
                                </li>
                                <li>
                                    <h3 class=" mg-sm">
                                        <span class="ion ion-location"></span> Collectivité locale
                                    </h3>
                                </li>
                                <li>
                                    <h3 class=" mg-sm">
                                        <span class="ion ion-location"></span> FSE - Fond Social Européen
                                    </h3>
                                </li>
                                <li>
                                    <h3 class=" mg-sm">
                                        <span class="ion ion-location"></span> Pôle emploi
                                    </h3>
                                </li>
                                <li>
                                    <h3 class=" mg-sm">
                                        <span class="ion ion-location"></span> OPCA - Organisme Paritaire Collecteur
                                        Agréé
                                    </h3>
                                </li>
                                <li>
                                    <h3 class=" mg-sm">
                                        <span class="ion ion-location"></span> Autres financeurs privés
                                    </h3>

                                    <h3 class=" mg-sm">
                                        <span class="ion ion-location"></span> Autres financeurs publics
                                    </h3>
                                </li>
                                <li>
                                    <h3 class=" mg-sm">
                                        <span class="ion ion-location"></span> Autres financeurs privés
                                    </h3>
                                </li>
                                <li>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- bloc-26 END -->

                <!-- bloc-27 -->
                <div class="l-bloc bgc-white" id="bloc-27">
                    <div class="glossaire-container">
                        <div class="glossaire-single-box">
                            <a class="no-pointer" name="bibliographie" id="bibliographie">
                                <h2 class="mg-md  tc-olive-drab-7">
                                    Bibliographie
                                </h2>
                            </a>

                            <a class="no-pointer" name="NFX50-750" id="NFX50-750"></a>
                            <a class="no-pointer" name="FDX50-751" id="FDX50-751"></a>
                            <a class="no-pointer" name="NFX50-760" id="NFX50-760"></a>

                            <h3 class="mg-md">
                                <span class="ion ion-bookmark"></span>
                                AFNOR - Norme NF X 50-750 : formation professionnelle,
                                terminologie, août 2015.
                            </h3>

                            <h3 class="mg-md">
                                <span class="ion ion-bookmark"></span>
                                AFNOR - Norme FD X 50-751 : formation professionnelle,
                                terminologie, fascicule explicatif, juillet 1996.
                            </h3>

                            <h3 class="mg-md">
                                <span class="ion ion-bookmark"></span>
                                AFNOR - Norme NF X 50-760 : formation professionnelle
                                - Les informations essentielles sur l'offre de formation
                                - Lisibilité de l'offre de formation, décembre 2013.
                            </h3>
                        </div>
                    </div>
                </div>
                <!-- bloc-27 END -->

            </div>
        </div>
    </div>
</div>
<!-- Main container END -->
