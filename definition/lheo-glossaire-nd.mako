# -*- coding: utf-8 -*-

<%inherit file="../site.mako"/>\

<!-- Main container -->
<div class="page-container">

    <button type="button" class="btn btn-default" aria-label="Left Align">
        <span class="glyphicon glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
    </button>

    <!-- bloc-12 -->
    <div class="bloc bgc-ube d-bloc tc-white" id="bloc-12">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <h1 class=" mg-md text-center tc-white">
                        Glossaire Lhéo
                    </h1>

                    <p class=" text-center">
                        Ce glossaire constitue une aide à l'utilisation de la fiche de présentation du référentiel
                        d'homogénéisation de l'information sur l'offre de formation. Vous retrouverez les termes
                        présents dans cette fiche et les définitions s'y rapportant. Certaines d&rsquo;entre elles
                        relèvent des normes AFNOR sur la Terminologie de la Formation Professionnelle (<a
                            href="#NFX50-750">NFX 50-750</a> et <a href="#NFX50-751">NFX
                        50-751</a>).
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-12 END -->

    <!-- bloc-13 -->
    <div class="bloc l-bloc bgc-white tc-yale-blue" id="bloc-13">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-4">
                    <a class="no-pointer" name="acquis">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Acquis
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Ensemble des savoirs et savoir-faire dont une personne manifeste la maîtrise dans une activité
                        professionnelle, sociale ou de formation (<a href="NFX50-750">NFX 50-750</a>).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="action">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Action de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        S'entend de tout ce qui contribue à la conception, à l&rsquo;organisation, et à la mise en œuvre
                        d&rsquo;une formation. Voir aussi <a href="#module-formation">Module de formation</a>.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="agrement">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Agrément au titre de la rémunération
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        L'article <a class="ltc-dark-slate-blue"
                                     href="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006904370&cidTexte=LEGITEXT000006072050&dateTexte=20151001"
                                     target="_blank">L6341-4</a> du code du travail prévoit l'agrément des stages de
                        formation par l'État (le préfet) ou les Régions (président du conseil régional) pour assurer la
                        rémunération de stagiaires demandeurs d'emploi non indemnisés et remplissant les conditions
                        d'accès à la rémunération prévue par le code du travail.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-12">
                    <a class="no-pointer" name="action-conventionnee">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Action de formation conventionnée
                        </h3>
                    </a>
                    <h4 class=" mg-lg tc-olive-drab-7 text-left">
                        Une action de formation au sens de l'article <a
                            href="http://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006904130"
                            data-placement="top" data-toggle="tooltip" title="Voir l'article"
                            target="_blank">L6313-1</a> à <a
                            href="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006904141&cidTexte=LEGITEXT000006072050&dateTexte=20151001"
                            data-placement="top" data-toggle="tooltip" title="Voir l'article"
                            target="_blank">L6313-11</a> du code du travail est dite conventionnée lorsqu'elle fait
                        l'objet d'une convention de formation professionnelle conforme aux dispositions des articles <a
                            href="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000028697984&cidTexte=LEGITEXT000006072050&dateTexte=20151001"
                            data-placement="top" data-toggle="tooltip" title="Voir l'article"
                            target="_blank">L6353-1</a> <a
                            href="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000021343606&cidTexte=LEGITEXT000006072050&dateTexte=20151001"
                            data-placement="top" data-toggle="tooltip" title="Voir l'article"
                            target="_blank">L6353-2</a> et <a
                            href="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000018522260&cidTexte=LEGITEXT000006072050&dateTexte=20151001"
                            data-placement="top" data-toggle="tooltip" title="Voir l'article"
                            target="_blank">R6353-1</a> (Etat, Régions et autres collectivités) ou <a
                            href="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000021342998&cidTexte=LEGITEXT000006072050&dateTexte=20091126"
                            data-placement="top" data-toggle="tooltip" title="Voir l'article"
                            target="_blank">L6331-21</a> (entreprises) du même code. Ces conventions fixent les
                        contenus, les modalités de réalisation ainsi que les objectifs à atteindre en contrepartie d'une
                        contribution financière à l'action de formation : directe s'il s'agit d'un achat dans le respect
                        des règles du code des marchés publics en vigueur, indirecte s'il s'agit de subventions de
                        fonctionnement consenties à l'organisme pour mettre en œuvre cette action. Il est enfin rappelé
                        que les personnes physiques qui entreprennent des formations à leurs frais doivent conclure un
                        contrat de formation professionnelle avec l'organisme dispensateur, conforme - à peine de
                        nullité - aux exigences des articles <a
                            href="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000021342990&cidTexte=LEGITEXT000006072050&dateTexte=20151001"
                            data-placement="top" data-toggle="tooltip" title="Voir l'article"
                            target="_blank">L6353-3</a> à <a
                            href="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006904417&cidTexte=LEGITEXT000006072050&dateTexte=20151001"
                            data-placement="top" data-toggle="tooltip" title="Voir l'article"
                            target="_blank">L6353-7</a> du code du travail.
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-13 END -->

    <!-- bloc-14 -->
    <div class="bloc bgc-anti-flash-white l-bloc" id="bloc-14">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-4">
                    <a class="no-pointer" name="age">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Age
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Il s'agit des conditions administratives d'âge requises pour pouvoir accéder à une action de
                        formation donnée.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="aptitude">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Aptitude
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Capacité supposée à exercer une activité (tâche à accomplir, emploi à occuper, connaissance à
                        acquérir). La reconnaissance juridique de l'aptitude (certificat d'aptitude, liste
                        d'aptitude...) ouvre l'accès à certains droits (emploi, formation...) (<a href="#NFX 50-751">NFX
                        50-751</a>).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="attestation-acquis">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Attestation des acquis
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Document délivré au stagiaire par les dispensateurs de formation, reconnaissant l'acquisition de
                        capacités à l'issue de la formation (<a href="#NFX 50-751">NFX 50-751</a>).
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="attestation-presence">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Attestation de présence
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Document écrit, à usage administratif, remis au commanditaire. Il certifie la présence de
                        l'apprenant (ou des apprenants) à une formation (<a href="#NFX 50-750">NFX 50-750</a>).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="attestation-stage">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Attestation de fin de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Document écrit, remis à l'apprenant, qui certifie sa participation à une formation et précise
                        les objectifs, la nature et la durée de la formation et le cas échéant les résultats de l&rsquo;évaluation
                        des acquis (article <a
                            href="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000021342995&cidTexte=LEGITEXT000006072050&dateTexte=20091126"
                            data-placement="top" data-toggle="tooltip" title="Voir l'article"
                            target="_blank">L.6353-1</a> alinéa 2 du code du travail) (<a href="#NFX 50-750">NFX
                        50-750</a>).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="bilan-competences">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Bilan de compétences
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Action permettant à un travailleur d'analyser ses compétences professionnelles ainsi que ses
                        aptitudes et ses motivations afin de définir un projet professionnel et, le cas échéant, un
                        projet de formation (articles <a
                            href="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000019870465&cidTexte=LEGITEXT000006072050&dateTexte=20081205"
                            data-placement="top" data-toggle="tooltip" title="Voir l'article"
                            target="_blank">L6313-1</a> et <a
                            href="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006904140&cidTexte=LEGITEXT000006072050&dateTexte=20151108"
                            data-placement="top" data-toggle="tooltip" title="Voir l'article"
                            target="_blank">L6313-10</a> du code du travail). Voir
                        aussi <a href="#action">Action de formation</a>.
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-14 END -->

    <!-- bloc-15 -->
    <div class="bloc l-bloc bgc-white" id="bloc-15">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-4">
                    <a class="no-pointer" name="capacite">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Capacité
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Ensemble de dispositions et d'acquis, constatés chez un apprenant, généralement formulés par
                        l'expression : être capable de... (<a href="#NFX 50-750">NFX 50-750</a>)
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="certificat-formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Certificat de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Document écrit, délivré par le dispensateur de formation ou une autorité de référence,
                        reconnaissant au titulaire un niveau de capacité vérifié par un contrôle (<a href="#NFX 50-750">NFX
                        50-750</a>).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="certification">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Certification
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Procédure définissant les conditions de délivrance d'un certificat qui valide les acquis d'une
                        formation.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="certification-acquis">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Certification des acquis de la formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Procédure définissant les conditions de délivrance d'un certificat qui valide les acquis d'une
                        formation.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="certification-pro">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Certification professionnelle
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Procédure visant à organiser les modalités de reconnaissance des qualifications et des acquis
                        professionnels nécessaires à l'exercice d'une profession.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="competence-pro">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Compétence professionnelle
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Mise en œuvre, en situation professionnelle, de capacités qui permettent d'exercer
                        convenablement une fonction ou une activité (<a href="#NFX 50-750">NFX 50-750</a>).
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="conditions-pedagogiques">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Conditions pédagogiques
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Conditions pédagogiques du déroulement de la formation. Voir <a href="#organisation-formation">Organisation
                        de la formation</a>,
                        <a href="#methode-pedagogique">Méthode pédagogique</a>, <a href="#moyen-pedagogique">Moyen
                        pédagogique</a>, <a href="#support-pedagogique">Support pédagogique</a>.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="conditions-specifiques">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Conditions spécifiques
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Cet élément indique les conditions spécifiques d'accès à la formation, les aptitudes requises,
                        une tranche d'âge, etc. Voir aussi <a href="#aptitude">Aptitude</a>, <a href="#age">Âge</a>, <a
                            href="#prerequis">Prérequis.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="contact-offre">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Contact sur l'offre de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Il s'agit du nom et du numéro de téléphone, de l'adresse postale ou électronique de la personne
                        habilitée à donner des renseignements sur l'action de formation. Cette personne peut être
                        également le contact de l'organisme responsable de l'offre.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="contact-organisme">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Contact de l'organisme
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Il s'agit du nom et du numéro de téléphone, de l'adresse postale ou électronique de la personne
                        habilitée à donner des renseignements sur l'action de formation ou l'ensemble des formations
                        proposées par l'organisme responsable de l'offre, voire sur l'organisme lui-même.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="contenu-formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Contenu de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Description détaillée des différents sujets traités dans la formation, en fonction d'objectifs
                        pédagogiques et de formation définis explicitement (<a href="#NFX50-750">NFX 50-750</a>).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="controle-connaissances">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Contrôle de connaissances
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Vérification de l'acquisition de savoirs. Ce contrôle peut être oral, écrit ou pratique (<a
                            href="#NFX50-750">NFX
                        50-750</a>). Voir aussi <a href="#certificat-formation">Certificat de formation</a>.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="conventionnement">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Conventionnement
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Cette information indique si une action de formation donnée bénéficie d'une contribution
                        financière du financeur public. Voir aussi <a href="#action-conventionnee">Action de formation
                        conventionnée</a>.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="coordonnees-organisme">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Coordonnées organisme
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Elles indiquent les coordonnées de l'organisme juridiquement responsable de l'action de
                        formation.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="date-limite-inscription">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Dates prévues de début et de fin de période d&rsquo;inscription
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Ces dates déterminent le début et la fin d&rsquo;une période d&rsquo;inscription pour une action
                        de formation à réaliser. La date de début remplie par des 0 indique qu&rsquo;il n&rsquo;y a pas
                        de date de début et la date de fin remplie avec des 9 indique qu&rsquo;il n&rsquo;y a pas de fin
                        définie (cas des E/S permanentes).
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-15 END -->

    <!-- bloc-16 -->
    <div class="bloc bgc-anti-flash-white l-bloc" id="bloc-16">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-6">
                    <a class="no-pointer" name="dates-debut-fin-stage">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Dates prévues de début et de fin de la session ou d'entrées sorties permanentes
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Elles indiquent de manière prévisionnelle les périodes de démarrage et d'achèvement de la phase
                        de réalisation de l'action de formation. De plus elles servent également à savoir si ces dates
                        sont prédéterminées ou non. Voir respectivement <a href="es-dates-fixes">Entrées-sorties à dates
                        fixes</a> ou <a href="#es-permanentes">Entrées-sorties
                        permanentes</a>.
                    </h4>
                </div>
                <div class="col-sm-6">
                    <a class="no-pointer" name="detail-conditions">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Détail des conditions de prise en charge
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Cette donnée permettra d'indiquer les conditions particulières de prise en charge de l'action
                        par le financeur, comme par exemple le conventionnement du conseil régional (nombre, public,
                        durée). Voir aussi <a href="#prix-horaire-TTC">Prix horaire T.T.C. de la formation</a>, <a
                            href="#prix-total-TTC">Prix total T.T.C. de la formation</a>,
                        <a href="#financement-formation">Financement de la formation</a>.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-6">
                    <a class="no-pointer" name="diplome">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Diplôme
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Document écrit établissant un privilège ou un droit. Il émane d'une autorité compétente, sous le
                        contrôle de l'État. Il conditionne l'accès à certaines professions et à certaines formations ou
                        concours. Il reconnaît au titulaire un niveau de capacité vérifié (<a href="#NFX50-750">NFX
                        50-750). Voir aussi
                        <a href="#certification-pro">Certification professionnelle</a>.
                    </h4>
                </div>
                <div class="col-sm-6">
                    <a class="no-pointer" name="dispensateur-formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Dispensateur de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Toute personne physique ou morale ayant la capacité de souscrire des conventions ou des contrats
                        de prestations de service dont l'objet est la formation. Cette expression désigne à la fois les
                        formateurs indépendants et les organismes de formation. Les dispensateurs de formation sont
                        soumis à des obligations légales et réglementaires particulières. Ils sont tenus notamment de
                        faire une déclaration d'activité en début d'activité dès la conclusion de la première convention
                        ou du premier contrat de formation professionnelle (article <a
                            href="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000021343616&cidTexte=LEGITEXT000006072050&dateTexte=20151108"
                            data-placement="top" data-toggle="tooltip" title="Voir l'article"
                            target="_blank">L6351-1</a> du code du travail). Les termes "dispensateur de formation",
                        "organisme de formation" et "prestataire de formation" sont synonymes (<a href="#NFX50-750">NFX
                        50-750</a>).
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-16 END -->

    <!-- bloc-17 -->
    <div class="bloc l-bloc bgc-white" id="bloc-17">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-4">
                    <a class="no-pointer" name="domaine-formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Domaine de la formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Les champs intellectuels dans lesquels vient s&rsquo;inscrire une action de formation,
                        considérée dans son contenu, son programme ou ses objectifs affichés (NSF, FORMACODE).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="duree-conventionnee">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Durée conventionnée
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Durée pendant laquelle l'action bénéficie d'un financement.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="es-dates-fixes">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Entrées-sorties à dates fixes
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Actions de formation dont les périodes de démarrage et d'achèvement (avec des dates de début de
                        fin) sont prédéterminées à l'avance. Elles correspondent au concept de session de formation.
                        Antonyme d'entrées-sorties permanentes.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="es-permanentes">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Entrées-sorties permanentes
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Actions de formation pour lesquelles les démarrages ou les fins de périodes de formation
                        interviennent indifféremment au long de l'année civile et sans dates prédéterminées à l'avance.
                        Antonyme d'entrées-sorties à dates fixes.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="etat-recrutement">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Etat du recrutement
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Cette donnée permet à l&rsquo;organisme responsable de la session de formation d&rsquo;indiquer,
                        à un instant donné, s&rsquo;il recrute sur la session. Elle peut prendre les valeurs : "ouvert",
                        tant que l&rsquo;organisme dispose de places à proposer sur la session ; "fermé", dès que le
                        nombre de places maximum est atteint ; "suspendu", dès lors que l&rsquo;inscription est
                        interrompue et qu&rsquo;il demeure possible de s&rsquo;inscrire à une date ultérieure sur cette
                        même session (exemple : cas des entrées/sorties permanentes). Ces valeurs sont répertoriées dans
                        la table du langage ad-hoc.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="evaluation-formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Evaluation de la formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Action d'apprécier, à l'aide de critères définis préalablement, l'atteinte des objectifs
                        pédagogiques et de formation d'une action de formation. Cette évaluation peut être faite à des
                        temps différents, par des acteurs différents (apprenant, formateur, entreprise cliente,). On
                        distingue, par exemple, l'évaluation de satisfaction, l'évaluation du contenu de l'action de
                        formation, l'évaluation des acquis, et l'évaluation des transferts éventuels en situation de
                        travail (<a href="#NFX50-750">NFX 50-750</a>).
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-17 END -->

    <!-- bloc-18 -->
    <div class="bloc bgc-anti-flash-white l-bloc" id="bloc-18">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-4">
                    <a class="no-pointer" name="examen">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Examen
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Épreuve ou série d'épreuves destinées à déterminer l'aptitude d'un candidat à obtenir un titre,
                        un diplôme, un certificat ou à suivre une formation (<a href="#NFX50-750">NFX 50-750</a>).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="financement-formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Financement de la formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Tous les moyens financiers mis en œuvre par l'État, les collectivités territoriales, les
                        partenaires sociaux, les entreprises et les apprenants pour réaliser la formation (<a
                            href="#NFX50-751">NFX 50-751</a>).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        S'entend de toutes les initiatives visant à apprendre, à former et notamment celles entrant dans
                        le champ d'application des dispositions relatives à la formation professionnelle continue. Ce
                        sont les actions visées à l'article <a
                            href="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006178201&cidTexte=LEGITEXT000006072050&dateTexte=20151126"
                            data-placement="top" data-toggle="tooltip" title="Voir l'article" target="_blank">L6313</a>
                        du code du travail et notamment les actions de préformation et de préparation à la vie
                        professionnelle, d'adaptation, de promotion, de prévention, d'acquisition, d'entretien ou de
                        perfectionnement, de bilan de compétences, et de validation des acquis de l'expérience.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="formation-alternee">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Formation alternée
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Succession de périodes de formation organisées entre lieu de formation et milieu de travail (<a
                            href="#NFX50-750">NFX
                        50-750</a>).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="foad">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Formation en centre ou à distance
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Indication permettant de savoir si la formation à lieu dans un centre de formation ou à
                        distance.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="formation-distance">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Formation à distance (FOAD)
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Système de formation conçu pour permettre à des individus de se former sans se déplacer dans un
                        lieu de formation et sans la présence physique d'un formateur (<a href="#NFX50-751">NFX
                        50-751</a>).
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="individualisation-formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Individualisation de la formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Mode d'organisation de la formation visant la mise en oeuvre d'une démarche personnalisée de
                        formation. Elle met à la disposition de l'apprenant l'ensemble des ressources et des moyens
                        pédagogiques nécessaires à son parcours de formation et à ses situations d'apprentissage. Elle
                        prend en compte ses acquis, ses objectifs, son rythme (<a href="#NFX50-751">NFX 50-751</a>).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="info-nb-heures-total">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Informations sur le nombre d'heures total
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Ce commentaire précise la décomposition du nombre d&rsquo;heures total en centre de formation et
                        en entreprise, et qui peut de plus comprendre des heures de formation ouverte et à distance par
                        exemple.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="info-public-vise">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Informations sur le public visé
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Cette donnée permet à l&rsquo;organisme responsable de l&rsquo;action de formation d&rsquo;ajouter
                        des caractéristiques à l&rsquo;article sélectionné dans la table "public visé" et ce, sous forme
                        de commentaires, dès lors qu&rsquo;ils ne donnent pas lieu à discrimination (portée juridique).
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-18 END -->

    <!-- bloc-19 -->
    <div class="bloc l-bloc bgc-white" id="bloc-19">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-4">
                    <a class="no-pointer" name="intitule-action">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Intitulé de l'action de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Intitulé qui sert à caractériser et singulariser une action de formation. Il en indique le
                        titre.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="lieu-formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Lieu de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Adresse complète du lieu où se déroule la formation.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="lieu-date-inscription">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Lieu et date d'information et d'inscription
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Indiquent la ou les adresse(s) précise(s) du/des lieu(x) où se déroulent les actions
                        d'information et d'inscription pour une action de formation donnée. Toutes les dates associées
                        aux actions d'information et aux périodes d'inscription doivent être également précisées.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="logistique-formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Logistique de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Gestion des moyens matériels, humains et pédagogiques nécessaires à une action de formation (<a
                            href="#NFX50-750">NFX
                        50-750</a>).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="methode-pedagogique">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Méthode pédagogique
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Ensemble de démarches formalisées et appliquées selon des principes définis pour acquérir un
                        ensemble de savoirs conformes aux objectifs pédagogiques (<a href="#NFX50-750">NFX 50-750</a>).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="modalites-alternance">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Modalités de l'alternance
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Préciser l'organisation de l'alternance : dates, durées des périodes en centre ou en entreprise.
                        Voir <a href="#formation-alternee">Formation alternée</a>.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="modalites-entrees-sorties">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Modalités d'entrées-sorties
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Voir <a href="#dates-debut-fin-stage">Dates prévues de début ou de fin de session de formation
                        ou entrées/sorties permanentes</a>.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="modalites-pedagogiques">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Modalités pédagogiques
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Pédagogies mises en œuvres dans le déroulement de la formation, comme des études de cas, des
                        mises en situation, l'individualisation de la formation, les possibilités d'autoformation, etc.
                        Voir <a href="#individualisation-formation">Individualisation de la formation</a>.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="modalites-recrutement">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Modalités de recrutement et d'admission
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Procédés mis en œuvre pour recruter. Voir <a href="#prerequis">Prérequis</a>.
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-19 END -->

    <!-- bloc-20 -->
    <div class="bloc bgc-anti-flash-white l-bloc" id="bloc-20">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-4">
                    <a class="no-pointer" name="module-formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Module de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Unité de formation autonome qui constitue un tout cohérent en soi, et fait partie d'un cursus de
                        formation. Il est construit à partir des éléments suivants : objectifs, objectifs pédagogiques
                        généraux, contenu, durée, pré-requis. Un module correspond à un ensemble de séquences de
                        formation (AFNOR). Un module de formation vise à faire acquérir des compétences, c'est-à-dire
                        une articulation de savoirs contextualisés dans une activité professionnelle.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="moyen-pedagogique">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Moyen pédagogique
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Tout procédé, matériel ou immatériel, utilisé dans le cadre d'une méthode pédagogique : lecture
                        d'ouvrages, étude de cas, mise en situation, utilisation de films, de jeux... (<a
                            href="#NFX50-750">NFX 50-750</a>).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="niveaux">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Niveaux
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Les niveaux de formation correspondent à une position hiérarchique dans une nomenclature définie
                        par l'éducation nationale d'un diplôme ou d'une formation. La personne peut soit être titulaire
                        du diplôme ou titre correspondant aux entrées de la nomenclature, soit occuper un emploi
                        exigeant normalement un niveau comparable.La nomenclature des niveaux actuelle a été construite
                        en 1969 en s'appuyant sur une grille établie en 1967 pour classer les formations conduisant aux
                        diplômes de l'éducation nationale.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="niveau-entree">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Niveau à l'entrée en formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Il correspond au niveau de titre ou de diplôme acquis par le demandeur de formation au moment de
                        son entrée dans l&rsquo;action. Voir aussi <a href="#niveaux">Niveaux</a>, <a href="#prerequis">Prérequis</a>.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="nom-organisme">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Nom de l'organisme de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Il s'agit de la dénomination usuelle de l'organisme responsable de l'offre de formation. Ce
                        peut-être notamment un sigle ou un nom pour les sociétés en nom propre. Voir <a
                            href="#organisme-responsable">Organisme
                        responsable</a>, <a href="#raison-sociale-formateur">Raison sociale de l'organisme formateur</a>,
                        <a href="#raison-sociale-organisme">Raison sociale de l'organisme responsable
                            de la formation</a>.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="nombre-heures-total-centre">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Nombre d&rsquo;heures total en centre de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Cette donnée correspond au nombre d&rsquo;heures maximum en centre de formation indiqué par l&rsquo;organisme
                        responsable de l&rsquo;action de formation.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="nombre-heures-total-entreprise">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Nombre d&rsquo;heures total en entreprise
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Cette donnée correspond au nombre d&rsquo;heures maximum en entreprise indiqué par l&rsquo;organisme
                        responsable de l&rsquo;action de formation.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="nombre-heures-total-max">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Nombre d&rsquo;heures total maximum
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Cette donnée correspond au nombre total d&rsquo;heures maximum indiqué par l&rsquo;organisme
                        responsable de l&rsquo;action de formation.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="siren">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Numéro SIREN/SIRET
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Il s'agit des numéros INSEE d'immatriculation des entreprises (SIREN), ou des établissements
                        employeurs d&rsquo;une entreprise (SIRET). Le SIRET se compose de l&rsquo;identifiant entreprise
                        agrémenté de caractères permettant d&rsquo;identifier chaque établissement employeur au sein de
                        l&rsquo;entreprise mère.
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-20 END -->

    <!-- bloc-21 -->
    <div class="bloc l-bloc bgc-white" id="bloc-21">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-6">
                    <a class="no-pointer" name="numero-activite">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Numéro de déclaration d'activité
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Il s'agit de l'immatriculation attribuée par les services de contrôle de la formation
                        professionnelle aux organismes exerçant dans ce secteur.
                    </h4>
                </div>
                <div class="col-sm-6">
                    <a class="no-pointer" name="objectif-formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Objectif de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Compétence(s) à acquérir, à améliorer ou à entretenir exprimée(s) initialement par les
                        commanditaires et/ou les apprenants. L'objectif de formation est l'élément fondamental des
                        cahiers des charges. Il sert à évaluer les effets de la formation (<a href="#NFX50-750">NFX
                        50-750</a>). Il doit exprimer
                        clairement les compétences visées (savoirs, savoir-faire, comportements). Il doit être
                        formalisé.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-6">
                    <a class="no-pointer" name="objectif-general-formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Objectif général de la formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Il peut s'agir d'une session de certification, de professionnalisation, de préparation à la
                        qualification, de remise à niveau, de (re)mobilisation, de perfectionnement ou de création
                        d'entreprise. Ces catégories sont de type administratif et sont décrites
                        ci-dessous.Certification - Cette catégorie comprend l'ensemble des formations sanctionnées par
                        le passage d'une certification (diplôme, titre, certificat de qualification professionnelle).
                        Les certifications regroupent l'ensemble des diplômes généraux de l'éducation nationale et de
                        l'enseignement supérieur (diplômes nationaux, diplômes des universités) ainsi que l'ensemble des
                        certifications professionnelles inscrites au Répertoire national des certifications
                        professionnelles (RNCP).
                    </h4>
                </div>
                <div class="col-sm-6">

                    <h3 class=" tc-dark-slate-blue text-left mg-sm">
                        Professionnalisation
                    </h3>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Les objectifs des formations professionnalisantes sont très proches de ceux des formations
                        certifiantes professionnelles, mais elles ne donnent lieu à aucun diplôme, titre ou certificat
                        inscrit au RNCP. Comme les formations certifiantes professionnelles, ces formations visent à
                        enseigner les techniques et connaissances propres à rendre un individu opérationnel dans un
                        métier (ou plus généralement sur un type de poste de travail). Attention à ne pas confondre avec
                        les formations de perfectionnement, qui visent à approfondir les compétences de publics déjà
                        opérationnels dans un métier ou un poste donné.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-6">
                    <h3 class=" tc-dark-slate-blue text-left mg-sm">
                        Préparation à la qualification
                    </h3>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Cette catégorie comprend les formations qui préparent à l'entrée dans toute formation
                        qualifiante (i.e. certifiante ou professionnalisante), quelque soit son niveau. On y inclut bien
                        sûr les formations de pré-qualification pour les jeunes (des Conseils régionaux) par exemple.
                        Les formations de préparation aux concours sont donc incluses dans cette catégorie. En effet,
                        elles préparent à l'entrée dans une école ou plus généralement à un cursus composé de périodes
                        de formation et de périodes d'application pratique. Ce n'est qu'à la fin de ce cursus que la
                        réussite du titre final aura lieu. Il convient de ne pas confondre les actions de préparation à
                        la qualification s'adressant aux jeunes non qualifiés, avec les formations aux savoirs de base.
                        Même s'ils comportent des modules de remise à niveau, leur finalité est bien, dans un métier
                        donné, de préparer à l'entrée dans une formation qualifiante pour ce métier.
                    </h4>
                </div>
                <div class="col-sm-6">
                    <h3 class=" tc-dark-slate-blue text-left mg-sm">
                        Remise à niveau, maîtrise des savoirs de base, initiation
                    </h3>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Il s'agit là de sessions de remise à niveau ou d'initiation à des compétences ou techniques
                        transversales à une large gamme de métiers. Il peut s'agir de remise à niveau dans les
                        disciplines générales (français, lutte contre l'illettrisme, mathématiques de base), mais
                        également d'initiation aux langues, d'initiation aux logiciels courants de bureautique (tableur,
                        traitement de texte, autre) ou d'initiation à Internet (messagerie et navigation) et aux autres
                        compétences clés.
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-21 END -->

    <!-- bloc-22 -->
    <div class="bloc bgc-anti-flash-white l-bloc" id="bloc-22">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-6">
                    <h3 class=" tc-dark-slate-blue text-left mg-sm">
                        (Re)mobilisation, aide à l'élaboration de projet professionnel
                    </h3>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Cette catégorie rassemble l'ensemble des sessions de formation visant à analyser les
                        perspectives d'orientation des apprenants en tenant compte de leurs motivations, de leurs
                        capacités professionnelles ainsi que des difficultés sociales qu'ils ont éventuellement
                        rencontrées. Ils peuvent intégrer un travail sur les savoirs de base ou des modules de
                        familiarisation avec le milieu de l'entreprise. Ces sessions ne se confondent pas avec les
                        sessions de formation de préqualification qui s'inscrivent quant à elles dans un métier défini
                        et constituent une étape avant d'entreprendre une action certifiante ou professionnalisante.
                    </h4>
                </div>
                <div class="col-sm-6">
                    <h3 class=" tc-dark-slate-blue text-left mg-sm">
                        Perfectionnement, élargissement des compétences
                    </h3>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Les formations de cette catégorie s'adressent à un public de personnes déjà opérationnelles dans
                        leur activité professionnelle occupée ou recherchée, mais qui désirent approfondir leurs
                        compétences ou acquérir des compétences supplémentaires. Elles favorisent l'adaptation des
                        salariés à leur poste de travail, l'évolution ou le maintien dans leur emploi. Dans le cas
                        particulier du perfectionnement, ces formations supposent explicitement des pré-requis
                        (qualification ou expérience professionnelle). Par exemple, cette catégorie comprend les
                        sessions de niveau avancé de langue, de bureautique et d'utilisation des outils Internet. Elle
                        comprend aussi les sessions de développement personnel pour les salariés.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-3">
                    <a class="no-pointer" name="organisation-materielle">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Organisation matérielle de la formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Elle concerne des informations diverses et éventuelles sur les moyens transport et les
                        commodités d'accès (parking, accès handicapés, etc) au lieu de formation, sa localisation
                        précise (adresse complète), les locaux (conditions d'accueil, localisation des salles de
                        formation, etc), les modalités de restauration (lieu, tarif, etc) et d'hébergement (lieu,
                        coordonnées, tarif, etc).
                    </h4>
                </div>
                <div class="col-sm-3">
                    <a class="no-pointer" name="organisme-accueil">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Organisme d'accueil de la formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Il s'agit, pour une action de formation donnée, de l'organisme qui l'accueille dans ses locaux.
                    </h4>
                </div>
                <div class="col-sm-3">
                    <a class="no-pointer" name="organisme-financeur">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Organisme financeur
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Organisme qui finance l'action de formation. Voir <a href="#financement-formation">Financement
                        de l'action de formation</a> et la
                        <a href="#annexeI">Table des financeurs</a>.
                    </h4>
                </div>
                <div class="col-sm-3">
                    <h3 class=" tc-dark-slate-blue text-left mg-sm">
                        Organisation de la formation
                    </h3>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Agencement des différentes actions de formation du point de vue matériel et pédagogique (<a
                            href="#NFX50-750">NFX
                        50-750</a>).
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-22 END -->

    <!-- bloc-23 -->
    <div class="bloc l-bloc bgc-white" id="bloc-23">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-4">
                    <a class="no-pointer" name="organisme-formateur">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Organisme formateur
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Il s'agit, pour une action de formation donnée, de l'organisme qui concrètement la réalise. Voir
                        <a href="#dispensateur-formation">Dispensateur de formation</a>.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="organisme-responsable">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Organisme responsable
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Il s'agit, pour une action de formation donnée, de l'organisme juridiquement chargé de l'offre
                        de formation proposée. Il est l'organisme signataire des documents contractuels relatifs à
                        l'offre. C'est l'organisme qui engage sa responsabilité auprès du financeur de l'action de
                        formation.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="parcours-formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Parcours de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Itinéraire organisé d'acquisition de connaissances. Il comporte des évaluations et peut
                        déboucher sur une validation (AFNOR). Voir <a href="#parcours-formation-individualise">Parcours
                        de formation individualisé</a>, <a href="#parcours-formation-mixte">Parcours de
                        formation mixte</a>, <a href="#parcours-formation-modularise">Parcours de formation
                        modularisé</a>, <a href="#parcours-formation-personnalisable">Parcours de formation
                        personnalisable</a>.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="parcours-formation-individualise">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Parcours de formation individualisé
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Parcours de formation mis en place après un positionnement général, permettant d'identifier les
                        écarts à l'objectif recherché et de répondre strictement à ceux-ci en termes de temps de
                        formation révisables.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="parcours-formation-mixte">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Parcours de formation mixte
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Parcours de formation mis en place pour un apprenant et répondant à la fois à certaines
                        caractéristiques de parcours individualisé ou modularisé. Voir <a
                            href="#parcours-formation-individualise">Parcours de formation
                        individualisé</a>, <a href="#parcours-formation-modularise">Parcours de formation modularisé</a>.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="parcours-formation-modularise">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Parcours de formation modularisé
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Parcours de formation mis en place pour un apprenant après un positionnement général et/ou des
                        positionnements par module de formation permettant d'identifier les modules nécessaires à
                        l'atteinte de l'objectif de formation recherché.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="parcours-formation-personnalisable">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Parcours de formation personnalisable
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Un parcours de formation est personnalisable quand il est adaptable à un apprenant. Il aboutit à
                        une offre de parcours individualisé, modularisé ou mixte. La personnalisation d'un parcours de
                        formation impose la mise en place d'un positionnement à l'entrée en formation. Voir <a
                            href="#parcours-formation-individualise">Parcours de
                        formation individualisé</a>, <a href="#parcours-formation-mixte">Parcours de formation mixte</a>,
                        <a href="#parcours-formation-modularise">Parcours de formation modularisé</a>.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="perimetre-recrutement">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Périmètre de recrutement
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Il correspond, pour une action de formation donnée, à l'espace potentiel de recrutement des
                        apprenants.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="positionnement">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Positionnement à l'entrée en formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Processus permettant d'évaluer à l'entrée en formation les acquis et les besoins d'un apprenant
                        au regard de l'objectif de la formation. Il permet d'élaborer un parcours personnalisé de
                        formation, réglementaire ou pédagogique. Voir <a href="#parcours-formation-personnalisable">Parcours
                        de formation personnalisable</a>, <a href="#objectif-formation">Objectif de
                        formation</a>.
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-23 END -->

    <!-- bloc-24 -->
    <div class="bloc bgc-anti-flash-white l-bloc" id="bloc-24">
        <div class="container bloc-lg">
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="prix-total-TTC">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Prix total T.T.C. de la formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Cette donnée correspond au coût total maximum par apprenant, affiché par l&rsquo;organisme
                        responsable de l&rsquo;action de formation, toutes taxes et tout frais compris.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="programme-formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Programme de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Descriptif écrit et détaillé des contenus de formation planifiés. Il respecte une progression
                        pédagogique liée aux objectifs de formation à atteindre (<a href="#NFX50-750">NFX 50-750</a>).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="public-vise">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Public visé
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Il s'agit des différentes catégories de population auxquelles s'adresse l'action de formation.
                        Ces catégories sont définies en fonction de différents types de critères (ex: administratifs,
                        sociaux-économiques, âge, sexe, etc...)
                    </h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <a class="no-pointer" name="prerequis">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Prérequis
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Acquis préliminaires, nécessaires pour suivre efficacement une formation déterminée (<a
                            href="#NFX50-750">NFX
                        50-750</a>).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="prise-charge-frais">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Prise en charge des frais de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Indique si une prise en charge des frais de formation est possible. Voir <a
                            href="#prix-horaire-TTC">Prix horaire T.T.C. de
                        la formation</a>, <a href="#financement-formation">Financement de la formation</a>.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="prix-horaire-TTC">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Prix horaire T.T.C. de la formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Prix de vente toutes taxes comprises de l'heure de formation qui induit l'ensemble des frais
                        entraînés par la réalisation de l'action de formation.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="raison-sociale-formateur">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Raison sociale de l'organisme formateur
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Il s'agit de la raison sociale de l'organisme qui assure la formation. Cette dénomination à
                        valeur juridique peut être identique au nom usuel ou sigle de l&rsquo;organisme. Voir aussi
                        <a href="#raison-sociale-organisme">Raison sociale de l'organisme responsable de la
                            formation</a>.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="raison-sociale-organisme">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Raison sociale de l'organisme responsable de la formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Il s'agit de la raison sociale de l'organisme responsable de l'offre. Cette dénomination à
                        valeur juridique peut être identique au nom usuel ou sigle de l'organisme. Voir aussi <a
                            href="#raison-sociale-formateur">Raison
                        sociale de l'organisme formateur</a>.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="reconnaissance-acquis">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Reconnaissance des acquis
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Prise en considération de l'ensemble des formations et des expériences d'un individu (<a
                            href="#NFX50-750">NFX
                        50-750</a>).
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="remuneration-possible">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Rémunération possible
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Indique la possibilité ou non de percevoir une rémunération pendant la session de formation,
                        indépendamment de l'étude particulière de chaque situation individuelle.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="renseignements-specifiques">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Renseignements spécifiques
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Ils permettent d'indiquer des informations spécifiques (démarche qualité, labels, capacités de
                        positionnement, ...) sur l'organisme responsable juridiquement de l'action de formation, et
                        notamment les garanties proposées pour réaliser l'action.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="restauration">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Restauration, hébergement, transports
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Informations relatives aux services annexes à une action de formation qui permettent de se
                        déplacer puis d'accéder au lieu de formation, d'assurer l'accueil, la restauration et
                        l'hébergement des apprenants. Voir <a href="#logistique-formation">Logistique de formation</a>.
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-24 END -->

    <!-- bloc-25 -->
    <div class="bloc l-bloc bgc-white" id="bloc-25">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-4">
                            <a class="no-pointer" name="resultats-formation">
                                <h3 class=" tc-dark-slate-blue text-left mg-sm">
                                    Résultats de la formation
                                </h3>
                            </a>
                            <h4 class=" mg-sm tc-olive-drab-7 text-left">
                                Ils matérialisent le passage en formation et en précisent les modalités de
                                reconnaissance ou de validation. Voir <a href="#acquis">Acquis</a>, <a
                                    href="#attestation-acquis">Attestation des
                                acquis</a>, <a href="#attestation-presence">Attestation de présence</a>, <a
                                    href="#attestation-stage">Attestation de fin de formation</a>, <a
                                    href="#certification-acquis">Certification des
                                acquis de la formation</a>, <a href="#certificat-formation">Certificat de formation</a>,
                                <a href="#controle-connaissances">Contrôle de connaissances</a>, <a href="#diplome">Diplôme</a>,
                                <a href="#certification-pro">Certification professionnelle</a>, <a
                                    href="#evaluation-formation">Evaluation de la formation</a>, <a href="#examen">Examen</a>,
                                <a href="#reconnaissance-acquis">Reconnaissance des
                                    acquis</a>, <a href="#validation-acquis">Validation des acquis</a>.
                            </h4>
                        </div>
                        <div class="col-sm-4">
                            <a class="no-pointer" name="rythme-formation">
                                <h3 class=" tc-dark-slate-blue text-left mg-sm">
                                    Rythme de la formation
                                </h3>
                            </a>
                            <h4 class=" mg-sm tc-olive-drab-7 text-left">
                                Ce sont la ou les possibilité(s) de suivi offerte(s) aux apprenants pour une action de
                                formation proposée. Ce peut-être un suivi à temps partiel, à temps plein ou encore un
                                mélange de ces 2 modalités (temps plein et temps partiel) sur la même action de
                                formation.
                            </h4>
                        </div>
                        <div class="col-sm-4">
                            <a class="no-pointer" name="sequence-formation">
                                <h3 class=" tc-dark-slate-blue text-left mg-sm">
                                    Séquence de formation
                                </h3>
                            </a>
                            <h4 class=" mg-sm tc-olive-drab-7 text-left">
                                Unité pédagogique élémentaire constitutive d'un module de formation. Elle est construite
                                à partir d'un objectif pédagogique général, d'un contenu, d'une durée et d'un
                                pré-requis.
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="session-formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Session de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Période pendant laquelle une action de formation sera réalisée dans un centre de formation ou
                        dans une entreprise et regroupant généralement plusieurs participants en simultané. Voir aussi
                        <a href="#action">Action de formation.</a>
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="sigle-organisme">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Sigle de l'organisme de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Nom. Voir <a href="#nom-organisme">Nom de l'organisme de formation</a>, <a
                            href="#organisme-responsable">Organisme responsable</a>.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="stage-formation">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Stage de formation
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Période pendant laquelle quelqu'un exerce une activité temporaire dans un centre de formation ou
                        en entreprise, en vue de sa formation. Dans LHÉO, il équivaut à la notion de session de
                        formation. Voir aussi <a href="#session-formation">Session de formation</a>.
                    </h4>
                </div>
            </div>
            <div class="row voffset">
                <div class="col-sm-4">
                    <a class="no-pointer" name="support-pedagogique">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Support pédagogique
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Moyen matériel utilisé dans le cadre d'une méthode pédagogique (transparents, DVD, logiciels,
                        plans de cours, livres, jeux,...) (<a href="#NFX50-750">NFX 50-750</a>).
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="titre-professionnel">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Titre professionnel
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Document écrit établissant un privilège ou un droit et attestant de la capacité d'une personne à
                        exercer les activités professionnelles correspondant à une profession déterminée. Il est délivré
                        ou reconnu par l'État. Il conditionne l'accès à certaines professions et à certaines formations
                        ou concours. Il reconnaît au titulaire un niveau de capacité vérifié. Voir aussi <a
                            href="#diplome">Diplôme</a> et
                        <a href="#certification-pro">Certification professionnelle</a>.
                    </h4>
                </div>
                <div class="col-sm-4">
                    <a class="no-pointer" name="validation-acquis">
                        <h3 class=" tc-dark-slate-blue text-left mg-sm">
                            Validation des acquis de l'expérience
                        </h3>
                    </a>
                    <h4 class=" mg-sm tc-olive-drab-7 text-left">
                        Procédure mise en oeuvre dans le cadre de la loi du 17 janvier 2002 qui ouvre le droit à toute
                        personne de faire reconnaître, sous certaines conditions, par une certification à finalité
                        professionnelle inscrite au répertoire national des certifications professionnelles (titre,
                        diplôme, CQP) les acquis de son expérience (NFX 50-750). Voir aussi <a href="#action">Action de
                        formation</a>.
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-25 END -->

    <!-- bloc-26 -->
    <div class="bloc bgc-anti-flash-white l-bloc" id="bloc-26">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="mg-md  tc-dark-slate-blue">
                        Annexes du glossaire
                    </h1>

                    <a class="no-pointer" name="annexeI" id="annexeI">
                        <h2 class="mg-md  tc-olive-drab-7">
                            Liste des financeurs potentiels
                        </h2>
                    </a>
                    <ul class="list-unstyled">
                        <li>
                            <h3 class=" mg-sm">
                                <span class="ion ion-location"></span> AGEFIPH - Association nationale de GEstion du
                                Fonds pour l&rsquo;Insertion Professionnelle des personnes Handicapées
                            </h3>
                        </li>
                        <li>
                            <h3 class=" mg-sm">
                                <span class="ion ion-location"></span> Entreprise
                            </h3>
                        </li>
                        <li>
                            <h3 class=" mg-sm">
                                <span class="ion ion-location"></span> État
                            </h3>
                        </li>
                        <li>
                            <h3 class=" mg-sm">
                                <span class="ion ion-location"></span> Conseil régional
                            </h3>
                        </li>
                        <li>
                            <h3 class=" mg-sm">
                                <span class="ion ion-location"></span> Conseil général
                            </h3>
                        </li>
                        <li>
                            <h3 class=" mg-sm">
                                <span class="ion ion-location"></span> Collectivité locale
                            </h3>
                        </li>
                        <li>
                            <h3 class=" mg-sm">
                                <span class="ion ion-location"></span> FSE - Fond Social Européen
                            </h3>
                        </li>
                        <li>
                            <h3 class=" mg-sm">
                                <span class="ion ion-location"></span> Pôle emploi
                            </h3>
                        </li>
                        <li>
                            <h3 class=" mg-sm">
                                <span class="ion ion-location"></span> OPCA - Organisme Paritaire Collecteur Agréé
                            </h3>
                        </li>
                        <li>
                            <h3 class=" mg-sm">
                                <span class="ion ion-location"></span> Autres financeurs privés
                            </h3>

                            <h3 class=" mg-sm">
                                <span class="ion ion-location"></span> Autres financeurs publics
                            </h3>
                        </li>
                        <li>
                            <h3 class=" mg-sm">
                                <span class="ion ion-location"></span> Autres financeurs privés
                            </h3>
                        </li>
                        <li>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-26 END -->

    <!-- bloc-27 -->
    <div class="bloc l-bloc bgc-white" id="bloc-27">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-12">
                    <a class="no-pointer" name="bibliographie" id="bibliographie">
                        <h2 class="mg-md  tc-olive-drab-7">
                            Bibliographie
                        </h2>
                    </a>

                    <a class="no-pointer" name="NFX50-750" id="NFX50-750"/>
                    <a class="no-pointer" name="NFX50-751" id="NFX50-751"/>

                    <h3 class="mg-md ">
                        <span class="ion ion-bookmark"></span> AFNOR - Norme NF X 50-750 : formation professionnelle,
                        terminologie, juillet 1996.AFNOR - Norme FD X 50-751 : formation professionnelle, terminologie,
                        fascicule explicatif, juillet 1996.
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-27 END -->


</div>
<!-- Main container END -->
