<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="lheo" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="offres" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="offre-formation" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="action-formation" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="organisation-formation" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="organismes" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="organisation-pedagogique" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="organisation-administrative" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="organisation-materielle" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="conditions-pedagogiques" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="conditions-administratives" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="modalites-acces" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="lieu-date-inscription" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="organisme-formation-responsable" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="ref-organisme-formation-responsable" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="organisme-formateur" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extras-offres" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extras-offre-formation" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extras-action-formation" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extras-organisation-formation" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extras-organisation-pedagogique" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extras-organisation-administrative" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extras-organisation-materielle" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extras-conditions-pedagogiques" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extras-conditions-administratives" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extras-modalites-acces" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extras-lieu-date-inscription" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extras-organismes" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extras-organisme-formation-responsable" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extras-organisme-formateur" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extras-domaine-formation" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="sessions" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="domaine-formation" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="intitule-action" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="nom-organisme" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="objectif-formation" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="resultats-attendus" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="contenu-formation" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="public-vise" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="sexe-public-vise" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="age-public-vise" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="rythme-formation" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="duree-indicative" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="code-niveau-entree" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="niveau-entree" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="modalite-alternance" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="FOAD" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="conditions-specifiques" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="age" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="prise-en-charge-frais-possible" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="remuneration-possible" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="diplomante" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="lieu-de-formation" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="modalite-entrees-sorties" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="dates-debut-fin-stage" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="session" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="adresse-inscription" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="date-inscription" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="contact-offre" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="numero-activite" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="SIREN-organisme-formation" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="raison-sociale" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="coordonnees-organisme" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="contact-organisme" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="renseignements-specifiques" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="organismes-financeurs" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="statut-public-vise" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="objectif-general-formation" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="modalites-recrutement" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="modalites-pedagogiques" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="SIREN-formateur" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="raison-sociale-formateur" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="code-perimetre-recrutement" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="infos-perimetre-recrutement" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="prix-horaire-TTC" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="nombre-heures-total" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="detail-conditions-prise-en-charge" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="conventionnement-possible" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="duree-conventionnee" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="code-financeur" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="date-limite-inscription" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="restauration-hebergement-transport" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="certification" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="parcours-de-formation" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="positionnement" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="module" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="identifiant-module" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="reference-module" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="sous-modules" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="modules-prerequis" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="sous-module" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="type-module" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="code-public-vise" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="codes-NSF" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="codes-FORMACODE" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="codes-ROME" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="code-NSF" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="code-FORMACODE" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="code-ROME" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="coordonnees-libres" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="coordonnees" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="SIREN" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="adresse" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="lignesco" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="ligneco" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="lignesad" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="lignead" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="codepostal" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="code-INSEE-commune" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="ville" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="departement" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="region" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="pays" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="courriel" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="web" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="urlweb" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="telfixe" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="portable" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="fax" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="numtel" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="periode" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="deb" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="fin" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="date" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extras" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="extra" mode="content">
    <xsl:value-of select="text()"/>
  </xsl:template>
  <xsl:template match="lheo" mode="fullname">
    <xsl:text>Élément racine d'un fichier en LHÉO/XML</xsl:text>
  </xsl:template>
  <xsl:template match="offres" mode="fullname">
    <xsl:text>Offres de formation</xsl:text>
  </xsl:template>
  <xsl:template match="offre-formation" mode="fullname">
    <xsl:text>Offre de formation</xsl:text>
  </xsl:template>
  <xsl:template match="action-formation" mode="fullname">
    <xsl:text>Action de formation</xsl:text>
  </xsl:template>
  <xsl:template match="organisation-formation" mode="fullname">
    <xsl:text>Organisation de la formation</xsl:text>
  </xsl:template>
  <xsl:template match="organismes" mode="fullname">
    <xsl:text>Organismes</xsl:text>
  </xsl:template>
  <xsl:template match="organisation-pedagogique" mode="fullname">
    <xsl:text>Organisation pédagogique</xsl:text>
  </xsl:template>
  <xsl:template match="organisation-administrative" mode="fullname">
    <xsl:text>Organisation administrative</xsl:text>
  </xsl:template>
  <xsl:template match="organisation-materielle" mode="fullname">
    <xsl:text>Organisation matérielle</xsl:text>
  </xsl:template>
  <xsl:template match="conditions-pedagogiques" mode="fullname">
    <xsl:text>Conditions pédagogiques</xsl:text>
  </xsl:template>
  <xsl:template match="conditions-administratives" mode="fullname">
    <xsl:text>Conditions administratives</xsl:text>
  </xsl:template>
  <xsl:template match="modalites-acces" mode="fullname">
    <xsl:text>Modalités d'accès</xsl:text>
  </xsl:template>
  <xsl:template match="lieu-date-inscription" mode="fullname">
    <xsl:text>Lieu et date d'inscription</xsl:text>
  </xsl:template>
  <xsl:template match="organisme-formation-responsable" mode="fullname">
    <xsl:text>Organisme de formation responsable</xsl:text>
  </xsl:template>
  <xsl:template match="ref-organisme-formation-responsable" mode="fullname">
    <xsl:text>Référence à un organisme de formation</xsl:text>
  </xsl:template>
  <xsl:template match="organisme-formateur" mode="fullname">
    <xsl:text>Organisme formateur</xsl:text>
  </xsl:template>
  <xsl:template match="extras-offres" mode="fullname">
    <xsl:text>Données supplémentaires sur les offres</xsl:text>
  </xsl:template>
  <xsl:template match="extras-offre-formation" mode="fullname">
    <xsl:text>Données supplémentaires sur une offre</xsl:text>
  </xsl:template>
  <xsl:template match="extras-action-formation" mode="fullname">
    <xsl:text>Données supplémentaires sur l'action de formation</xsl:text>
  </xsl:template>
  <xsl:template match="extras-organisation-formation" mode="fullname">
    <xsl:text>Données supplémentaires sur l'organisation de la formation</xsl:text>
  </xsl:template>
  <xsl:template match="extras-organisation-pedagogique" mode="fullname">
    <xsl:text>Données supplémentaires sur l'organisation pédagogique</xsl:text>
  </xsl:template>
  <xsl:template match="extras-organisation-administrative" mode="fullname">
    <xsl:text>Données supplémentaires sur l'organisation administrative</xsl:text>
  </xsl:template>
  <xsl:template match="extras-organisation-materielle" mode="fullname">
    <xsl:text>Données supplémentaires sur l'organisation matérielle</xsl:text>
  </xsl:template>
  <xsl:template match="extras-conditions-pedagogiques" mode="fullname">
    <xsl:text>Données supplémentaires sur les conditions pédagogiques</xsl:text>
  </xsl:template>
  <xsl:template match="extras-conditions-administratives" mode="fullname">
    <xsl:text>Données supplémentaires sur les conditions administratives</xsl:text>
  </xsl:template>
  <xsl:template match="extras-modalites-acces" mode="fullname">
    <xsl:text>Données supplémentaires sur les modalités d'accès</xsl:text>
  </xsl:template>
  <xsl:template match="extras-lieu-date-inscription" mode="fullname">
    <xsl:text>Données supplémentaires sur le lieu et les dates d'inscription</xsl:text>
  </xsl:template>
  <xsl:template match="extras-organismes" mode="fullname">
    <xsl:text>Données supplémentaires sur les organismes</xsl:text>
  </xsl:template>
  <xsl:template match="extras-organisme-formation-responsable" mode="fullname">
    <xsl:text>Données supplémentaires sur l'organisme de formation responsable</xsl:text>
  </xsl:template>
  <xsl:template match="extras-organisme-formateur" mode="fullname">
    <xsl:text>Données supplémentaires sur l'organisme formateur</xsl:text>
  </xsl:template>
  <xsl:template match="extras-domaine-formation" mode="fullname">
    <xsl:text>Données supplémentaires sur le domaine de formation</xsl:text>
  </xsl:template>
  <xsl:template match="sessions" mode="fullname">
    <xsl:text>Sessions de formation</xsl:text>
  </xsl:template>
  <xsl:template match="domaine-formation" mode="fullname">
    <xsl:text>Domaine de la formation (NSF, FORMACODE, ROME)</xsl:text>
  </xsl:template>
  <xsl:template match="intitule-action" mode="fullname">
    <xsl:text>Intitulé de l'action ou de la certification préparée</xsl:text>
  </xsl:template>
  <xsl:template match="nom-organisme" mode="fullname">
    <xsl:text>Nom de l'organisme de formation</xsl:text>
  </xsl:template>
  <xsl:template match="objectif-formation" mode="fullname">
    <xsl:text>Objectif de formation</xsl:text>
  </xsl:template>
  <xsl:template match="resultats-attendus" mode="fullname">
    <xsl:text>Résultats attendus de la formation</xsl:text>
  </xsl:template>
  <xsl:template match="contenu-formation" mode="fullname">
    <xsl:text>Contenu de la formation</xsl:text>
  </xsl:template>
  <xsl:template match="public-vise" mode="fullname">
    <xsl:text>Type de Public visé</xsl:text>
  </xsl:template>
  <xsl:template match="sexe-public-vise" mode="fullname">
    <xsl:text>Sexe du public visé</xsl:text>
  </xsl:template>
  <xsl:template match="age-public-vise" mode="fullname">
    <xsl:text>Âge du public visé</xsl:text>
  </xsl:template>
  <xsl:template match="rythme-formation" mode="fullname">
    <xsl:text>Rythme de la formation</xsl:text>
  </xsl:template>
  <xsl:template match="duree-indicative" mode="fullname">
    <xsl:text>Durée indicative</xsl:text>
  </xsl:template>
  <xsl:template match="code-niveau-entree" mode="fullname">
    <xsl:text>Statut du niveau à l'entrée en formation</xsl:text>
  </xsl:template>
  <xsl:template match="niveau-entree" mode="fullname">
    <xsl:text>Niveau à l'entrée en formation</xsl:text>
  </xsl:template>
  <xsl:template match="modalite-alternance" mode="fullname">
    <xsl:text>Modalité de l'alternance</xsl:text>
  </xsl:template>
  <xsl:template match="FOAD" mode="fullname">
    <xsl:text>Formation en centre ou à distance</xsl:text>
  </xsl:template>
  <xsl:template match="conditions-specifiques" mode="fullname">
    <xsl:text>Conditions spécifiques</xsl:text>
  </xsl:template>
  <xsl:template match="age" mode="fullname">
    <xsl:text>Âge</xsl:text>
  </xsl:template>
  <xsl:template match="prise-en-charge-frais-possible" mode="fullname">
    <xsl:text>Prise en charge des frais de formation possible</xsl:text>
  </xsl:template>
  <xsl:template match="remuneration-possible" mode="fullname">
    <xsl:text>Rémunération possible</xsl:text>
  </xsl:template>
  <xsl:template match="diplomante" mode="fullname">
    <xsl:text>Formation diplômante</xsl:text>
  </xsl:template>
  <xsl:template match="lieu-de-formation" mode="fullname">
    <xsl:text>Lieu de la formation</xsl:text>
  </xsl:template>
  <xsl:template match="modalite-entrees-sorties" mode="fullname">
    <xsl:text>Dates fixes ou entrées/sorties permanentes ?</xsl:text>
  </xsl:template>
  <xsl:template match="dates-debut-fin-stage" mode="fullname">
    <xsl:text>Dates prévues de début et de fin de stage</xsl:text>
  </xsl:template>
  <xsl:template match="session" mode="fullname">
    <xsl:text>Session de formation</xsl:text>
  </xsl:template>
  <xsl:template match="adresse-inscription" mode="fullname">
    <xsl:text>Adresse d'information et d'inscription</xsl:text>
  </xsl:template>
  <xsl:template match="date-inscription" mode="fullname">
    <xsl:text>Date d'information et d'inscription</xsl:text>
  </xsl:template>
  <xsl:template match="contact-offre" mode="fullname">
    <xsl:text>Contact de l'action de formation</xsl:text>
  </xsl:template>
  <xsl:template match="numero-activite" mode="fullname">
    <xsl:text>Numéro de déclaration d'activité</xsl:text>
  </xsl:template>
  <xsl:template match="SIREN-organisme-formation" mode="fullname">
    <xsl:text>Numéro SIREN, SIRET de l'organisme de formation</xsl:text>
  </xsl:template>
  <xsl:template match="raison-sociale" mode="fullname">
    <xsl:text>Raison sociale de l'organisme</xsl:text>
  </xsl:template>
  <xsl:template match="coordonnees-organisme" mode="fullname">
    <xsl:text>Coordonnées de l'organisme</xsl:text>
  </xsl:template>
  <xsl:template match="contact-organisme" mode="fullname">
    <xsl:text>Contact avec l'organisme</xsl:text>
  </xsl:template>
  <xsl:template match="renseignements-specifiques" mode="fullname">
    <xsl:text>Renseignements spécifiques sur l'organisme</xsl:text>
  </xsl:template>
  <xsl:template match="organismes-financeurs" mode="fullname">
    <xsl:text>Organismes financeurs</xsl:text>
  </xsl:template>
  <xsl:template match="statut-public-vise" mode="fullname">
    <xsl:text>Statut du public visé</xsl:text>
  </xsl:template>
  <xsl:template match="objectif-general-formation" mode="fullname">
    <xsl:text>Objectif général de la formation</xsl:text>
  </xsl:template>
  <xsl:template match="modalites-recrutement" mode="fullname">
    <xsl:text>Modalités de recrutement</xsl:text>
  </xsl:template>
  <xsl:template match="modalites-pedagogiques" mode="fullname">
    <xsl:text>Modalités pédagogiques</xsl:text>
  </xsl:template>
  <xsl:template match="SIREN-formateur" mode="fullname">
    <xsl:text>SIRET de l'organisme formateur</xsl:text>
  </xsl:template>
  <xsl:template match="raison-sociale-formateur" mode="fullname">
    <xsl:text>Raison sociale de l'organisme formateur</xsl:text>
  </xsl:template>
  <xsl:template match="code-perimetre-recrutement" mode="fullname">
    <xsl:text>Périmètre de recrutement</xsl:text>
  </xsl:template>
  <xsl:template match="infos-perimetre-recrutement" mode="fullname">
    <xsl:text>Informations supplémentaires sur le périmètre de recrutement</xsl:text>
  </xsl:template>
  <xsl:template match="prix-horaire-TTC" mode="fullname">
    <xsl:text>Prix horaire TTC</xsl:text>
  </xsl:template>
  <xsl:template match="nombre-heures-total" mode="fullname">
    <xsl:text>Total du nombre d'heures</xsl:text>
  </xsl:template>
  <xsl:template match="detail-conditions-prise-en-charge" mode="fullname">
    <xsl:text>Détails des conditions de prise en charge</xsl:text>
  </xsl:template>
  <xsl:template match="conventionnement-possible" mode="fullname">
    <xsl:text>Possibilité de conventionnement</xsl:text>
  </xsl:template>
  <xsl:template match="duree-conventionnee" mode="fullname">
    <xsl:text>Durée du conventionnement</xsl:text>
  </xsl:template>
  <xsl:template match="code-financeur" mode="fullname">
    <xsl:text>Code financeur</xsl:text>
  </xsl:template>
  <xsl:template match="date-limite-inscription" mode="fullname">
    <xsl:text>Date limite d'inscription</xsl:text>
  </xsl:template>
  <xsl:template match="restauration-hebergement-transport" mode="fullname">
    <xsl:text>Restauration, hébergement, transport</xsl:text>
  </xsl:template>
  <xsl:template match="certification" mode="fullname">
    <xsl:text>Certification préparée</xsl:text>
  </xsl:template>
  <xsl:template match="parcours-de-formation" mode="fullname">
    <xsl:text>Type de parcours de formation</xsl:text>
  </xsl:template>
  <xsl:template match="positionnement" mode="fullname">
    <xsl:text>Type de positionnement</xsl:text>
  </xsl:template>
  <xsl:template match="module" mode="fullname">
    <xsl:text>Module de formation</xsl:text>
  </xsl:template>
  <xsl:template match="identifiant-module" mode="fullname">
    <xsl:text>Identifiant de module</xsl:text>
  </xsl:template>
  <xsl:template match="reference-module" mode="fullname">
    <xsl:text>Référence de module</xsl:text>
  </xsl:template>
  <xsl:template match="sous-modules" mode="fullname">
    <xsl:text>Sous modules</xsl:text>
  </xsl:template>
  <xsl:template match="modules-prerequis" mode="fullname">
    <xsl:text>Modules prérequis</xsl:text>
  </xsl:template>
  <xsl:template match="sous-module" mode="fullname">
    <xsl:text>Sous module</xsl:text>
  </xsl:template>
  <xsl:template match="type-module" mode="fullname">
    <xsl:text>Type de module</xsl:text>
  </xsl:template>
  <xsl:template match="code-public-vise" mode="fullname">
    <xsl:text>Code du public visé</xsl:text>
  </xsl:template>
  <xsl:template match="codes-NSF" mode="fullname">
    <xsl:text>Liste de codes NSF</xsl:text>
  </xsl:template>
  <xsl:template match="codes-FORMACODE" mode="fullname">
    <xsl:text>Liste de codes FORMACODE</xsl:text>
  </xsl:template>
  <xsl:template match="codes-ROME" mode="fullname">
    <xsl:text>Liste de codes ROME</xsl:text>
  </xsl:template>
  <xsl:template match="code-NSF" mode="fullname">
    <xsl:text>Code NSF</xsl:text>
  </xsl:template>
  <xsl:template match="code-FORMACODE" mode="fullname">
    <xsl:text>Code FORMACODE</xsl:text>
  </xsl:template>
  <xsl:template match="code-ROME" mode="fullname">
    <xsl:text>Code ROME</xsl:text>
  </xsl:template>
  <xsl:template match="coordonnees-libres" mode="fullname">
    <xsl:text>Coordonnées libres</xsl:text>
  </xsl:template>
  <xsl:template match="coordonnees" mode="fullname">
    <xsl:text>Coordonnées</xsl:text>
  </xsl:template>
  <xsl:template match="SIREN" mode="fullname">
    <xsl:text>SIRET</xsl:text>
  </xsl:template>
  <xsl:template match="adresse" mode="fullname">
    <xsl:text>Adresse</xsl:text>
  </xsl:template>
  <xsl:template match="lignesco" mode="fullname">
    <xsl:text>Lignes de coordonnées</xsl:text>
  </xsl:template>
  <xsl:template match="ligneco" mode="fullname">
    <xsl:text>Ligne de coordonnées</xsl:text>
  </xsl:template>
  <xsl:template match="lignesad" mode="fullname">
    <xsl:text>Lignes d'adresse</xsl:text>
  </xsl:template>
  <xsl:template match="lignead" mode="fullname">
    <xsl:text>Ligne d'adresse</xsl:text>
  </xsl:template>
  <xsl:template match="codepostal" mode="fullname">
    <xsl:text>Code postal</xsl:text>
  </xsl:template>
  <xsl:template match="code-INSEE-commune" mode="fullname">
    <xsl:text>Code commune INSEE</xsl:text>
  </xsl:template>
  <xsl:template match="ville" mode="fullname">
    <xsl:text>Ville</xsl:text>
  </xsl:template>
  <xsl:template match="departement" mode="fullname">
    <xsl:text>Département</xsl:text>
  </xsl:template>
  <xsl:template match="region" mode="fullname">
    <xsl:text>Région</xsl:text>
  </xsl:template>
  <xsl:template match="pays" mode="fullname">
    <xsl:text>Pays</xsl:text>
  </xsl:template>
  <xsl:template match="courriel" mode="fullname">
    <xsl:text>Courriel</xsl:text>
  </xsl:template>
  <xsl:template match="web" mode="fullname">
    <xsl:text>Web</xsl:text>
  </xsl:template>
  <xsl:template match="urlweb" mode="fullname">
    <xsl:text>URL</xsl:text>
  </xsl:template>
  <xsl:template match="telfixe" mode="fullname">
    <xsl:text>Téléphone fixe</xsl:text>
  </xsl:template>
  <xsl:template match="portable" mode="fullname">
    <xsl:text>Téléphone portable</xsl:text>
  </xsl:template>
  <xsl:template match="fax" mode="fullname">
    <xsl:text>Fax</xsl:text>
  </xsl:template>
  <xsl:template match="numtel" mode="fullname">
    <xsl:text>Numéro de téléphone</xsl:text>
  </xsl:template>
  <xsl:template match="periode" mode="fullname">
    <xsl:text>Période</xsl:text>
  </xsl:template>
  <xsl:template match="deb" mode="fullname">
    <xsl:text>Début</xsl:text>
  </xsl:template>
  <xsl:template match="fin" mode="fullname">
    <xsl:text>Fin</xsl:text>
  </xsl:template>
  <xsl:template match="date" mode="fullname">
    <xsl:text>Date</xsl:text>
  </xsl:template>
  <xsl:template match="extras" mode="fullname">
    <xsl:text/>
  </xsl:template>
  <xsl:template match="extra" mode="fullname">
    <xsl:text/>
  </xsl:template>
  <xsl:template name="dict_dict-departements-france">
    <xsl:param name="key"/>
    <xsl:choose>
      <xsl:when test="$key='1'">
        <xsl:text>Ain</xsl:text>
      </xsl:when>
      <xsl:when test="$key='01'">
        <xsl:text>Ain</xsl:text>
      </xsl:when>
      <xsl:when test="$key='001'">
        <xsl:text>Ain</xsl:text>
      </xsl:when>
      <xsl:when test="$key='2'">
        <xsl:text>Aisne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='02'">
        <xsl:text>Aisne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='002'">
        <xsl:text>Aisne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='3'">
        <xsl:text>Allier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='03'">
        <xsl:text>Allier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='003'">
        <xsl:text>Allier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='4'">
        <xsl:text>Alpes de Haute-Provence</xsl:text>
      </xsl:when>
      <xsl:when test="$key='04'">
        <xsl:text>Alpes de Haute-Provence</xsl:text>
      </xsl:when>
      <xsl:when test="$key='004'">
        <xsl:text>Alpes de Haute-Provence</xsl:text>
      </xsl:when>
      <xsl:when test="$key='5'">
        <xsl:text>Hautes-Alpes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='05'">
        <xsl:text>Hautes-Alpes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='005'">
        <xsl:text>Hautes-Alpes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='6'">
        <xsl:text>Alpes-Maritimes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='06'">
        <xsl:text>Alpes-Maritimes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='006'">
        <xsl:text>Alpes-Maritimes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='7'">
        <xsl:text>Ardèche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='07'">
        <xsl:text>Ardèche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='007'">
        <xsl:text>Ardèche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='8'">
        <xsl:text>Ardennes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='08'">
        <xsl:text>Ardennes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='008'">
        <xsl:text>Ardennes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='9'">
        <xsl:text>Ariège</xsl:text>
      </xsl:when>
      <xsl:when test="$key='09'">
        <xsl:text>Ariège</xsl:text>
      </xsl:when>
      <xsl:when test="$key='009'">
        <xsl:text>Ariège</xsl:text>
      </xsl:when>
      <xsl:when test="$key='10'">
        <xsl:text>Aube</xsl:text>
      </xsl:when>
      <xsl:when test="$key='010'">
        <xsl:text>Aube</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11'">
        <xsl:text>Aude</xsl:text>
      </xsl:when>
      <xsl:when test="$key='011'">
        <xsl:text>Aude</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12'">
        <xsl:text>Aveyron</xsl:text>
      </xsl:when>
      <xsl:when test="$key='012'">
        <xsl:text>Aveyron</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13'">
        <xsl:text>Bouches-du-Rhône</xsl:text>
      </xsl:when>
      <xsl:when test="$key='013'">
        <xsl:text>Bouches-du-Rhône</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14'">
        <xsl:text>Calvados</xsl:text>
      </xsl:when>
      <xsl:when test="$key='014'">
        <xsl:text>Calvados</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15'">
        <xsl:text>Cantal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='015'">
        <xsl:text>Cantal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='16'">
        <xsl:text>Charente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='016'">
        <xsl:text>Charente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='17'">
        <xsl:text>Charente-Maritime</xsl:text>
      </xsl:when>
      <xsl:when test="$key='017'">
        <xsl:text>Charente-Maritime</xsl:text>
      </xsl:when>
      <xsl:when test="$key='18'">
        <xsl:text>Cher</xsl:text>
      </xsl:when>
      <xsl:when test="$key='018'">
        <xsl:text>Cher</xsl:text>
      </xsl:when>
      <xsl:when test="$key='19'">
        <xsl:text>Corrèze</xsl:text>
      </xsl:when>
      <xsl:when test="$key='019'">
        <xsl:text>Corrèze</xsl:text>
      </xsl:when>
      <xsl:when test="$key='20'">
        <xsl:text>Corse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='020'">
        <xsl:text>Corse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21'">
        <xsl:text>Côte-d'Or</xsl:text>
      </xsl:when>
      <xsl:when test="$key='021'">
        <xsl:text>Côte-d'Or</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22'">
        <xsl:text>Côtes-d'Armor</xsl:text>
      </xsl:when>
      <xsl:when test="$key='022'">
        <xsl:text>Côtes-d'Armor</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23'">
        <xsl:text>Creuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='023'">
        <xsl:text>Creuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24'">
        <xsl:text>Dordogne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='024'">
        <xsl:text>Dordogne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='25'">
        <xsl:text>Doubs</xsl:text>
      </xsl:when>
      <xsl:when test="$key='025'">
        <xsl:text>Doubs</xsl:text>
      </xsl:when>
      <xsl:when test="$key='26'">
        <xsl:text>Drôme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='026'">
        <xsl:text>Drôme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='27'">
        <xsl:text>Eure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='027'">
        <xsl:text>Eure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='28'">
        <xsl:text>Eure-et-Loir</xsl:text>
      </xsl:when>
      <xsl:when test="$key='028'">
        <xsl:text>Eure-et-Loir</xsl:text>
      </xsl:when>
      <xsl:when test="$key='29'">
        <xsl:text>Finistère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='029'">
        <xsl:text>Finistère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30'">
        <xsl:text>Gard</xsl:text>
      </xsl:when>
      <xsl:when test="$key='030'">
        <xsl:text>Gard</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31'">
        <xsl:text>Haute-Garonne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='031'">
        <xsl:text>Haute-Garonne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32'">
        <xsl:text>Gers</xsl:text>
      </xsl:when>
      <xsl:when test="$key='032'">
        <xsl:text>Gers</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33'">
        <xsl:text>Gironde</xsl:text>
      </xsl:when>
      <xsl:when test="$key='033'">
        <xsl:text>Gironde</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34'">
        <xsl:text>Hérault</xsl:text>
      </xsl:when>
      <xsl:when test="$key='034'">
        <xsl:text>Hérault</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35'">
        <xsl:text>Ille-et-Vilaine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='035'">
        <xsl:text>Ille-et-Vilaine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='36'">
        <xsl:text>Indre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='036'">
        <xsl:text>Indre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='37'">
        <xsl:text>Indre-et-Loire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='037'">
        <xsl:text>Indre-et-Loire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='38'">
        <xsl:text>Isère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='038'">
        <xsl:text>Isère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='39'">
        <xsl:text>Jura</xsl:text>
      </xsl:when>
      <xsl:when test="$key='039'">
        <xsl:text>Jura</xsl:text>
      </xsl:when>
      <xsl:when test="$key='40'">
        <xsl:text>Landes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='040'">
        <xsl:text>Landes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41'">
        <xsl:text>Loir-et-Cher</xsl:text>
      </xsl:when>
      <xsl:when test="$key='041'">
        <xsl:text>Loir-et-Cher</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42'">
        <xsl:text>Loire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='042'">
        <xsl:text>Loire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43'">
        <xsl:text>Haute-Loire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='043'">
        <xsl:text>Haute-Loire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44'">
        <xsl:text>Loire-Atlantique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='044'">
        <xsl:text>Loire-Atlantique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45'">
        <xsl:text>Loiret</xsl:text>
      </xsl:when>
      <xsl:when test="$key='045'">
        <xsl:text>Loiret</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46'">
        <xsl:text>Lot</xsl:text>
      </xsl:when>
      <xsl:when test="$key='046'">
        <xsl:text>Lot</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47'">
        <xsl:text>Lot-et-Garonne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='047'">
        <xsl:text>Lot-et-Garonne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='48'">
        <xsl:text>Lozère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='048'">
        <xsl:text>Lozère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='49'">
        <xsl:text>Maine-et-Loire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='049'">
        <xsl:text>Maine-et-Loire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='50'">
        <xsl:text>Manche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='050'">
        <xsl:text>Manche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51'">
        <xsl:text>Marne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='051'">
        <xsl:text>Marne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52'">
        <xsl:text>Haute-Marne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='052'">
        <xsl:text>Haute-Marne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53'">
        <xsl:text>Mayenne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='053'">
        <xsl:text>Mayenne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54'">
        <xsl:text>Meurthe-et-Moselle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='054'">
        <xsl:text>Meurthe-et-Moselle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='55'">
        <xsl:text>Meuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='055'">
        <xsl:text>Meuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='56'">
        <xsl:text>Morbihan</xsl:text>
      </xsl:when>
      <xsl:when test="$key='056'">
        <xsl:text>Morbihan</xsl:text>
      </xsl:when>
      <xsl:when test="$key='57'">
        <xsl:text>Moselle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='057'">
        <xsl:text>Moselle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='58'">
        <xsl:text>Nièvre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='058'">
        <xsl:text>Nièvre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='59'">
        <xsl:text>Nord</xsl:text>
      </xsl:when>
      <xsl:when test="$key='059'">
        <xsl:text>Nord</xsl:text>
      </xsl:when>
      <xsl:when test="$key='60'">
        <xsl:text>Oise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='060'">
        <xsl:text>Oise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61'">
        <xsl:text>Orne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='061'">
        <xsl:text>Orne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62'">
        <xsl:text>Pas-de-Calais</xsl:text>
      </xsl:when>
      <xsl:when test="$key='062'">
        <xsl:text>Pas-de-Calais</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63'">
        <xsl:text>Puy-de-Dôme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='063'">
        <xsl:text>Puy-de-Dôme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64'">
        <xsl:text>Pyrénées-Atlantiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='064'">
        <xsl:text>Pyrénées-Atlantiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='65'">
        <xsl:text>Hautes-Pyrénées</xsl:text>
      </xsl:when>
      <xsl:when test="$key='065'">
        <xsl:text>Hautes-Pyrénées</xsl:text>
      </xsl:when>
      <xsl:when test="$key='66'">
        <xsl:text>Pyrénées-Orientales</xsl:text>
      </xsl:when>
      <xsl:when test="$key='066'">
        <xsl:text>Pyrénées-Orientales</xsl:text>
      </xsl:when>
      <xsl:when test="$key='67'">
        <xsl:text>Bas-Rhin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='067'">
        <xsl:text>Bas-Rhin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='68'">
        <xsl:text>Haut-Rhin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='068'">
        <xsl:text>Haut-Rhin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='69'">
        <xsl:text>Rhône</xsl:text>
      </xsl:when>
      <xsl:when test="$key='069'">
        <xsl:text>Rhône</xsl:text>
      </xsl:when>
      <xsl:when test="$key='70'">
        <xsl:text>Haute-Saône</xsl:text>
      </xsl:when>
      <xsl:when test="$key='070'">
        <xsl:text>Haute-Saône</xsl:text>
      </xsl:when>
      <xsl:when test="$key='71'">
        <xsl:text>Saône-et-Loire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='071'">
        <xsl:text>Saône-et-Loire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='72'">
        <xsl:text>Sarthe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='072'">
        <xsl:text>Sarthe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='73'">
        <xsl:text>Savoie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='073'">
        <xsl:text>Savoie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='74'">
        <xsl:text>Haute-Savoie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='074'">
        <xsl:text>Haute-Savoie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='75'">
        <xsl:text>Paris</xsl:text>
      </xsl:when>
      <xsl:when test="$key='075'">
        <xsl:text>Paris</xsl:text>
      </xsl:when>
      <xsl:when test="$key='76'">
        <xsl:text>Seine-Maritime</xsl:text>
      </xsl:when>
      <xsl:when test="$key='076'">
        <xsl:text>Seine-Maritime</xsl:text>
      </xsl:when>
      <xsl:when test="$key='77'">
        <xsl:text>Seine-et-Marne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='077'">
        <xsl:text>Seine-et-Marne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='78'">
        <xsl:text>Yvelines</xsl:text>
      </xsl:when>
      <xsl:when test="$key='078'">
        <xsl:text>Yvelines</xsl:text>
      </xsl:when>
      <xsl:when test="$key='79'">
        <xsl:text>Deux-Sèvres</xsl:text>
      </xsl:when>
      <xsl:when test="$key='079'">
        <xsl:text>Deux-Sèvres</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80'">
        <xsl:text>Somme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='080'">
        <xsl:text>Somme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='81'">
        <xsl:text>Tarn</xsl:text>
      </xsl:when>
      <xsl:when test="$key='081'">
        <xsl:text>Tarn</xsl:text>
      </xsl:when>
      <xsl:when test="$key='82'">
        <xsl:text>Tarn-et-Garonne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='082'">
        <xsl:text>Tarn-et-Garonne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='83'">
        <xsl:text>Var</xsl:text>
      </xsl:when>
      <xsl:when test="$key='083'">
        <xsl:text>Var</xsl:text>
      </xsl:when>
      <xsl:when test="$key='84'">
        <xsl:text>Vaucluse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='084'">
        <xsl:text>Vaucluse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='85'">
        <xsl:text>Vendée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='085'">
        <xsl:text>Vendée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='86'">
        <xsl:text>Vienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='086'">
        <xsl:text>Vienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='87'">
        <xsl:text>Haute-Vienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='087'">
        <xsl:text>Haute-Vienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='88'">
        <xsl:text>Vosges</xsl:text>
      </xsl:when>
      <xsl:when test="$key='088'">
        <xsl:text>Vosges</xsl:text>
      </xsl:when>
      <xsl:when test="$key='89'">
        <xsl:text>Yonne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='089'">
        <xsl:text>Yonne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='90'">
        <xsl:text>Territoire de Belfort</xsl:text>
      </xsl:when>
      <xsl:when test="$key='090'">
        <xsl:text>Territoire de Belfort</xsl:text>
      </xsl:when>
      <xsl:when test="$key='91'">
        <xsl:text>Essonne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='091'">
        <xsl:text>Essonne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='92'">
        <xsl:text>Hauts-de-Seine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='092'">
        <xsl:text>Hauts-de-Seine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='93'">
        <xsl:text>Seine-Saint-Denis</xsl:text>
      </xsl:when>
      <xsl:when test="$key='093'">
        <xsl:text>Seine-Saint-Denis</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94'">
        <xsl:text>Val-de-Marne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='094'">
        <xsl:text>Val-de-Marne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='95'">
        <xsl:text>Val-d'Oise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='095'">
        <xsl:text>Val-d'Oise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='971'">
        <xsl:text>Guadeloupe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='972'">
        <xsl:text>Martinique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='973'">
        <xsl:text>Guyane</xsl:text>
      </xsl:when>
      <xsl:when test="$key='974'">
        <xsl:text>Réunion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='975'">
        <xsl:text>Saint-Pierre-et-Miquelon</xsl:text>
      </xsl:when>
      <xsl:when test="$key='976'">
        <xsl:text>Mayotte</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>UNKNOWN</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="dict_dict-regions-france">
    <xsl:param name="key"/>
    <xsl:choose>
      <xsl:when test="$key='01'">
        <xsl:text>Guadeloupe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='1'">
        <xsl:text>Guadeloupe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='02'">
        <xsl:text>Martinique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='2'">
        <xsl:text>Martinique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='03'">
        <xsl:text>Guyane</xsl:text>
      </xsl:when>
      <xsl:when test="$key='3'">
        <xsl:text>Guyane</xsl:text>
      </xsl:when>
      <xsl:when test="$key='04'">
        <xsl:text>Réunion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='4'">
        <xsl:text>Réunion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11'">
        <xsl:text>Île-de-France</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21'">
        <xsl:text>Champagne-Ardenne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22'">
        <xsl:text>Picardie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23'">
        <xsl:text>Haute-Normandie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24'">
        <xsl:text>Centre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='25'">
        <xsl:text>Basse-Normandie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='26'">
        <xsl:text>Bourgogne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31'">
        <xsl:text>Nord-Pas-de-Calais</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41'">
        <xsl:text>Lorraine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42'">
        <xsl:text>Alsace</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43'">
        <xsl:text>Franche-Comté</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52'">
        <xsl:text>Pays de la Loire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53'">
        <xsl:text>Bretagne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54'">
        <xsl:text>Poitou-Charentes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='72'">
        <xsl:text>Aquitaine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='73'">
        <xsl:text>Midi-Pyrénées</xsl:text>
      </xsl:when>
      <xsl:when test="$key='74'">
        <xsl:text>Limousin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='82'">
        <xsl:text>Rhône-Alpes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='83'">
        <xsl:text>Auvergne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='91'">
        <xsl:text>Languedoc-Roussillon</xsl:text>
      </xsl:when>
      <xsl:when test="$key='93'">
        <xsl:text>Provence-Alpes-Côte d'Azur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94'">
        <xsl:text>Corse</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>UNKNOWN</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="dict_dict-pays">
    <xsl:param name="key"/>
    <xsl:choose>
      <xsl:when test="$key='AF'">
        <xsl:text>AFGHANISTAN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='ZA'">
        <xsl:text>AFRIQUE DU SUD</xsl:text>
      </xsl:when>
      <xsl:when test="$key='AL'">
        <xsl:text>ALBANIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='DZ'">
        <xsl:text>ALGÉRIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='DE'">
        <xsl:text>ALLEMAGNE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='AD'">
        <xsl:text>ANDORRE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='AO'">
        <xsl:text>ANGOLA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='AI'">
        <xsl:text>ANGUILLA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='AQ'">
        <xsl:text>ANTARCTIQUE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='AG'">
        <xsl:text>ANTIGUA-ET-BARBUDA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='AN'">
        <xsl:text>ANTILLES NÉERLANDAISES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SA'">
        <xsl:text>ARABIE SAOUDITE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='AR'">
        <xsl:text>ARGENTINE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='AM'">
        <xsl:text>ARMÉNIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='AW'">
        <xsl:text>ARUBA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='AU'">
        <xsl:text>AUSTRALIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='AT'">
        <xsl:text>AUTRICHE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='AZ'">
        <xsl:text>AZERBAÏDJAN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BS'">
        <xsl:text>BAHAMAS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BH'">
        <xsl:text>BAHREÏN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BD'">
        <xsl:text>BANGLADESH</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BB'">
        <xsl:text>BARBADE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BY'">
        <xsl:text>BÉLARUS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BE'">
        <xsl:text>BELGIQUE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BZ'">
        <xsl:text>BELIZE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BJ'">
        <xsl:text>BÉNIN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BM'">
        <xsl:text>BERMUDES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BT'">
        <xsl:text>BHOUTAN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BO'">
        <xsl:text>BOLIVIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BA'">
        <xsl:text>BOSNIE-HERZÉGOVINE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BW'">
        <xsl:text>BOTSWANA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BV'">
        <xsl:text>BOUVET, ÎLE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BR'">
        <xsl:text>BRÉSIL</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BN'">
        <xsl:text>BRUNÉI DARUSSALAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BG'">
        <xsl:text>BULGARIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BF'">
        <xsl:text>BURKINA FASO</xsl:text>
      </xsl:when>
      <xsl:when test="$key='BI'">
        <xsl:text>BURUNDI</xsl:text>
      </xsl:when>
      <xsl:when test="$key='KY'">
        <xsl:text>CAÏMANES, ÎLES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='KH'">
        <xsl:text>CAMBODGE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CM'">
        <xsl:text>CAMEROUN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CA'">
        <xsl:text>CANADA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CV'">
        <xsl:text>CAP-VERT</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CF'">
        <xsl:text>CENTRAFRICAINE, RÉPUBLIQUE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CL'">
        <xsl:text>CHILI</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CN'">
        <xsl:text>CHINE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CX'">
        <xsl:text>CHRISTMAS, ÎLE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CY'">
        <xsl:text>CHYPRE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CC'">
        <xsl:text>COCOS (KEELING), ÎLES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CO'">
        <xsl:text>COLOMBIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='KM'">
        <xsl:text>COMORES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CG'">
        <xsl:text>CONGO</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CD'">
        <xsl:text>CONGO, LA RÉPUBLIQUE DÉMOCRATIQUE DU</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CK'">
        <xsl:text>COOK, ÎLES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='KR'">
        <xsl:text>CORÉE, RÉPUBLIQUE DE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='KP'">
        <xsl:text>CORÉE, RÉPUBLIQUE POPULAIRE DÉMOCRATIQUE DE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CR'">
        <xsl:text>COSTA RICA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CI'">
        <xsl:text>CÔTE D'IVOIRE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='HR'">
        <xsl:text>CROATIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CU'">
        <xsl:text>CUBA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='DK'">
        <xsl:text>DANEMARK</xsl:text>
      </xsl:when>
      <xsl:when test="$key='DJ'">
        <xsl:text>DJIBOUTI</xsl:text>
      </xsl:when>
      <xsl:when test="$key='DO'">
        <xsl:text>DOMINICAINE, RÉPUBLIQUE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='DM'">
        <xsl:text>DOMINIQUE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='EG'">
        <xsl:text>ÉGYPTE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SV'">
        <xsl:text>EL SALVADOR</xsl:text>
      </xsl:when>
      <xsl:when test="$key='AE'">
        <xsl:text>ÉMIRATS ARABES UNIS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='EC'">
        <xsl:text>ÉQUATEUR</xsl:text>
      </xsl:when>
      <xsl:when test="$key='ER'">
        <xsl:text>ÉRYTHRÉE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='ES'">
        <xsl:text>ESPAGNE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='EE'">
        <xsl:text>ESTONIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='US'">
        <xsl:text>ÉTATS-UNIS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='ET'">
        <xsl:text>ÉTHIOPIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='FK'">
        <xsl:text>FALKLAND, ÎLES (MALVINAS)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='FO'">
        <xsl:text>FÉROÉ, ÎLES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='FJ'">
        <xsl:text>FIDJI</xsl:text>
      </xsl:when>
      <xsl:when test="$key='FI'">
        <xsl:text>FINLANDE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='FR'">
        <xsl:text>FRANCE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GA'">
        <xsl:text>GABON</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GM'">
        <xsl:text>GAMBIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GE'">
        <xsl:text>GÉORGIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GS'">
        <xsl:text>GÉORGIE DU SUD ET LES ÎLES SANDWICH DU SUD</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GH'">
        <xsl:text>GHANA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GI'">
        <xsl:text>GIBRALTAR</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GR'">
        <xsl:text>GRÈCE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GD'">
        <xsl:text>GRENADE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GL'">
        <xsl:text>GROENLAND</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GP'">
        <xsl:text>GUADELOUPE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GU'">
        <xsl:text>GUAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GT'">
        <xsl:text>GUATEMALA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GN'">
        <xsl:text>GUINÉE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GW'">
        <xsl:text>GUINÉE-BISSAU</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GQ'">
        <xsl:text>GUINÉE ÉQUATORIALE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GY'">
        <xsl:text>GUYANA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GF'">
        <xsl:text>GUYANE FRANÇAISE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='HT'">
        <xsl:text>HAÏTI</xsl:text>
      </xsl:when>
      <xsl:when test="$key='HM'">
        <xsl:text>HEARD, ÎLE ET MCDONALD, ÎLES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='HN'">
        <xsl:text>HONDURAS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='HK'">
        <xsl:text>HONG-KONG</xsl:text>
      </xsl:when>
      <xsl:when test="$key='HU'">
        <xsl:text>HONGRIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='UM'">
        <xsl:text>ÎLES MINEURES ÉLOIGNÉES DES ÉTATS-UNIS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='VG'">
        <xsl:text>ÎLES VIERGES BRITANNIQUES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='VI'">
        <xsl:text>ÎLES VIERGES DES ÉTATS-UNIS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='IN'">
        <xsl:text>INDE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='ID'">
        <xsl:text>INDONÉSIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='IR'">
        <xsl:text>IRAN, RÉPUBLIQUE ISLAMIQUE D'</xsl:text>
      </xsl:when>
      <xsl:when test="$key='IQ'">
        <xsl:text>IRAQ</xsl:text>
      </xsl:when>
      <xsl:when test="$key='IE'">
        <xsl:text>IRLANDE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='IS'">
        <xsl:text>ISLANDE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='IL'">
        <xsl:text>ISRAËL</xsl:text>
      </xsl:when>
      <xsl:when test="$key='IT'">
        <xsl:text>ITALIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='JM'">
        <xsl:text>JAMAÏQUE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='JP'">
        <xsl:text>JAPON</xsl:text>
      </xsl:when>
      <xsl:when test="$key='JO'">
        <xsl:text>JORDANIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='KZ'">
        <xsl:text>KAZAKHSTAN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='KE'">
        <xsl:text>KENYA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='KG'">
        <xsl:text>KIRGHIZISTAN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='KI'">
        <xsl:text>KIRIBATI</xsl:text>
      </xsl:when>
      <xsl:when test="$key='KW'">
        <xsl:text>KOWEÏT</xsl:text>
      </xsl:when>
      <xsl:when test="$key='LA'">
        <xsl:text>LAO, RÉPUBLIQUE DÉMOCRATIQUE POPULAIRE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='LS'">
        <xsl:text>LESOTHO</xsl:text>
      </xsl:when>
      <xsl:when test="$key='LV'">
        <xsl:text>LETTONIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='LB'">
        <xsl:text>LIBAN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='LR'">
        <xsl:text>LIBÉRIA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='LY'">
        <xsl:text>LIBYENNE, JAMAHIRIYA ARABE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='LI'">
        <xsl:text>LIECHTENSTEIN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='LT'">
        <xsl:text>LITUANIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='LU'">
        <xsl:text>LUXEMBOURG</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MO'">
        <xsl:text>MACAO</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MK'">
        <xsl:text>MACÉDOINE, L'EX-RÉPUBLIQUE YOUGOSLAVE DE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MG'">
        <xsl:text>MADAGASCAR</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MY'">
        <xsl:text>MALAISIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MW'">
        <xsl:text>MALAWI</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MV'">
        <xsl:text>MALDIVES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='ML'">
        <xsl:text>MALI</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MT'">
        <xsl:text>MALTE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MP'">
        <xsl:text>MARIANNES DU NORD, ÎLES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MA'">
        <xsl:text>MAROC</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MH'">
        <xsl:text>MARSHALL, ÎLES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MQ'">
        <xsl:text>MARTINIQUE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MU'">
        <xsl:text>MAURICE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MR'">
        <xsl:text>MAURITANIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='YT'">
        <xsl:text>MAYOTTE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MX'">
        <xsl:text>MEXIQUE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='FM'">
        <xsl:text>MICRONÉSIE, ÉTATS FÉDÉRÉS DE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MD'">
        <xsl:text>MOLDOVA, RÉPUBLIQUE DE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MC'">
        <xsl:text>MONACO</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MN'">
        <xsl:text>MONGOLIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MS'">
        <xsl:text>MONTSERRAT</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MZ'">
        <xsl:text>MOZAMBIQUE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='MM'">
        <xsl:text>MYANMAR</xsl:text>
      </xsl:when>
      <xsl:when test="$key='NA'">
        <xsl:text>NAMIBIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='NR'">
        <xsl:text>NAURU</xsl:text>
      </xsl:when>
      <xsl:when test="$key='NP'">
        <xsl:text>NÉPAL</xsl:text>
      </xsl:when>
      <xsl:when test="$key='NI'">
        <xsl:text>NICARAGUA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='NE'">
        <xsl:text>NIGER</xsl:text>
      </xsl:when>
      <xsl:when test="$key='NG'">
        <xsl:text>NIGÉRIA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='NU'">
        <xsl:text>NIUÉ</xsl:text>
      </xsl:when>
      <xsl:when test="$key='NF'">
        <xsl:text>NORFOLK, ÎLE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='NO'">
        <xsl:text>NORVÈGE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='NC'">
        <xsl:text>NOUVELLE-CALÉDONIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='NZ'">
        <xsl:text>NOUVELLE-ZÉLANDE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='IO'">
        <xsl:text>OCÉAN INDIEN, TERRITOIRE BRITANNIQUE DE L'</xsl:text>
      </xsl:when>
      <xsl:when test="$key='OM'">
        <xsl:text>OMAN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='UG'">
        <xsl:text>OUGANDA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='UZ'">
        <xsl:text>OUZBÉKISTAN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='PK'">
        <xsl:text>PAKISTAN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='PW'">
        <xsl:text>PALAOS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='PS'">
        <xsl:text>PALESTINIEN OCCUPÉ, TERRITOIRE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='PA'">
        <xsl:text>PANAMA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='PG'">
        <xsl:text>PAPOUASIE-NOUVELLE-GUINÉE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='PY'">
        <xsl:text>PARAGUAY</xsl:text>
      </xsl:when>
      <xsl:when test="$key='NL'">
        <xsl:text>PAYS-BAS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='PE'">
        <xsl:text>PÉROU</xsl:text>
      </xsl:when>
      <xsl:when test="$key='PH'">
        <xsl:text>PHILIPPINES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='PN'">
        <xsl:text>PITCAIRN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='PL'">
        <xsl:text>POLOGNE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='PF'">
        <xsl:text>POLYNÉSIE FRANÇAISE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='PR'">
        <xsl:text>PORTO RICO</xsl:text>
      </xsl:when>
      <xsl:when test="$key='PT'">
        <xsl:text>PORTUGAL</xsl:text>
      </xsl:when>
      <xsl:when test="$key='QA'">
        <xsl:text>QATAR</xsl:text>
      </xsl:when>
      <xsl:when test="$key='RE'">
        <xsl:text>RÉUNION</xsl:text>
      </xsl:when>
      <xsl:when test="$key='RO'">
        <xsl:text>ROUMANIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='GB'">
        <xsl:text>ROYAUME-UNI</xsl:text>
      </xsl:when>
      <xsl:when test="$key='RU'">
        <xsl:text>RUSSIE, FÉDÉRATION DE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='RW'">
        <xsl:text>RWANDA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='EH'">
        <xsl:text>SAHARA OCCIDENTAL</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SH'">
        <xsl:text>SAINTE-HÉLÈNE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='LC'">
        <xsl:text>SAINTE-LUCIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='KN'">
        <xsl:text>SAINT-KITTS-ET-NEVIS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SM'">
        <xsl:text>SAINT-MARIN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='PM'">
        <xsl:text>SAINT-PIERRE-ET-MIQUELON</xsl:text>
      </xsl:when>
      <xsl:when test="$key='VA'">
        <xsl:text>SAINT-SIÈGE (ÉTAT DE LA CITÉ DU VATICAN)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='VC'">
        <xsl:text>SAINT-VINCENT-ET-LES GRENADINES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SB'">
        <xsl:text>SALOMON, ÎLES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='WS'">
        <xsl:text>SAMOA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='AS'">
        <xsl:text>SAMOA AMÉRICAINES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='ST'">
        <xsl:text>SAO TOMÉ-ET-PRINCIPE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SN'">
        <xsl:text>SÉNÉGAL</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SC'">
        <xsl:text>SEYCHELLES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SL'">
        <xsl:text>SIERRA LEONE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SG'">
        <xsl:text>SINGAPOUR</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SK'">
        <xsl:text>SLOVAQUIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SI'">
        <xsl:text>SLOVÉNIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SO'">
        <xsl:text>SOMALIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SD'">
        <xsl:text>SOUDAN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='LK'">
        <xsl:text>SRI LANKA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SE'">
        <xsl:text>SUÈDE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CH'">
        <xsl:text>SUISSE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SR'">
        <xsl:text>SURINAME</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SJ'">
        <xsl:text>SVALBARD ET ÎLE JAN MAYEN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SZ'">
        <xsl:text>SWAZILAND</xsl:text>
      </xsl:when>
      <xsl:when test="$key='SY'">
        <xsl:text>SYRIENNE, RÉPUBLIQUE ARABE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='TJ'">
        <xsl:text>TADJIKISTAN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='TW'">
        <xsl:text>TAÏWAN, PROVINCE DE CHINE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='TZ'">
        <xsl:text>TANZANIE, RÉPUBLIQUE-UNIE DE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='TD'">
        <xsl:text>TCHAD</xsl:text>
      </xsl:when>
      <xsl:when test="$key='CZ'">
        <xsl:text>TCHÈQUE, RÉPUBLIQUE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='TF'">
        <xsl:text>TERRES AUSTRALES FRANÇAISES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='TH'">
        <xsl:text>THAÏLANDE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='TL'">
        <xsl:text>TIMOR-LESTE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='TG'">
        <xsl:text>TOGO</xsl:text>
      </xsl:when>
      <xsl:when test="$key='TK'">
        <xsl:text>TOKELAU</xsl:text>
      </xsl:when>
      <xsl:when test="$key='TO'">
        <xsl:text>TONGA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='TT'">
        <xsl:text>TRINITÉ-ET-TOBAGO</xsl:text>
      </xsl:when>
      <xsl:when test="$key='TN'">
        <xsl:text>TUNISIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='TM'">
        <xsl:text>TURKMÉNISTAN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='TC'">
        <xsl:text>TURKS ET CAÏQUES, ÎLES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='TR'">
        <xsl:text>TURQUIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='TV'">
        <xsl:text>TUVALU</xsl:text>
      </xsl:when>
      <xsl:when test="$key='UA'">
        <xsl:text>UKRAINE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='UY'">
        <xsl:text>URUGUAY</xsl:text>
      </xsl:when>
      <xsl:when test="$key='VU'">
        <xsl:text>VANUATU</xsl:text>
      </xsl:when>
      <xsl:when test="$key='VE'">
        <xsl:text>VENEZUELA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='VN'">
        <xsl:text>VIET NAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='WF'">
        <xsl:text>WALLIS ET FUTUNA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='YE'">
        <xsl:text>YÉMEN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='YU'">
        <xsl:text>YOUGOSLAVIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='ZM'">
        <xsl:text>ZAMBIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='ZW'">
        <xsl:text>ZIMBABWE</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>UNKNOWN</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="dict_dict-NSF">
    <xsl:param name="key"/>
    <xsl:choose>
      <xsl:when test="$key='100'">
        <xsl:text>Formations générales</xsl:text>
      </xsl:when>
      <xsl:when test="$key='110'">
        <xsl:text>Spécialités pluriscientifiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='111'">
        <xsl:text>Physique-chimie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='112'">
        <xsl:text>Chimie-biologie, biochimie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='113'">
        <xsl:text>Sciences naturelles, biologie-géologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='114'">
        <xsl:text>Mathématiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='115'">
        <xsl:text>Physique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='116'">
        <xsl:text>Chimie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='117'">
        <xsl:text>Sciences de la terre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='118'">
        <xsl:text>Sciences de la vie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='120'">
        <xsl:text>Spécialités pluridisciplinaires, sciences humaines et droit</xsl:text>
      </xsl:when>
      <xsl:when test="$key='121'">
        <xsl:text>Géographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='122'">
        <xsl:text>Economie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='123'">
        <xsl:text>Sciences sociales (y compris démographie, anthropologie)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='124'">
        <xsl:text>Psychologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='125'">
        <xsl:text>Linguistique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='156'">
        <xsl:text>Histoire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='127'">
        <xsl:text>Philosophie, éthique et théologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='128'">
        <xsl:text>Droit, sciences politiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='130'">
        <xsl:text>Spécialités littéraires et artistiques plurivalentes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='131'">
        <xsl:text>Français, littérature et civilisation française</xsl:text>
      </xsl:when>
      <xsl:when test="$key='132'">
        <xsl:text>Arts plastiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='133'">
        <xsl:text>Musique, arts du spectacle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='134'">
        <xsl:text>Autres disciplines artistiques et spécialités artistiques plurivalentes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='135'">
        <xsl:text>Langues et civilisations anciennes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='136'">
        <xsl:text>Langues vivantes, civilisations étrangères et régionales</xsl:text>
      </xsl:when>
      <xsl:when test="$key='200'">
        <xsl:text>Technologies industrielles fondamentales</xsl:text>
      </xsl:when>
      <xsl:when test="$key='201'">
        <xsl:text>Technologies de commandes des transformations industrielles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='210'">
        <xsl:text>Spécialités plurivalentes de l'agronomie et de l'agriculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='211'">
        <xsl:text>Productions végétales, cultures spécialisées et protection des cultures</xsl:text>
      </xsl:when>
      <xsl:when test="$key='212'">
        <xsl:text>Productions animales, élevage spécialisé, aquaculture, soins aux animaux (y compris vétérinaire)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='213'">
        <xsl:text>Forêts, espaces naturels, faune sauvage, pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='214'">
        <xsl:text>Aménagement paysager (parcs, jardins, espaces verts, terrains de sport)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='220'">
        <xsl:text>Spécialités pluritechnologiques des transformations</xsl:text>
      </xsl:when>
      <xsl:when test="$key='221'">
        <xsl:text>Agro-alimentaire, alimentation, cuisine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='222'">
        <xsl:text>Transformations chimiques et apparentées (y compris industrie pharmaceutique)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='223'">
        <xsl:text>Métallurgie (y compris sidérurgie, fonderie, non ferreux...)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='224'">
        <xsl:text>Matériaux de construction, verre, céramique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='225'">
        <xsl:text>Plasturgie, matériaux composites</xsl:text>
      </xsl:when>
      <xsl:when test="$key='226'">
        <xsl:text>Papier, carton</xsl:text>
      </xsl:when>
      <xsl:when test="$key='227'">
        <xsl:text>Energie, génie climatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='230'">
        <xsl:text>Spécialités pluritechnologiques génie civil, construction, bois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='231'">
        <xsl:text>Mines et carrières, génie civil, topographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='232'">
        <xsl:text>Bâtiment : construction et couverture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='233'">
        <xsl:text>Bâtiment : finitions</xsl:text>
      </xsl:when>
      <xsl:when test="$key='234'">
        <xsl:text>Travail du bois et de l'ameublement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='240'">
        <xsl:text>Spécialités pluritechnologiques matériaux souples</xsl:text>
      </xsl:when>
      <xsl:when test="$key='241'">
        <xsl:text>Textile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='242'">
        <xsl:text>Habillement (y.c mode, couture)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='243'">
        <xsl:text>Cuirs et peaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='250'">
        <xsl:text>Spécialités pluritechnologiques mécanique-électricité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='251'">
        <xsl:text>Mécanique générale et de précision, usinage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='252'">
        <xsl:text>Moteurs et mécanique auto</xsl:text>
      </xsl:when>
      <xsl:when test="$key='253'">
        <xsl:text>Mécanique aéronautique et spatiale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='254'">
        <xsl:text>Structures métalliques (y compris soudure, carrosserie, coque bateau, cellule avion)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='255'">
        <xsl:text>Electricité, électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='300'">
        <xsl:text>Spécialités plurivalentes des services</xsl:text>
      </xsl:when>
      <xsl:when test="$key='310'">
        <xsl:text>Spécialités plurivalentes des échanges et de la gestion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='311'">
        <xsl:text>Transports, manutention, magasinage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='312'">
        <xsl:text>Commerce, vente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='313'">
        <xsl:text>Finances, banque, assurances, immobilier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='314'">
        <xsl:text>Comptabilité, gestion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='315'">
        <xsl:text>Ressources humaines, gestion du personnel, gestion de l'emploi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='320'">
        <xsl:text>Spécialités plurivalentes de la communication et de l'information</xsl:text>
      </xsl:when>
      <xsl:when test="$key='321'">
        <xsl:text>Journalisme et communication</xsl:text>
      </xsl:when>
      <xsl:when test="$key='322'">
        <xsl:text>Techniques de l'imprimerie et de l'édition</xsl:text>
      </xsl:when>
      <xsl:when test="$key='323'">
        <xsl:text>Techniques de l'image et du son, métiers connexes du spectacle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='324'">
        <xsl:text>Secrétariat, bureautique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='325'">
        <xsl:text>Documentation, bibliothèque, administration des données</xsl:text>
      </xsl:when>
      <xsl:when test="$key='326'">
        <xsl:text>Informatique, traitement de l'information, réseaux de transmission</xsl:text>
      </xsl:when>
      <xsl:when test="$key='330'">
        <xsl:text>Spécialités plurivalentes des services aux personnes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='331'">
        <xsl:text>Santé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='332'">
        <xsl:text>Travail social</xsl:text>
      </xsl:when>
      <xsl:when test="$key='333'">
        <xsl:text>Enseignement, formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='334'">
        <xsl:text>Accueil, hôtellerie, tourisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='335'">
        <xsl:text>Animation sportive, culturelle et de loisirs</xsl:text>
      </xsl:when>
      <xsl:when test="$key='336'">
        <xsl:text>Coiffure, esthétique et autres spécialités de services aux personnes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='340'">
        <xsl:text>Spécialités plurivalentes des services à la collectivité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='341'">
        <xsl:text>Aménagement du territoire, urbanisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='342'">
        <xsl:text>Développement et protection du patrimoine culturel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='343'">
        <xsl:text>Nettoyage, assainissement, protection de l'environnement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='344'">
        <xsl:text>Sécurité des biens et des personnes, police, surveillance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='345'">
        <xsl:text>Application des droits et statuts des personnes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='346'">
        <xsl:text>Spécialités militaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='410'">
        <xsl:text>Spécialités concernant plusieurs capacités</xsl:text>
      </xsl:when>
      <xsl:when test="$key='411'">
        <xsl:text>Pratiques sportives (y compris : arts martiaux)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='412'">
        <xsl:text>Développement des capacités mentales, apprentissage de base</xsl:text>
      </xsl:when>
      <xsl:when test="$key='413'">
        <xsl:text>Développement des capacités comportementales et relationnelles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='414'">
        <xsl:text>Développement des capacités individuelles d'organisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='415'">
        <xsl:text>Développement des capacités d'orientation, d'insertion ou de réinsertion sociales et professionnelles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='421'">
        <xsl:text>Jeux et activités spécifiques de loisirs</xsl:text>
      </xsl:when>
      <xsl:when test="$key='422'">
        <xsl:text>Economie et activités domestiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='423'">
        <xsl:text>Vie familiale, vie sociale et autres formations au développement personnel</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>UNKNOWN</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="dict_dict-ROME">
    <xsl:param name="key"/>
    <xsl:choose>
      <xsl:when test="$key='41117'">
        <xsl:text>Aide agricole saisonnier/aide agricole saisonnière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41121'">
        <xsl:text>Eleveur/éleveuse de bétail sur sol</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41122'">
        <xsl:text>Eleveur-soigneur/éleveuse-soigneuse de chevaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41123'">
        <xsl:text>Eleveur/éleveuse en production laitière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41124'">
        <xsl:text>Eleveur/éleveuse hors sol</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41125'">
        <xsl:text>Eleveur spécialisé/éleveuse spécialisée en productions rares</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41131'">
        <xsl:text>Polyculteur-éleveur/polycultrice-éleveuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41141'">
        <xsl:text>Collecteur/collectrice d'espèces sauvages</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41211'">
        <xsl:text>Aquaculteur/aquacultrice</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41212'">
        <xsl:text>Matelot a la pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41221'">
        <xsl:text>Marin de la navigation maritime</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41222'">
        <xsl:text>Marin de la navigation fluviale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42111'">
        <xsl:text>Assistant/assistante des travaux publics et du gros oeuvre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42112'">
        <xsl:text>Ouvrier/ouvrière des travaux publics</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42113'">
        <xsl:text>Ouvrier/ouvrière du béton</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42114'">
        <xsl:text>Ouvrier/ouvrière de la maçonnerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42121'">
        <xsl:text>Monteur/monteuse en structures métalliques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42122'">
        <xsl:text>Monteur/monteuse en structures bois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42123'">
        <xsl:text>Couvreur/couvreuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42124'">
        <xsl:text>Ouvrier/ouvrière de l'étanchéité et de l'isolation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42131'">
        <xsl:text>Ouvrier/ouvrière de l'extraction solide (minerai, minéraux...)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42132'">
        <xsl:text>Ouvrier/ouvrière de l'extraction liquide et gazeuse (pétrole, eau...)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42211'">
        <xsl:text>Electricien/électricienne du bâtiment et des travaux publics</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42212'">
        <xsl:text>Installateur/installatrice d'équipements sanitaires et thermiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42221'">
        <xsl:text>Poseur/poseuse de fermetures menuisées</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42222'">
        <xsl:text>Monteur/monteuse plaquiste en agencements</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42231'">
        <xsl:text>Poseur/poseuse de revêtements rigides</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42232'">
        <xsl:text>Poseur/poseuse de revêtements souples</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42233'">
        <xsl:text>Peintre en bâtiment</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43111'">
        <xsl:text>Conducteur/conductrice de transport de particuliers</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43112'">
        <xsl:text>Conducteur/conductrice de transport en commun (réseau routier)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43113'">
        <xsl:text>Conducteur-livreur/conductrice-livreuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43114'">
        <xsl:text>Conducteur/conductrice de transport de marchandises (réseau routier)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43121'">
        <xsl:text>Conducteur/conductrice sur réseau guide</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43211'">
        <xsl:text>Conducteur/conductrice d'engins de chantier du BTP, du génie civil et de l'exploitation des carrières</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43212'">
        <xsl:text>Conducteur/conductrice d'engins d'exploitation agricole et forestière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43213'">
        <xsl:text>Conducteur/conductrice d'engins de traction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43221'">
        <xsl:text>Conducteur/conductrice d'engins de levage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43311'">
        <xsl:text>Agent/agente du stockage et de la répartition de marchandises</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43312'">
        <xsl:text>Agent/agente de manipulation et de déplacement des charges</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43313'">
        <xsl:text>Déménageur/déménageuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43314'">
        <xsl:text>Agent/agente de routage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43315'">
        <xsl:text>Agent distributeur/agente distributrice</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43321'">
        <xsl:text>Agent/agente de manoeuvre du réseau ferré</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43322'">
        <xsl:text>Personnel du mouvement (transport ferroviaire)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43323'">
        <xsl:text>Agent/agente de remontée filoguidée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43331'">
        <xsl:text>Personnel administratif de la circulation internationale des marchandises</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43332'">
        <xsl:text>Affréteur/affréteuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43333'">
        <xsl:text>Litigeur/litigeuse transport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43411'">
        <xsl:text>Accompagnateur/accompagnatrice tourisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43412'">
        <xsl:text>Agent/agente de contrôle des transports en commun</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43413'">
        <xsl:text>Personnel navigant commercial de l'aviation civile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43414'">
        <xsl:text>Agent/agente d'escale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44111'">
        <xsl:text>Agent/agente d'usinage des métaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33223'">
        <xsl:text>Concepteur-animateur/conceptrice-animatrice développement de produits d'assurances</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33224'">
        <xsl:text>Inspecteur technico-administratif/inspectrice technico-administrative d'assurances</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33225'">
        <xsl:text>Inspecteur délégué aux indemnisations en assurances/Inspectrice déléguée aux indemnisations en assurances</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33226'">
        <xsl:text>Chargé/chargée d'études actuarielles en assurances</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33231'">
        <xsl:text>Chargé/chargée de gestion globale en immobilier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33232'">
        <xsl:text>Transacteur/transactrice en immobilier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33233'">
        <xsl:text>Chargé/chargée d'opérations immobilières</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33311'">
        <xsl:text>Dirigeant/dirigeante de pme/pmi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33312'">
        <xsl:text>Cadre dirigeant/cadre dirigeante de la fonction publique (et assimilé)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33313'">
        <xsl:text>Cadre d'état-major de grande entreprise privée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41111'">
        <xsl:text>Cultivateur/cultivatrice en grandes cultures</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41112'">
        <xsl:text>Maraicher-horticulteur/maraichère-horticultrice</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41113'">
        <xsl:text>Jardinier/jardinière d'espaces verts</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41114'">
        <xsl:text>Arboriculteur-viticulteur/arboricultrice-viticultrice</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41115'">
        <xsl:text>Sylviculteur/sylvicultrice</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41116'">
        <xsl:text>Bûcheron</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24221'">
        <xsl:text>Conseiller/conseillère en information médicale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24311'">
        <xsl:text>Masseur/masseuse kinésithérapeute</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24312'">
        <xsl:text>Ergothérapeute</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24313'">
        <xsl:text>Psychomotricien/psychomotricienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24314'">
        <xsl:text>Orthophoniste</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24315'">
        <xsl:text>Orthoptiste</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24316'">
        <xsl:text>Pédicure-podologue</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24317'">
        <xsl:text>Diététicien/diététicienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24321'">
        <xsl:text>Audioprothésiste</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24322'">
        <xsl:text>Opticien-lunetier/opticienne-lunetière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24323'">
        <xsl:text>Prothésiste dentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24324'">
        <xsl:text>Prothésiste-orthésiste</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31111'">
        <xsl:text>Médecin praticien</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31112'">
        <xsl:text>Chirurgien dentiste/chirurgienne dentiste</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31113'">
        <xsl:text>Psychologue clinicien/psychologue clinicienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31114'">
        <xsl:text>Sage-femme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31121'">
        <xsl:text>Spécialiste conseil de la santé publique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31131'">
        <xsl:text>Vétérinaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31211'">
        <xsl:text>Biologiste médical</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31221'">
        <xsl:text>Pharmacien/pharmacienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32111'">
        <xsl:text>Cadre de la comptabilité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32112'">
        <xsl:text>Cadre de l'audit et du contrôle comptable et financier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32113'">
        <xsl:text>Responsable administratif et financier/responsable administrative et financière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32114'">
        <xsl:text>Cadre financier spécialisé/cadre financière spécialisée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32115'">
        <xsl:text>Analyste de gestion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32121'">
        <xsl:text>Cadre de la gestion des ressources humaines</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32122'">
        <xsl:text>Responsable de formation en entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32131'">
        <xsl:text>Responsable en organisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32141'">
        <xsl:text>Juriste</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32142'">
        <xsl:text>Collaborateur/collaboratrice juridique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32151'">
        <xsl:text>Chargé/chargée d'analyses et de développement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32152'">
        <xsl:text>Chargé/chargée d'études et de recherche en sciences de l'homme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32161'">
        <xsl:text>Cadre charge/cadre chargée de la sécurité publique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32162'">
        <xsl:text>Cadre responsable des ressources publiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32163'">
        <xsl:text>Cadre responsable de l'utilisation des fonds publics</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32164'">
        <xsl:text>Cadre chargé/cadre chargée de l'application des droits des personnes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32165'">
        <xsl:text>Cadre technico-administratif/cadre technico-administrative des services au public</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32171'">
        <xsl:text>Cadre juridique garant/cadre juridique garante des règles institutionnelles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32172'">
        <xsl:text>Cadre responsable de la mise en oeuvre de la politique des pouvoirs publics</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32211'">
        <xsl:text>Rédacteur/rédactrice de presse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32212'">
        <xsl:text>Créateur/créatrice de support de communication visuelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32213'">
        <xsl:text>Chargé/chargée de communication</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32214'">
        <xsl:text>Spécialiste de la gestion de l'information</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32221'">
        <xsl:text>Responsable de campagne publicitaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32222'">
        <xsl:text>Responsable du plan média</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32231'">
        <xsl:text>Coordinateur/coordinatrice d'édition</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32241'">
        <xsl:text>Traducteur/traductrice, interprète</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32311'">
        <xsl:text>Informaticien/informaticienne d'exploitation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32321'">
        <xsl:text>Informaticien/informaticienne d'étude</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32331'">
        <xsl:text>Informaticien expert/informaticienne experte</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32341'">
        <xsl:text>Organisateur informaticien/organisatrice informaticienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33111'">
        <xsl:text>Acheteur industriel/acheteuse industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33112'">
        <xsl:text>Responsable de la stratégie commerciale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33113'">
        <xsl:text>Responsable des ventes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33114'">
        <xsl:text>Chef de produit</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33115'">
        <xsl:text>Cadre de gestion administrative des ventes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33121'">
        <xsl:text>Marchandiseur/marchandiseuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33122'">
        <xsl:text>Acheteur/acheteuse du commerce</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33123'">
        <xsl:text>Chef de département commercial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33124'">
        <xsl:text>Directeur/directrice de magasin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33211'">
        <xsl:text>Conseiller financier/conseillère financière bancaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33212'">
        <xsl:text>Attache commercial/attachée commerciale bancaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33213'">
        <xsl:text>Responsable d'exploitation bancaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33214'">
        <xsl:text>Concepteur/conceptrice gestionnaire de produits bancaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33215'">
        <xsl:text>Conseiller/conseillère en crédit bancaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33216'">
        <xsl:text>Opérateur/opératrice sur marchés de capitaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33221'">
        <xsl:text>Responsable d'exploitation en assurances</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33222'">
        <xsl:text>Agent général, courtier/agente générale, courtière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23131'">
        <xsl:text>Animateur/animatrice généraliste de loisirs</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23132'">
        <xsl:text>Animateur/animatrice spécialiste d'activités culturelles et techniques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23133'">
        <xsl:text>Animateur/animatrice spécialiste d'activités sportives</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23141'">
        <xsl:text>Professionnel/professionnelle d'activités sportives</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23151'">
        <xsl:text>Educateur-intervenant éducatif/éducatrice-intervenante éducative</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23211'">
        <xsl:text>Conseiller/conseillère en développement local</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23221'">
        <xsl:text>Conseiller/conseillère en emploi et insertion professionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24111'">
        <xsl:text>Aide-soignant/aide-soignante</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24121'">
        <xsl:text>Infirmier/infirmière généraliste</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24122'">
        <xsl:text>Infirmier/infirmière de service spécialisé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24131'">
        <xsl:text>Cadre des services paramédicaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24211'">
        <xsl:text>Technicien/technicienne en imagerie médicale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24212'">
        <xsl:text>Technicien/technicienne de laboratoire d'analyses médicales</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24213'">
        <xsl:text>Préparateur/préparatrice en pharmacie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44141'">
        <xsl:text>Agent/agente de traitement thermique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44142'">
        <xsl:text>Agent/agente de traitement de surface</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44143'">
        <xsl:text>Stratifieur-mouliste/stratifieuse-mouliste</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44151'">
        <xsl:text>Contrôleur/contrôleuse de fabrication de la construction mécanique et du travail des métaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44211'">
        <xsl:text>Opérateur/opératrice sur machines automatiques en production électrique et électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44212'">
        <xsl:text>Interconnecteur/interconnectrice en matériel électrique et électromécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44213'">
        <xsl:text>Interconnecteur/interconnectrice en électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44214'">
        <xsl:text>Bobinier/bobiniére de la construction électrique et électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44221'">
        <xsl:text>Contrôleur/contrôleuse en électricité et électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44311'">
        <xsl:text>Mécanicien/mécanicienne de maintenance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44312'">
        <xsl:text>Affûteur/affûteuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44313'">
        <xsl:text>Régleur/régleuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44314'">
        <xsl:text>Maintenicien/maintenicienne en mécanique aéronautique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44315'">
        <xsl:text>Maintenicien/maintenicienne en mécanique maritime</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44316'">
        <xsl:text>Mécanicien/mécanicienne d'engins de chantier, de levage et manutention et de machines agricoles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44321'">
        <xsl:text>Mécanicien/mécanicienne de véhicules particuliers et industriels</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44322'">
        <xsl:text>Mécanicien/mécanicienne en motocycles, matériels d'entretien et de loisirs</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44323'">
        <xsl:text>Réparateur/réparatrice en carrosserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44324'">
        <xsl:text>Maintenicien/maintenicienne en microsystèmes horlogers et photographiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44331'">
        <xsl:text>Electricien/électricienne de maintenance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44332'">
        <xsl:text>Maintenicien/maintenicienne en instruments de bord, équipements électriques et électroniques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44341'">
        <xsl:text>Polymaintenicien/polymaintenicienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45111'">
        <xsl:text>Pilote d'installation des industries chimiques et de production d'énergie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45112'">
        <xsl:text>Opérateur/opératrice sur appareils de transformation physique ou chimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45113'">
        <xsl:text>Opérateur/opératrice sur machines de formage des matières plastiques et du caoutchouc</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45121'">
        <xsl:text>Pilote d'installation des industries agroalimentaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45122'">
        <xsl:text>Opérateur/opératrice sur machines et appareils de fabrication des industries agroalimentaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45211'">
        <xsl:text>Pilote d'installation de production des métaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45212'">
        <xsl:text>Opérateur/opératrice de production des métaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45213'">
        <xsl:text>Opérateur/opératrice sur machines de première transformation des métaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45221'">
        <xsl:text>Pilote d'installation de production de matière verrière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45222'">
        <xsl:text>Opérateur/opératrice de formage du verre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45231'">
        <xsl:text>Pilote d'installation de production cimentière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45232'">
        <xsl:text>Opérateur/opératrice de production de céramique et de matériaux de construction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45311'">
        <xsl:text>Opérateur/opératrice de production de panneaux à base de bois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45321'">
        <xsl:text>Opérateur/opératrice de production des pâtes à papier et à carton</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45322'">
        <xsl:text>Opérateur/opératrice de production de papier-carton</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45411'">
        <xsl:text>Opérateur/opératrice sur machines de finition, contrôle et conditionnement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45412'">
        <xsl:text>Agent/agente main de finition, contrôle et conditionnement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45413'">
        <xsl:text>Opérateur/opératrice de laboratoire des industries de process</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45414'">
        <xsl:text>Agent/agente de traitements dépolluants</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45421'">
        <xsl:text>Modeleur/modeleuse-mouliste</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46111'">
        <xsl:text>Trieur-classeur/trieuse-classeuse des industries des matériaux souples</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46112'">
        <xsl:text>Teinturier-coloriste/teinturiere-coloriste des industries des matériaux souples</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46113'">
        <xsl:text>Préparateur/préparatrice de produits des industries des matériaux souples</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46114'">
        <xsl:text>Patronnier-gradeur/patronnière-gradeuse des industries des matériaux souples</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46115'">
        <xsl:text>Opérateur/opératrice d'atelier de coupe des industries des matériaux souples</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46116'">
        <xsl:text>Apprêteur/apprêteuse des industries des matériaux souples</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46121'">
        <xsl:text>Conducteur/conductrice de machines de filature textile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46122'">
        <xsl:text>Conducteur/conductrice de machines de fabrication de produits textiles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46123'">
        <xsl:text>Opérateur/opératrice de tannerie-mégisserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46124'">
        <xsl:text>Opérateur/opératrice d'assemblage-montage des industries des cuirs et peaux et matériaux associés</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46125'">
        <xsl:text>Opérateur/opératrice d'assemblage-montage des industries de l'habillement et autres fabrications à base d'étoffes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46131'">
        <xsl:text>Conducteur/conductrice de machines d'ennoblissement textile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46132'">
        <xsl:text>Opérateur/opératrice de finition des industries des matériaux souples</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46133'">
        <xsl:text>Visiteur-contrôleur/visiteuse-contrôleuse des industries des matériaux souples</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46134'">
        <xsl:text>Opérateur/opératrice d'entretien des articles textiles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44112'">
        <xsl:text>Agent/agente de découpage des métaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44113'">
        <xsl:text>Conducteur/conductrice d'équipement de formage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44114'">
        <xsl:text>Chaudronnier-tolier/chaudronnière-tolière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44121'">
        <xsl:text>Opérateur-régleur/opératrice-régleuse sur machine-outil</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44131'">
        <xsl:text>Agent/agente de montage-assemblage de la construction mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44132'">
        <xsl:text>Soudeur/soudeuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44133'">
        <xsl:text>Charpentier/charpentière en structures métalliques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44134'">
        <xsl:text>Tuyauteur industriel/tuyauteuse industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44135'">
        <xsl:text>Ajusteur-mécanicien/ajusteuse-mécanicienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13311'">
        <xsl:text>Employé/employée de café, bar-brasserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13321'">
        <xsl:text>Exploitant/exploitante de café, bar-brasserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14111'">
        <xsl:text>Employé/employée de libre-service</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14112'">
        <xsl:text>Hôte/hôtesse de caisse de libre-service</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14113'">
        <xsl:text>Chef de rayon produits frais</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14114'">
        <xsl:text>Chef de rayon hors produits frais</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14115'">
        <xsl:text>Responsable de caisses</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14121'">
        <xsl:text>Caissier/caissière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14122'">
        <xsl:text>Animateur/animatrice de vente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14123'">
        <xsl:text>Employé/employée de station-service</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14211'">
        <xsl:text>Vendeur/vendeuse en produits frais (commerce de gros)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14212'">
        <xsl:text>Vendeur/vendeuse en produits frais (commerce de détail)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14213'">
        <xsl:text>Vendeur/vendeuse en alimentation générale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14221'">
        <xsl:text>Vendeur/vendeuse en produits utilitaires (outillage, bricolage, droguerie...)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14222'">
        <xsl:text>Vendeur/vendeuse en équipement du foyer</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14223'">
        <xsl:text>Vendeur/vendeuse en équipement de la personne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14224'">
        <xsl:text>Vendeur/vendeuse en articles de sport et de loisirs de plein air</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14225'">
        <xsl:text>Vendeur/vendeuse en produits culturels et ludiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14226'">
        <xsl:text>Vendeur/vendeuse en articles de luxe (non alimentaires)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14227'">
        <xsl:text>Fleuriste</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14228'">
        <xsl:text>Antiquaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14231'">
        <xsl:text>Télévendeur/télévendeuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14232'">
        <xsl:text>Technicien/technicienne de la vente à distance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14311'">
        <xsl:text>Attaché commercial/attachée commerciale en biens d'équipement professionnels</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14312'">
        <xsl:text>Attaché commercial/attachée commerciale en biens intermédiaires et matières premières</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14313'">
        <xsl:text>Attaché commercial/attachée commerciale en biens de consommation auprès des entreprises</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14314'">
        <xsl:text>Attaché commercial/attachée commerciale en services auprès des entreprises</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14321'">
        <xsl:text>Représentant/représentante à domicile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14322'">
        <xsl:text>Représentant/représentante en véhicules</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21111'">
        <xsl:text>Artiste plasticien/artiste plasticienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21112'">
        <xsl:text>Auteur-écrivain</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21121'">
        <xsl:text>Stylicien industriel/stylicienne industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21122'">
        <xsl:text>Aménageur/aménageuse d'espace intérieur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21131'">
        <xsl:text>Photographe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21211'">
        <xsl:text>Artiste dramatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21212'">
        <xsl:text>Artiste de la musique et du chant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21213'">
        <xsl:text>Artiste de la danse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21214'">
        <xsl:text>Artiste du cirque et du music-hall</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21215'">
        <xsl:text>Professionnel/professionnelle de la mise en scène et de la réalisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21216'">
        <xsl:text>Animateur-présentateur/animatrice-présentatrice</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21217'">
        <xsl:text>Présentateur/présentatrice de modèles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21221'">
        <xsl:text>Professionnel/professionnelle du son</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21222'">
        <xsl:text>Professionnel/professionnelle de l'image</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21223'">
        <xsl:text>Professionnel/professionnelle de l'éclairage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21224'">
        <xsl:text>Professionnel/professionnelle du décor et des accessoires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21225'">
        <xsl:text>Professionnel/professionnelle du costume et de l'habillage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21226'">
        <xsl:text>Professionnel/professionnelle de la coiffure et du maquillage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21227'">
        <xsl:text>Professionnel/professionnelle du montage de l'image et du son</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21231'">
        <xsl:text>Professionnel/professionnelle de la production des spectacles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21232'">
        <xsl:text>Agent/agente de promotion des artistes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21241'">
        <xsl:text>Opérateur/opératrice d'attractions</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21242'">
        <xsl:text>Employé/employée des jeux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21243'">
        <xsl:text>Exploitant/exploitante d'équipement de loisirs et de sport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22111'">
        <xsl:text>Enseignant/enseignante des écoles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22121'">
        <xsl:text>Enseignant/enseignante d'enseignement général</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22122'">
        <xsl:text>Enseignant/enseignante d'enseignement technique (agricole, professionnel, technologique)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22131'">
        <xsl:text>Enseignant chercheur/enseignante chercheuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22141'">
        <xsl:text>Personnel d'éducation et de surveillance d'établissement d'enseignement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22142'">
        <xsl:text>Administrateur/administratrice d'école maternelle ou primaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22143'">
        <xsl:text>Administrateur/administratrice d'établissement d'enseignement secondaire et supérieur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22151'">
        <xsl:text>Inspecteur/inspectrice de l'enseignement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22211'">
        <xsl:text>Formateur/formatrice</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22212'">
        <xsl:text>Conseiller/conseillère en formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22213'">
        <xsl:text>Responsable pédagogique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22214'">
        <xsl:text>Consultant/consultante en formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22215'">
        <xsl:text>Concepteur-organisateur/conceptrice-organisatrice en formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22221'">
        <xsl:text>Instructeur/instructrice en conduite de véhicules a moteur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23111'">
        <xsl:text>Informateur social/informatrice sociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23112'">
        <xsl:text>Intervenant/intervenante d'action sociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23121'">
        <xsl:text>Chargé/chargée de protection des biens et des personnes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13221'">
        <xsl:text>Employé polyvalent/employée polyvalente de restauration</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13222'">
        <xsl:text>Serveur/serveuse en restauration</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13223'">
        <xsl:text>Sommelier/sommelière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13224'">
        <xsl:text>Barman/barmaid</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13231'">
        <xsl:text>Chef de cuisine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13232'">
        <xsl:text>Maître/maîtresse d'hôtel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13233'">
        <xsl:text>Responsable de restauration de collectivité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13234'">
        <xsl:text>Directeur/directrice de restaurant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13235'">
        <xsl:text>Exploitant/exploitante de restaurant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61'">
        <xsl:text>Agents de maîtrise, techniciens et cadres techniques hors industrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11111'">
        <xsl:text>Employé/employée de ménage à domicile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11112'">
        <xsl:text>Intervenant/intervenante à domicile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11113'">
        <xsl:text>Intervenant/intervenante auprès d'enfants</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11121'">
        <xsl:text>Assistant/assistante en cabinet médical</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11122'">
        <xsl:text>Agent/agente de service de collectivité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11131'">
        <xsl:text>Assistant/assistante de coiffure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11132'">
        <xsl:text>Coiffeur/coiffeuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11133'">
        <xsl:text>Esthéticien-cosméticien/esthéticienne-cosméticienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11141'">
        <xsl:text>Employé/employée technique des services funéraires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11151'">
        <xsl:text>Auxiliaire de soins aux animaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11211'">
        <xsl:text>Nettoyeur/nettoyeuse de locaux et de surfaces</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11212'">
        <xsl:text>Laveur de vitres spécialisé/laveuse de vitres spécialisée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11213'">
        <xsl:text>Agent/agente d'entretien et de nettoyage urbain</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11214'">
        <xsl:text>Agent/agente d'entretien et d'assainissement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11221'">
        <xsl:text>Agent/agente de gardiennage et d'entretien</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11222'">
        <xsl:text>Agent/agente de sécurité et de surveillance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11311'">
        <xsl:text>Agent/agente de la sécurité et de l'ordre public</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11312'">
        <xsl:text>Technicien/technicienne de la sécurité et de l'ordre public</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12111'">
        <xsl:text>Distributeur/distributrice messagerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12112'">
        <xsl:text>Agent/agente d'accueil</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12113'">
        <xsl:text>Agent/agente d'enquêtes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12121'">
        <xsl:text>Agent administratif/agente administrative d'entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12122'">
        <xsl:text>Transcripteur/transcriptrice dactylographe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12131'">
        <xsl:text>Secrétaire bureautique polyvalent/polyvalente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12132'">
        <xsl:text>Secrétaire bureautique spécialisé/spécialisée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12133'">
        <xsl:text>Assistant/assistante de direction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12141'">
        <xsl:text>Technicien/technicienne des services administratifs</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12142'">
        <xsl:text>Technicien/technicienne des services comptables</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12151'">
        <xsl:text>Agent/agente d'application des règles financières publiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12152'">
        <xsl:text>Contrôleur/contrôleuse de la régularité des finances publiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12153'">
        <xsl:text>Contrôleur/contrôleuse de l'utilisation des fonds publics</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12161'">
        <xsl:text>Agent administratif/agente administrative des services au public</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12162'">
        <xsl:text>Contrôleur/contrôleuse de la défense des droits des personnes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12163'">
        <xsl:text>Technicien administratif/technicienne administrative des services au public</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12211'">
        <xsl:text>Agent administratif/agente administrative des opérations bancaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12212'">
        <xsl:text>Agent/agente de banque contact clientèle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12213'">
        <xsl:text>Conseiller/conseillère de clientèle bancaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12221'">
        <xsl:text>Agent administratif/agente administrative d'assurances</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12222'">
        <xsl:text>Rédacteur/rédactrice juridique en assurances</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12223'">
        <xsl:text>Conseiller/conseillère en assurances</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12224'">
        <xsl:text>Technicien souscripteur/technicienne souscriptrice d'assurances</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12231'">
        <xsl:text>Chargé/chargée de groupe d'immeubles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12241'">
        <xsl:text>Technicien/technicienne de vente du tourisme et du transport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12242'">
        <xsl:text>Technicien/technicienne de production du tourisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13111'">
        <xsl:text>Employé/employée d'étage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13121'">
        <xsl:text>Employé/employée du hall</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13122'">
        <xsl:text>Réceptionniste en établissement hôtelier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13131'">
        <xsl:text>Gouvernant/gouvernante en établissement hôtelier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13132'">
        <xsl:text>Concierge d'hôtel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13133'">
        <xsl:text>Chef de réception</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13134'">
        <xsl:text>Directeur/directrice d'hébergement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13135'">
        <xsl:text>Exploitant/exploitante d'hôtel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13211'">
        <xsl:text>Aide de cuisine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13212'">
        <xsl:text>Cuisinier/cuisinière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47'">
        <xsl:text>Personnel de type artisanal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51'">
        <xsl:text>Maîtrise industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52'">
        <xsl:text>Techniciens industriels</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53'">
        <xsl:text>Cadres techniques de l'industrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='454'">
        <xsl:text>Personnel des fonctions transsectorielles aux industries de process</xsl:text>
      </xsl:when>
      <xsl:when test="$key='461'">
        <xsl:text>Personnel des industries des matériaux souples (textile, habillement, cuir)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='462'">
        <xsl:text>Personnel des industries graphiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='463'">
        <xsl:text>Personnel des industries de l'ameublement et du bois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='471'">
        <xsl:text>Personnel de l'alimentation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='472'">
        <xsl:text>Personnel artisanal de l'habillement, du cuir et du textile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='473'">
        <xsl:text>Personnel du travail artisanal des matériaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='474'">
        <xsl:text>Personnel artisanal divers</xsl:text>
      </xsl:when>
      <xsl:when test="$key='511'">
        <xsl:text>Agents d'encadrement de fabrication industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='512'">
        <xsl:text>Agents d'encadrement de maintenance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='521'">
        <xsl:text>Techniciens de préparation de la production</xsl:text>
      </xsl:when>
      <xsl:when test="$key='522'">
        <xsl:text>Techniciens de fabrication, contrôle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='523'">
        <xsl:text>Techniciens d'installation, maintenance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='531'">
        <xsl:text>Cadres techniques de préparation de la production</xsl:text>
      </xsl:when>
      <xsl:when test="$key='532'">
        <xsl:text>Cadres techniques de production</xsl:text>
      </xsl:when>
      <xsl:when test="$key='533'">
        <xsl:text>Cadres technico-commerciaux et de maintenance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='611'">
        <xsl:text>Agents de maîtrise, techniciens et ingénieurs de l'agriculture et de la pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='612'">
        <xsl:text>Agents de maîtrise, techniciens et ingénieurs du bâtiment, des travaux publics et de l'extraction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='613'">
        <xsl:text>Techniciens et cadres du transport et de la logistique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11'">
        <xsl:text>Personnel des services aux personnes et à la collectivité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12'">
        <xsl:text>Personnel des services administratifs et commerciaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13'">
        <xsl:text>Personnel de l'industrie hôtelière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14'">
        <xsl:text>Personnel de la distribution et de la vente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21'">
        <xsl:text>Professionnels des arts et du spectacle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22'">
        <xsl:text>Professionnels de la formation initiale et de la formation continue</xsl:text>
      </xsl:when>
      <xsl:when test="$key='111'">
        <xsl:text>Personnel des services aux personnes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='112'">
        <xsl:text>Personnel des services aux entreprises et aux collectivités</xsl:text>
      </xsl:when>
      <xsl:when test="$key='113'">
        <xsl:text>Personnel de la sécurité publique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='121'">
        <xsl:text>Personnel des services administratifs</xsl:text>
      </xsl:when>
      <xsl:when test="$key='122'">
        <xsl:text>Personnel des services commerciaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='131'">
        <xsl:text>Personnel de l'hôtellerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='132'">
        <xsl:text>Personnel de la restauration</xsl:text>
      </xsl:when>
      <xsl:when test="$key='133'">
        <xsl:text>Personnel de café, bar-brasserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='141'">
        <xsl:text>Personnel de la distribution</xsl:text>
      </xsl:when>
      <xsl:when test="$key='142'">
        <xsl:text>Personnel de la vente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='143'">
        <xsl:text>Personnel des forces de vente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='211'">
        <xsl:text>Professionnels des arts</xsl:text>
      </xsl:when>
      <xsl:when test="$key='212'">
        <xsl:text>Professionnels du spectacle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='221'">
        <xsl:text>Professionnels de la formation initiale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='222'">
        <xsl:text>Professionnels de la formation continue</xsl:text>
      </xsl:when>
      <xsl:when test="$key='231'">
        <xsl:text>Professionnels de l'intervention sociale et culturelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='232'">
        <xsl:text>Professionnels de l'intervention socio-économique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='241'">
        <xsl:text>Professionnels des soins paramédicaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='242'">
        <xsl:text>Professionnels médico-techniques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='243'">
        <xsl:text>Professionnels de la rééducation et de l'appareillage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='311'">
        <xsl:text>Praticiens de la santé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='312'">
        <xsl:text>Praticiens médico-techniques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23'">
        <xsl:text>Professionnels de l'intervention sociale, du développement local et de l'emploi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24'">
        <xsl:text>Professionnels de la santé (professions paramédicales)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31'">
        <xsl:text>Professionnels de la santé (professions médicales)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='321'">
        <xsl:text>Cadres de la gestion administrative</xsl:text>
      </xsl:when>
      <xsl:when test="$key='322'">
        <xsl:text>Professionnels de l'information et de la communication</xsl:text>
      </xsl:when>
      <xsl:when test="$key='323'">
        <xsl:text>Professionnels de l'informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='331'">
        <xsl:text>Cadres de la gestion commerciale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='332'">
        <xsl:text>Cadres de la banque, des assurances et de l'immobilier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='333'">
        <xsl:text>Cadres dirigeants</xsl:text>
      </xsl:when>
      <xsl:when test="$key='411'">
        <xsl:text>Personnel de la production agricole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='412'">
        <xsl:text>Personnel de la pêche et de la navigation maritime et fluviale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='421'">
        <xsl:text>Personnel du gros oeuvre et des travaux publics</xsl:text>
      </xsl:when>
      <xsl:when test="$key='422'">
        <xsl:text>Personnel du second oeuvre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='431'">
        <xsl:text>Conducteurs d'engins de transport terrestre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='432'">
        <xsl:text>Conducteurs d'engins de manoeuvre, de génie civil et agricole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='433'">
        <xsl:text>Personnel de la logistique (manutention, gestion et exploitation des transports)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='434'">
        <xsl:text>Personnel d'accompagnement du transport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='441'">
        <xsl:text>Personnel de la construction mécanique et du travail des métaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='442'">
        <xsl:text>Personnel de la construction électrique et électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='443'">
        <xsl:text>Personnel d'entretien, maintenance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='451'">
        <xsl:text>Conducteurs d'installation des industries chimiques, de production d'énergie et agroalimentaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='452'">
        <xsl:text>Conducteurs d'installation de la métallurgie et des matériaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='453'">
        <xsl:text>Conducteurs d'installation de l'industrie lourde du bois et du papier-carton</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32'">
        <xsl:text>Cadres administratifs et professionnels de l'information et de la communication</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33'">
        <xsl:text>Cadres commerciaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41'">
        <xsl:text>Personnel de l'agriculture et de la pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42'">
        <xsl:text>Personnel du bâtiment, des travaux publics et de l'extraction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43'">
        <xsl:text>Personnel du transport et de la logistique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44'">
        <xsl:text>Personnel de la mécanique, de l'électricité et de l'électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45'">
        <xsl:text>Personnel des industries de process</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46'">
        <xsl:text>Personnel des autres industries (matériaux souples, industries graphiques, ameublement et bois)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52232'">
        <xsl:text>Technicien/technicienne de laboratoire de contrôle de fabrication des industries de process</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52233'">
        <xsl:text>Technicien/technicienne en application industrielle des industries de process</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52234'">
        <xsl:text>Technicien/technicienne en environnement des industries de process</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52235'">
        <xsl:text>Technicien/technicienne d'analyses industrielles des industries de process</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52241'">
        <xsl:text>Technicien/technicienne des industries des matériaux souples</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52242'">
        <xsl:text>Technicien/technicienne des industries graphiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52243'">
        <xsl:text>Technicien/technicienne des industries de l'ameublement et du bois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52311'">
        <xsl:text>Technicien/technicienne d'installation d'équipements industriels et professionnels</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52312'">
        <xsl:text>Installateur-maintenicien/installatrice-maintenicienne en systèmes automatisés</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52313'">
        <xsl:text>Installateur-maintenicien/installatrice-maintenicienne en ascenseurs (et autres systèmes automatiques)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52314'">
        <xsl:text>Inspecteur/inspectrice de mise en conformité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52315'">
        <xsl:text>Rédacteur/rédactrice de notices techniques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52321'">
        <xsl:text>Technicien/technicienne de maintenance en informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52322'">
        <xsl:text>Maintenicien/maintenicienne en matériel bureautique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52331'">
        <xsl:text>Maintenicien/maintenicienne en biens électrodomestiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52332'">
        <xsl:text>Maintenicien/maintenicienne des systèmes thermiques, climatiques et frigorifiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52333'">
        <xsl:text>Maintenicien/maintenicienne en électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53111'">
        <xsl:text>Cadre technique de méthodes-ordonnancement-planification</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53121'">
        <xsl:text>Cadre technique d'études scientifiques et de recherche fondamentale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53122'">
        <xsl:text>Cadre technique d'études-recherche-développement de l'industrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53131'">
        <xsl:text>Cadre technique de l'environnement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53211'">
        <xsl:text>Cadre technique de la production</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53212'">
        <xsl:text>Cadre technique de contrôle-qualité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53213'">
        <xsl:text>Cadre technique d'hygiène et sécurité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53311'">
        <xsl:text>Cadre technico-commercial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53312'">
        <xsl:text>Ingénieur/ingénieure d'affaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53321'">
        <xsl:text>Cadre technique d'entretien, maintenance, travaux neufs</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61111'">
        <xsl:text>Agent/agente technique agricole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61112'">
        <xsl:text>Conseiller/conseillère d'agriculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61113'">
        <xsl:text>Chargé/chargée d'études et de recherches agricoles et aquacoles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61114'">
        <xsl:text>Chargé/chargée de la protection du patrimoine naturel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61121'">
        <xsl:text>Responsable de culture ou d'élevage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61131'">
        <xsl:text>Cadre pont à la pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61211'">
        <xsl:text>Architecte du BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61221'">
        <xsl:text>Dessinateur/dessinatrice du BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61222'">
        <xsl:text>Géomètre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61223'">
        <xsl:text>Chargé/chargée d'études techniques du BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61224'">
        <xsl:text>Chargé/chargée d'études techniques du sous-sol</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61231'">
        <xsl:text>Chef de chantier du BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61232'">
        <xsl:text>Conducteur/conductrice de travaux du BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61233'">
        <xsl:text>Cadre technique d'exploitation des gisements</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61311'">
        <xsl:text>Responsable logistique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61312'">
        <xsl:text>Responsable d'exploitation des transports routiers de marchandises</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61313'">
        <xsl:text>Responsable d'exploitation des transports routiers de voyageurs</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61321'">
        <xsl:text>Personnel navigant technique de l'aviation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61322'">
        <xsl:text>Personnel d'encadrement de la marine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61331'">
        <xsl:text>Technicien/technicienne de préparation des vols</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61332'">
        <xsl:text>Contrôleur/contrôleuse de la navigation aérienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46231'">
        <xsl:text>Conducteur/conductrice de machines de façonnage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46232'">
        <xsl:text>Opérateur/opératrice d'exécution de façonnage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46241'">
        <xsl:text>Opérateur/opératrice de laboratoire photographique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46242'">
        <xsl:text>Opérateur/opératrice de laboratoire cinématographique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46311'">
        <xsl:text>Opérateur/opératrice de sciage débit</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46321'">
        <xsl:text>Conducteur/conductrice de machine de fabrication des industries de l'ameublement et du bois (et matériaux associés)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46322'">
        <xsl:text>Façonneur/façonneuse bois et matériaux associés (production de série)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46323'">
        <xsl:text>Monteur/monteuse d'ouvrages en bois et matériaux associés (production de série)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47111'">
        <xsl:text>Préparateur/préparatrice en produits de boulangerie-viennoiserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47112'">
        <xsl:text>Préparateur/préparatrice en produits de pâtisserie-confiserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47113'">
        <xsl:text>Employé/employée en terminal de cuisson (boulangerie, viennoiserie)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47121'">
        <xsl:text>Opérateur/opératrice de transformation des viandes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47122'">
        <xsl:text>Préparateur/préparatrice en produits carnés</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47123'">
        <xsl:text>Traiteur-charcutier/traiteuse-charcutière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47124'">
        <xsl:text>Préparateur/préparatrice en produits de la pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47131'">
        <xsl:text>Opérateur/opératrice de fermentation artisanale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47132'">
        <xsl:text>Testeur sensoriel/testeuse sensorielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47141'">
        <xsl:text>Assistant/assistante de fabrication de l'alimentation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47211'">
        <xsl:text>Réparateur-retoucheur/réparatrice-retoucheuse en habillement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47212'">
        <xsl:text>Fabricant/fabricante de vêtements sur mesure ou en petite série</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47213'">
        <xsl:text>Fabricant/fabricante d'articles de chapellerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47214'">
        <xsl:text>Tapissier-décorateur/tapissière-décoratrice en ameublement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47221'">
        <xsl:text>Fabricant/fabricante d'articles en cuir et autres matériaux souples (hors vêtement)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47222'">
        <xsl:text>Réparateur/réparatrice d'articles en cuir et autres matériaux souples</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47231'">
        <xsl:text>Professionnel/professionnelle de l'entretien artisanal des textiles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47232'">
        <xsl:text>Façonnier/façonnière d'ouvrages d'art en fils</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47311'">
        <xsl:text>Métallier artisanal/métallière artisanale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47312'">
        <xsl:text>Professionnel/professionnelle de la bijouterie, orfèvrerie et joaillerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47321'">
        <xsl:text>Verrier/verrière à la main</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47322'">
        <xsl:text>Céramiste à la main</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47331'">
        <xsl:text>Réalisateur/réalisatrice d'ouvrages en bois et matériaux associés</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47332'">
        <xsl:text>Façonnier/façonnière d'ouvrages décoratifs en bois et matériaux associés</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47333'">
        <xsl:text>Finisseur/finisseuse sur bois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47334'">
        <xsl:text>Fabricant/fabricante d'objets de vannerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47341'">
        <xsl:text>Professionnel/professionnelle du travail de la pierre et matériaux associés</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47411'">
        <xsl:text>Professionnel/professionnelle de la gravure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47412'">
        <xsl:text>Professionnel/professionnelle de la dorure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47413'">
        <xsl:text>Professionnel/professionnelle de la reliure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47421'">
        <xsl:text>Peintre finisseur/peintre finisseuse d'art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47431'">
        <xsl:text>Facteur-réparateur/factrice-réparatrice d'instruments de musique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='47432'">
        <xsl:text>Professionnel/professionnelle de la conservation des animaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51111'">
        <xsl:text>Agent/agente d'encadrement de la construction mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51112'">
        <xsl:text>Agent/agente d'encadrement de production électrique et électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51121'">
        <xsl:text>Agent/agente d'encadrement des industries de process</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51131'">
        <xsl:text>Agent/agente d'encadrement des industries des matériaux souples</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51132'">
        <xsl:text>Agent/agente d'encadrement des industries graphiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51133'">
        <xsl:text>Agent/agente d'encadrement des laboratoires photographiques et cinématographiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51134'">
        <xsl:text>Agent/agente d'encadrement des industries de l'ameublement et du bois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51211'">
        <xsl:text>Agent/agente d'encadrement de maintenance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52111'">
        <xsl:text>Technicien/technicienne de méthodes-ordonnancement-planification de l'industrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52121'">
        <xsl:text>Dessinateur-projeteur/dessinatrice-projeteuse de la construction mécanique et du travail des métaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52122'">
        <xsl:text>Dessinateur/dessinatrice de la construction mécanique et du travail des métaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52131'">
        <xsl:text>Technicien/technicienne d'études-recherche-développement en électricité et électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52132'">
        <xsl:text>Dessinateur-projeteur/dessinatrice-projeteuse en électricité et électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52133'">
        <xsl:text>Dessinateur/dessinatrice en électricité et électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52141'">
        <xsl:text>Technicien/technicienne de laboratoire de recherche des industries de process</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52151'">
        <xsl:text>Modéliste des industries des matériaux souples</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52211'">
        <xsl:text>Technicien/technicienne de fabrication de la construction mécanique et du travail des métaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46211'">
        <xsl:text>Préparateur-correcteur/préparatrice-correctrice des industries graphiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46212'">
        <xsl:text>Opérateur/opératrice de composition</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46213'">
        <xsl:text>Préparateur/préparatrice en forme imprimante</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46214'">
        <xsl:text>Photograveur/photograveuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46221'">
        <xsl:text>Conducteur/conductrice de machines d'impression</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46222'">
        <xsl:text>Bobinier-receveur/bobinière-receveuse des industries graphiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52212'">
        <xsl:text>Technicien/technicienne qualité de la construction mécanique et du travail des métaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52221'">
        <xsl:text>Technicien/technicienne de contrôle-essai-qualité en électricité et électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52231'">
        <xsl:text>Technicien/technicienne de production des industries de process</xsl:text>
      </xsl:when>
      <xsl:when test="$key='00'">
        <xsl:text>Famille non définie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='009'">
        <xsl:text>Domaine non défini</xsl:text>
      </xsl:when>
      <xsl:when test="$key='00999'">
        <xsl:text>Métier non défini</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>UNKNOWN</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="dict_dict-FORMACODE">
    <xsl:param name="key"/>
    <xsl:choose>
      <xsl:when test="$key='11001'">
        <xsl:text>théorie graphes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11002'">
        <xsl:text>programmation linéaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11006'">
        <xsl:text>analyse factorielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11007'">
        <xsl:text>plan expérience</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11014'">
        <xsl:text>algorithme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11016'">
        <xsl:text>analyse données</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11017'">
        <xsl:text>statistique appliquée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11018'">
        <xsl:text>représentation graphique statistique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11020'">
        <xsl:text>modèle simulation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11022'">
        <xsl:text>recherche opérationnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11025'">
        <xsl:text>calcul scientifique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11029'">
        <xsl:text>sondage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11031'">
        <xsl:text>modèle mathématique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11033'">
        <xsl:text>mathématiques décision</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11036'">
        <xsl:text>statistique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11037'">
        <xsl:text>série temporelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11038'">
        <xsl:text>probabilité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11040'">
        <xsl:text>mathématiques financières</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11045'">
        <xsl:text>statistique descriptive</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11050'">
        <xsl:text>mathématiques informatiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11052'">
        <xsl:text>mathématiques appliquées</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11054'">
        <xsl:text>mathématiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11057'">
        <xsl:text>statistique inférentielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11058'">
        <xsl:text>logique mathématique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11067'">
        <xsl:text>géométrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11069'">
        <xsl:text>trigonométrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11071'">
        <xsl:text>analyse mathématique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11076'">
        <xsl:text>algèbre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11080'">
        <xsl:text>calcul différentiel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11082'">
        <xsl:text>analyse numérique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11083'">
        <xsl:text>mathématiques mise à niveau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11085'">
        <xsl:text>algèbre linéaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11091'">
        <xsl:text>calcul intégral</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11094'">
        <xsl:text>calcul tensoriel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11095'">
        <xsl:text>calcul matriciel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11097'">
        <xsl:text>algèbre boole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11401'">
        <xsl:text>chromatographie liquide</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11403'">
        <xsl:text>photométrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11404'">
        <xsl:text>microscopie électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11405'">
        <xsl:text>spectrométrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11410'">
        <xsl:text>chromatographie gazeuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11411'">
        <xsl:text>chromatographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11413'">
        <xsl:text>colorimétrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11414'">
        <xsl:text>mesure optique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11417'">
        <xsl:text>résonance magnétique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11421'">
        <xsl:text>thermométrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11428'">
        <xsl:text>physique nucléaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11434'">
        <xsl:text>méthode physique analyse chimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11436'">
        <xsl:text>physique matière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11438'">
        <xsl:text>physique plasma</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11441'">
        <xsl:text>méthode électrochimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11448'">
        <xsl:text>physique solides</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11449'">
        <xsl:text>cristallographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11450'">
        <xsl:text>cryogénie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11452'">
        <xsl:text>thermodynamique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11454'">
        <xsl:text>physique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11457'">
        <xsl:text>magnétisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11461'">
        <xsl:text>thermique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11466'">
        <xsl:text>électromagnétisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11467'">
        <xsl:text>optique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11469'">
        <xsl:text>rayonnement optique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11470'">
        <xsl:text>transfert thermique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11472'">
        <xsl:text>mécanique quantique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11474'">
        <xsl:text>astronomie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11475'">
        <xsl:text>propagation onde</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11477'">
        <xsl:text>compatibilité électromagnétique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11479'">
        <xsl:text>laser</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11481'">
        <xsl:text>photonique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11483'">
        <xsl:text>physique mise à niveau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11486'">
        <xsl:text>acoustique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11489'">
        <xsl:text>sécurité laser</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11494'">
        <xsl:text>astrophysique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11497'">
        <xsl:text>acoustique architecturale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11501'">
        <xsl:text>cristallisation précipitation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11503'">
        <xsl:text>distillation industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11504'">
        <xsl:text>analyseur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11506'">
        <xsl:text>échantillonnage chimie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11510'">
        <xsl:text>filtration</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11512'">
        <xsl:text>procédé séparation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11517'">
        <xsl:text>chimie fine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11521'">
        <xsl:text>séchage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11522'">
        <xsl:text>dosage chimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11523'">
        <xsl:text>analyse chimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11528'">
        <xsl:text>granulation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11530'">
        <xsl:text>essai pharmacologique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11532'">
        <xsl:text>dépoussiérage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11534'">
        <xsl:text>génie chimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11536'">
        <xsl:text>chimie milieu colloïdal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11538'">
        <xsl:text>fluidisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11541'">
        <xsl:text>pharmacologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11547'">
        <xsl:text>qualité chimie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11550'">
        <xsl:text>pétrochimie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11551'">
        <xsl:text>raffinage pétrolier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11554'">
        <xsl:text>chimie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11557'">
        <xsl:text>équipement industrie pétrolière et chimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11559'">
        <xsl:text>sécurité industrie pétrochimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11560'">
        <xsl:text>synthèse organique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11561'">
        <xsl:text>chimie organique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11567'">
        <xsl:text>sécurité industrie chimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11569'">
        <xsl:text>habilitation chimie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11576'">
        <xsl:text>chimie générale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11578'">
        <xsl:text>électrochimie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11580'">
        <xsl:text>chimie agroalimentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11583'">
        <xsl:text>chimie mise à niveau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11588'">
        <xsl:text>radiochimie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11591'">
        <xsl:text>chimie textile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11592'">
        <xsl:text>corrosion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11594'">
        <xsl:text>chimie minérale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11595'">
        <xsl:text>chimie macromoléculaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11596'">
        <xsl:text>photochimie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='11597'">
        <xsl:text>cinétique chimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12002'">
        <xsl:text>malherbologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12003'">
        <xsl:text>biologie végétale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12005'">
        <xsl:text>mycologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12006'">
        <xsl:text>virologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12010'">
        <xsl:text>animal laboratoire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12015'">
        <xsl:text>bactériologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12017'">
        <xsl:text>cytologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12021'">
        <xsl:text>zootechnie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12023'">
        <xsl:text>botanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12026'">
        <xsl:text>microbiologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12028'">
        <xsl:text>anatomie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12030'">
        <xsl:text>entomologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12032'">
        <xsl:text>zoologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12035'">
        <xsl:text>parasitologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12039'">
        <xsl:text>biologie moléculaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12040'">
        <xsl:text>ichtyologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12041'">
        <xsl:text>ornithologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12046'">
        <xsl:text>biologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12049'">
        <xsl:text>génétique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12051'">
        <xsl:text>éthologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12054'">
        <xsl:text>sciences naturelles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12058'">
        <xsl:text>biochimie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12059'">
        <xsl:text>fermentation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12061'">
        <xsl:text>biologie marine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12065'">
        <xsl:text>qualité laboratoire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12067'">
        <xsl:text>biologie mise à niveau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12069'">
        <xsl:text>enzymologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12075'">
        <xsl:text>prévention sécurité biologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12079'">
        <xsl:text>chronobiologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12081'">
        <xsl:text>biotechnologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12083'">
        <xsl:text>taxidermie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12086'">
        <xsl:text>immunologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12087'">
        <xsl:text>physiologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12089'">
        <xsl:text>biophysique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12094'">
        <xsl:text>thanatopraxie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12205'">
        <xsl:text>cartographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12207'">
        <xsl:text>géodésie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12212'">
        <xsl:text>sismologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12225'">
        <xsl:text>topographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12227'">
        <xsl:text>hydrogéologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12232'">
        <xsl:text>SIG</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12233'">
        <xsl:text>géophysique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12237'">
        <xsl:text>exploitation mine carrière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12241'">
        <xsl:text>océanographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12248'">
        <xsl:text>exploitation hydrocarbure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12250'">
        <xsl:text>hydrologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12252'">
        <xsl:text>géographie physique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12254'">
        <xsl:text>sciences de la terre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12256'">
        <xsl:text>prospection géologique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12261'">
        <xsl:text>météorologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12269'">
        <xsl:text>minéralogie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12270'">
        <xsl:text>hygrométrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12273'">
        <xsl:text>géomorphologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12275'">
        <xsl:text>géologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12279'">
        <xsl:text>sédimentologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12281'">
        <xsl:text>pédologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12288'">
        <xsl:text>paléontologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12295'">
        <xsl:text>volcanologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12296'">
        <xsl:text>géochimie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12297'">
        <xsl:text>pétrographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12298'">
        <xsl:text>tectonique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12503'">
        <xsl:text>aménagement équipement collectif</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12504'">
        <xsl:text>aménagement foncier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12505'">
        <xsl:text>POS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12509'">
        <xsl:text>installation classée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12510'">
        <xsl:text>aménagement gestion rivière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12512'">
        <xsl:text>aménagement urbain</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12514'">
        <xsl:text>développement rural</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12516'">
        <xsl:text>aménagement touristique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12518'">
        <xsl:text>droit environnement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12520'">
        <xsl:text>traitement eau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12523'">
        <xsl:text>développement local</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12526'">
        <xsl:text>réhabilitation écologique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12529'">
        <xsl:text>garde parc naturel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12530'">
        <xsl:text>distribution eau potable</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12532'">
        <xsl:text>gestion ressource eau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12534'">
        <xsl:text>aménagement territoire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12538'">
        <xsl:text>parc naturel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12539'">
        <xsl:text>chasse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12540'">
        <xsl:text>station épuration</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12541'">
        <xsl:text>assainissement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12547'">
        <xsl:text>protection milieu naturel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12549'">
        <xsl:text>conservation littoral</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12550'">
        <xsl:text>pollution pétrochimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12554'">
        <xsl:text>environnement aménagement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12556'">
        <xsl:text>écologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12558'">
        <xsl:text>protection forêt</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12560'">
        <xsl:text>réduction bruit</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12562'">
        <xsl:text>pollution</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12569'">
        <xsl:text>garde forestier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12570'">
        <xsl:text>pollution atmosphérique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12573'">
        <xsl:text>écoproduit</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12576'">
        <xsl:text>écoindustrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12578'">
        <xsl:text>animation environnement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12580'">
        <xsl:text>pollution sol</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12582'">
        <xsl:text>collecte traitement déchet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12584'">
        <xsl:text>génie environnement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12585'">
        <xsl:text>conseil environnement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12586'">
        <xsl:text>management environnemental</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12589'">
        <xsl:text>découverte nature</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12591'">
        <xsl:text>traitement déchet hospitalier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12592'">
        <xsl:text>traitement déchet industriel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12593'">
        <xsl:text>traitement déchet ménager</xsl:text>
      </xsl:when>
      <xsl:when test="$key='12598'">
        <xsl:text>environnement agriculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13012'">
        <xsl:text>décentralisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13016'">
        <xsl:text>aide développement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13021'">
        <xsl:text>collectivité territoriale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13024'">
        <xsl:text>institution européenne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13030'">
        <xsl:text>préparation concours administratif</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13032'">
        <xsl:text>politique régionale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13036'">
        <xsl:text>politique internationale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13038'">
        <xsl:text>politique santé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13041'">
        <xsl:text>fonction publique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13049'">
        <xsl:text>planification</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13054'">
        <xsl:text>science politique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13057'">
        <xsl:text>politique économique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13061'">
        <xsl:text>politique environnement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13072'">
        <xsl:text>politique sociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13078'">
        <xsl:text>politique agricole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13080'">
        <xsl:text>politique sociale communautaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13082'">
        <xsl:text>politique communautaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13084'">
        <xsl:text>politique emploi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13087'">
        <xsl:text>institution politique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13089'">
        <xsl:text>politique agricole commune</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13091'">
        <xsl:text>aide communautaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13092'">
        <xsl:text>politique formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13096'">
        <xsl:text>politique culturelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13110'">
        <xsl:text>contrôle gestion publique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13113'">
        <xsl:text>économie membre CE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13115'">
        <xsl:text>connaissance entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13121'">
        <xsl:text>gestion publique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13127'">
        <xsl:text>comptabilité nationale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13129'">
        <xsl:text>prévision économique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13130'">
        <xsl:text>économie monétaire européenne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13133'">
        <xsl:text>microéconomie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13138'">
        <xsl:text>analyse économique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13141'">
        <xsl:text>économie monétaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13149'">
        <xsl:text>économétrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13154'">
        <xsl:text>économie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13158'">
        <xsl:text>économie agricole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13162'">
        <xsl:text>économie nationale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13166'">
        <xsl:text>économie secteur activité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13168'">
        <xsl:text>économie tourisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13171'">
        <xsl:text>économie régionale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13175'">
        <xsl:text>économie sociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13183'">
        <xsl:text>économie internationale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13187'">
        <xsl:text>gestion association</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13192'">
        <xsl:text>communauté européenne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13194'">
        <xsl:text>économie PVD</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13196'">
        <xsl:text>gestion coopérative</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13201'">
        <xsl:text>droit travail pays étranger</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13202'">
        <xsl:text>conseil prud'hommes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13204'">
        <xsl:text>pouvoir disciplinaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13205'">
        <xsl:text>droit sécurité sociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13207'">
        <xsl:text>audit juridique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13209'">
        <xsl:text>marché public</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13213'">
        <xsl:text>inspection travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13214'">
        <xsl:text>droit FPC</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13218'">
        <xsl:text>droit administratif</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13219'">
        <xsl:text>droit pénal national</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13220'">
        <xsl:text>droit syndical</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13222'">
        <xsl:text>droit travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13229'">
        <xsl:text>droit urbanisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13231'">
        <xsl:text>droit comparé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13234'">
        <xsl:text>droit social</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13237'">
        <xsl:text>droit public</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13239'">
        <xsl:text>droit constitutionnel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13243'">
        <xsl:text>droit auteur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13245'">
        <xsl:text>droit propriété industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13246'">
        <xsl:text>droit communautaire immobilier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13249'">
        <xsl:text>droit civique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13250'">
        <xsl:text>constitution société</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13251'">
        <xsl:text>statut organisation sans but lucratif</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13252'">
        <xsl:text>procédure collective</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13253'">
        <xsl:text>droit propriété intellectuelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13254'">
        <xsl:text>droit</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13256'">
        <xsl:text>droit contrat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13258'">
        <xsl:text>contrat international</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13260'">
        <xsl:text>transmission entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13261'">
        <xsl:text>droit affaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13262'">
        <xsl:text>droit sociétés</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13263'">
        <xsl:text>responsabilité dirigeant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13266'">
        <xsl:text>droit judiciaire privé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13267'">
        <xsl:text>droit international</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13268'">
        <xsl:text>droit international privé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13269'">
        <xsl:text>contentieux international</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13270'">
        <xsl:text>contrat commercial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13271'">
        <xsl:text>statut groupement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13272'">
        <xsl:text>droit commercial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13273'">
        <xsl:text>statut société</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13274'">
        <xsl:text>droit privé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13275'">
        <xsl:text>notariat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13277'">
        <xsl:text>droit national étranger</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13278'">
        <xsl:text>droit international public</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13279'">
        <xsl:text>droit maritime</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13280'">
        <xsl:text>recouvrement créance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13281'">
        <xsl:text>statut entreprise individuelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13285'">
        <xsl:text>droit civil</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13286'">
        <xsl:text>droit immobilier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13287'">
        <xsl:text>droit communautaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13288'">
        <xsl:text>forme juridique entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13291'">
        <xsl:text>contentieux commercial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13292'">
        <xsl:text>droit concurrence et consommation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13295'">
        <xsl:text>droit personnes famille</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13296'">
        <xsl:text>responsabilité civile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13297'">
        <xsl:text>bail immobilier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13298'">
        <xsl:text>droit communautaire social</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13299'">
        <xsl:text>droit communautaire affaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13303'">
        <xsl:text>contrôle urssaf</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13304'">
        <xsl:text>contentieux fiscal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13306'">
        <xsl:text>DADS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13307'">
        <xsl:text>liasse fiscale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13310'">
        <xsl:text>taxe foncière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13312'">
        <xsl:text>taxe parafiscale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13313'">
        <xsl:text>plus value</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13315'">
        <xsl:text>audit fiscal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13318'">
        <xsl:text>fiscalité européenne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13321'">
        <xsl:text>fiscalité immobilière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13324'">
        <xsl:text>contrôle fiscal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13326'">
        <xsl:text>imprimé fiscal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13327'">
        <xsl:text>fiscalité internationale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13331'">
        <xsl:text>fiscalité mobilière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13346'">
        <xsl:text>défiscalisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13351'">
        <xsl:text>droit enregistrement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13354'">
        <xsl:text>droit fiscal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13360'">
        <xsl:text>impôt fortune</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13362'">
        <xsl:text>fiscalité particulier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13366'">
        <xsl:text>fiscalité entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13371'">
        <xsl:text>impôt revenu</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13375'">
        <xsl:text>fiscalité groupement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13378'">
        <xsl:text>tva intracommunautaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13379'">
        <xsl:text>taxe professionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13380'">
        <xsl:text>fiscalité non salarié</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13381'">
        <xsl:text>déficit fiscal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13382'">
        <xsl:text>fiscalité société</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13384'">
        <xsl:text>fiscalité association</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13386'">
        <xsl:text>taxe sur salaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13389'">
        <xsl:text>TVA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13392'">
        <xsl:text>régime imposition</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13393'">
        <xsl:text>résultat fiscal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13396'">
        <xsl:text>taxe effort construction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13397'">
        <xsl:text>taxe formation professionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='13398'">
        <xsl:text>taxe apprentissage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14201'">
        <xsl:text>culture civilisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14207'">
        <xsl:text>archéologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14208'">
        <xsl:text>généalogie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14215'">
        <xsl:text>sciences et techniques patrimoine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14216'">
        <xsl:text>histoire art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14220'">
        <xsl:text>éthique médicale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14221'">
        <xsl:text>éthique professionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14223'">
        <xsl:text>géographie humaine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14224'">
        <xsl:text>anthropologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14227'">
        <xsl:text>histoire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14229'">
        <xsl:text>histoire sciences et techniques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14231'">
        <xsl:text>éthique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14232'">
        <xsl:text>philosophie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14235'">
        <xsl:text>ethnologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14238'">
        <xsl:text>numismatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14241'">
        <xsl:text>religion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14254'">
        <xsl:text>sciences humaines</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14256'">
        <xsl:text>sciences sociales</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14258'">
        <xsl:text>démographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14261'">
        <xsl:text>littérature</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14269'">
        <xsl:text>sociologie famille</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14270'">
        <xsl:text>littérature étrangère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14272'">
        <xsl:text>linguistique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14277'">
        <xsl:text>sociologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14279'">
        <xsl:text>sociologie éducation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14284'">
        <xsl:text>méthodologie évaluation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14286'">
        <xsl:text>analyse institutionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14289'">
        <xsl:text>gérontologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14290'">
        <xsl:text>analyse discours</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14293'">
        <xsl:text>analyse systémique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14295'">
        <xsl:text>recherche action</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14297'">
        <xsl:text>criminologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14299'">
        <xsl:text>sociologie travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14403'">
        <xsl:text>neuropsychologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14406'">
        <xsl:text>scénothérapie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14407'">
        <xsl:text>musicothérapie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14411'">
        <xsl:text>psychogérontologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14414'">
        <xsl:text>psychopathologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14420'">
        <xsl:text>psychologie enfant adolescent</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14423'">
        <xsl:text>psychophysiologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14426'">
        <xsl:text>art thérapie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14428'">
        <xsl:text>hippothérapie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14435'">
        <xsl:text>urgence psychologique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14441'">
        <xsl:text>psychopédagogie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14447'">
        <xsl:text>thérapie corporelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14449'">
        <xsl:text>relaxation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14450'">
        <xsl:text>psychologie expérimentale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14454'">
        <xsl:text>psychologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14456'">
        <xsl:text>psychothérapie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14461'">
        <xsl:text>psychologie sociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14469'">
        <xsl:text>thérapie familiale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14472'">
        <xsl:text>psychologie travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14475'">
        <xsl:text>psychanalyse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14477'">
        <xsl:text>thérapie groupe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14484'">
        <xsl:text>morphopsychologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14489'">
        <xsl:text>psychodrame</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14490'">
        <xsl:text>dynamique groupe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14492'">
        <xsl:text>test psychologique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14493'">
        <xsl:text>graphologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14496'">
        <xsl:text>groupe Balint</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14497'">
        <xsl:text>gestalt thérapie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='14498'">
        <xsl:text>analyse transactionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15000'">
        <xsl:text>synthèse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15001'">
        <xsl:text>prise notes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15002'">
        <xsl:text>prise parole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15003'">
        <xsl:text>expression média</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15004'">
        <xsl:text>communication professionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15005'">
        <xsl:text>méthodologie négociation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15006'">
        <xsl:text>relation interculturelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15008'">
        <xsl:text>budget familial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15009'">
        <xsl:text>éducation ménagère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15010'">
        <xsl:text>rapport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15011'">
        <xsl:text>expression écrite</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15012'">
        <xsl:text>expression orale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15013'">
        <xsl:text>PNL</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15016'">
        <xsl:text>gestion conflit interpersonnel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15017'">
        <xsl:text>mécanique familiale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15019'">
        <xsl:text>couture initiation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15021'">
        <xsl:text>atelier écriture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15029'">
        <xsl:text>bricolage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15030'">
        <xsl:text>calcul mise à niveau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15031'">
        <xsl:text>adaptation sociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15034'">
        <xsl:text>relation interpersonnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15036'">
        <xsl:text>lecture rapide</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15038'">
        <xsl:text>vie quotidienne loisir</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15039'">
        <xsl:text>jardinage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15040'">
        <xsl:text>français mise à niveau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15041'">
        <xsl:text>mise à niveau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15043'">
        <xsl:text>alphabétisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15048'">
        <xsl:text>préparation retraite</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15050'">
        <xsl:text>mise à niveau technologique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15054'">
        <xsl:text>développement personnel et professionnel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15058'">
        <xsl:text>culture générale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15059'">
        <xsl:text>connaissance monde actuel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15061'">
        <xsl:text>accompagnement vers emploi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15062'">
        <xsl:text>orientation professionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15066'">
        <xsl:text>efficacité personnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15068'">
        <xsl:text>gestion temps</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15070'">
        <xsl:text>TRE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15073'">
        <xsl:text>préparation examen concours</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15075'">
        <xsl:text>affirmation soi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15078'">
        <xsl:text>intelligence émotionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15079'">
        <xsl:text>valorisation image de soi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15081'">
        <xsl:text>bilan professionnel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15084'">
        <xsl:text>préparation entrée formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15089'">
        <xsl:text>raisonnement logique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15091'">
        <xsl:text>gestion carrière personnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15093'">
        <xsl:text>préparation entrée université</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15094'">
        <xsl:text>préparation entrée grande école</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15096'">
        <xsl:text>développement mémoire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15097'">
        <xsl:text>gestion stress</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15098'">
        <xsl:text>créativité personnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15099'">
        <xsl:text>résolution problème</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15201'">
        <xsl:text>vietnamien</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15202'">
        <xsl:text>langue perse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15203'">
        <xsl:text>anglais tourisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15204'">
        <xsl:text>anglais technique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15205'">
        <xsl:text>anglais commercial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15206'">
        <xsl:text>anglais secrétariat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15207'">
        <xsl:text>espagnol technique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15208'">
        <xsl:text>brésilien</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15209'">
        <xsl:text>espagnol secrétariat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15210'">
        <xsl:text>japonais</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15212'">
        <xsl:text>chinois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15213'">
        <xsl:text>américain</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15214'">
        <xsl:text>anglais appliqué</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15217'">
        <xsl:text>français commercial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15218'">
        <xsl:text>portugais</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15219'">
        <xsl:text>espagnol commercial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15220'">
        <xsl:text>coréen</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15221'">
        <xsl:text>langue orientale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15222'">
        <xsl:text>turc</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15226'">
        <xsl:text>français appliqué</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15227'">
        <xsl:text>espagnol tourisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15228'">
        <xsl:text>espagnol</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15229'">
        <xsl:text>espagnol appliqué</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15231'">
        <xsl:text>traduction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15233'">
        <xsl:text>langue africaine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15234'">
        <xsl:text>anglais</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15235'">
        <xsl:text>français langue étrangère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15236'">
        <xsl:text>italien secrétariat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15237'">
        <xsl:text>italien technique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15238'">
        <xsl:text>italien appliqué</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15239'">
        <xsl:text>italien commercial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15240'">
        <xsl:text>langue amérindienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15241'">
        <xsl:text>interprétariat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15247'">
        <xsl:text>italien</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15248'">
        <xsl:text>italien tourisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15249'">
        <xsl:text>roumain</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15250'">
        <xsl:text>langue des signes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15251'">
        <xsl:text>braille</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15254'">
        <xsl:text>langues</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15259'">
        <xsl:text>arabe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15260'">
        <xsl:text>grec moderne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15269'">
        <xsl:text>espéranto</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15270'">
        <xsl:text>langue dialecte régional</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15274'">
        <xsl:text>finnois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15277'">
        <xsl:text>néerlandais</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15278'">
        <xsl:text>langue indonésienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15279'">
        <xsl:text>yiddish</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15280'">
        <xsl:text>langue ancienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15282'">
        <xsl:text>langue scandinave</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15283'">
        <xsl:text>russe appliqué</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15284'">
        <xsl:text>polonais</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15285'">
        <xsl:text>langue slave</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15286'">
        <xsl:text>hongrois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15287'">
        <xsl:text>allemand</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15288'">
        <xsl:text>allemand appliqué</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15289'">
        <xsl:text>allemand technique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15290'">
        <xsl:text>hébreu</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15291'">
        <xsl:text>danois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15292'">
        <xsl:text>suédois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15293'">
        <xsl:text>norvégien</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15294'">
        <xsl:text>russe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15295'">
        <xsl:text>tchèque</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15296'">
        <xsl:text>serbocroate</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15297'">
        <xsl:text>allemand tourisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15298'">
        <xsl:text>allemand secrétariat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15299'">
        <xsl:text>allemand commercial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15400'">
        <xsl:text>escalade</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15403'">
        <xsl:text>lutte</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15404'">
        <xsl:text>aikido</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15405'">
        <xsl:text>judo</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15406'">
        <xsl:text>volley-ball</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15407'">
        <xsl:text>tennis</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15408'">
        <xsl:text>tennis table</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15409'">
        <xsl:text>squash</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15410'">
        <xsl:text>alpinisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15411'">
        <xsl:text>ski</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15412'">
        <xsl:text>sport montagne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15413'">
        <xsl:text>boxe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15415'">
        <xsl:text>karaté</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15416'">
        <xsl:text>handball</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15419'">
        <xsl:text>basket-ball</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15420'">
        <xsl:text>parachutisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15421'">
        <xsl:text>vol libre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15422'">
        <xsl:text>char à voile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15424'">
        <xsl:text>sport combat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15425'">
        <xsl:text>escrime</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15426'">
        <xsl:text>football</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15429'">
        <xsl:text>rugby</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15430'">
        <xsl:text>vol à voile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15435'">
        <xsl:text>arbitrage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15436'">
        <xsl:text>éducation sportive</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15437'">
        <xsl:text>sport balle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15438'">
        <xsl:text>badminton</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15439'">
        <xsl:text>golf</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15440'">
        <xsl:text>ULM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15442'">
        <xsl:text>sport aérien</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15447'">
        <xsl:text>matériel sport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15448'">
        <xsl:text>installation sportive</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15450'">
        <xsl:text>yoga</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15452'">
        <xsl:text>gymnastique douce</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15454'">
        <xsl:text>activité physique et sportive</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15457'">
        <xsl:text>gestion sport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15459'">
        <xsl:text>sport adapté</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15460'">
        <xsl:text>ski nautique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15462'">
        <xsl:text>cyclisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15466'">
        <xsl:text>activité physique pour tous</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15469'">
        <xsl:text>randonnée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15470'">
        <xsl:text>planche à voile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15472'">
        <xsl:text>sport nautique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15473'">
        <xsl:text>spéléologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15474'">
        <xsl:text>sport glace</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15477'">
        <xsl:text>remise en forme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15479'">
        <xsl:text>équitation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15480'">
        <xsl:text>plongée sous-marine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15483'">
        <xsl:text>canoë kayak</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15484'">
        <xsl:text>patinage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15487'">
        <xsl:text>gymnastique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15489'">
        <xsl:text>athlétisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15490'">
        <xsl:text>éducation natation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15491'">
        <xsl:text>natation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15492'">
        <xsl:text>aviron</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15493'">
        <xsl:text>voile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15494'">
        <xsl:text>hockey</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15495'">
        <xsl:text>tir arc</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15496'">
        <xsl:text>expression corporelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15497'">
        <xsl:text>sport mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15498'">
        <xsl:text>musculation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='15499'">
        <xsl:text>haltérophilie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21008'">
        <xsl:text>insémination animale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21011'">
        <xsl:text>machinisme agricole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21015'">
        <xsl:text>cuniculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21017'">
        <xsl:text>élevage animal fourrure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21018'">
        <xsl:text>médecine vétérinaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21019'">
        <xsl:text>sériciculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21020'">
        <xsl:text>gestion parcelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21023'">
        <xsl:text>bâtiment agricole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21024'">
        <xsl:text>sécurité agriculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21026'">
        <xsl:text>élevage gibier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21029'">
        <xsl:text>héliciculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21032'">
        <xsl:text>agroéquipement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21039'">
        <xsl:text>lombriculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21049'">
        <xsl:text>apiculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21052'">
        <xsl:text>gestion exploitation agricole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21054'">
        <xsl:text>agriculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21056'">
        <xsl:text>élevage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21058'">
        <xsl:text>aviculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21060'">
        <xsl:text>installation agriculteur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21069'">
        <xsl:text>élevage palmipède gras</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21075'">
        <xsl:text>élevage ovin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21078'">
        <xsl:text>élevage bovin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21081'">
        <xsl:text>droit rural</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21083'">
        <xsl:text>vente produit fermier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21084'">
        <xsl:text>commercialisation agricole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21085'">
        <xsl:text>élevage laitier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21086'">
        <xsl:text>élevage caprin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21088'">
        <xsl:text>élevage cheval</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21089'">
        <xsl:text>maréchalerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21092'">
        <xsl:text>groupement agriculteurs</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21097'">
        <xsl:text>élevage porcin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21099'">
        <xsl:text>soin cheval</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21201'">
        <xsl:text>culture irriguée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21203'">
        <xsl:text>tonnellerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21204'">
        <xsl:text>machinisme viticole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21206'">
        <xsl:text>production fourragère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21207'">
        <xsl:text>culture plante médicinale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21208'">
        <xsl:text>culture champignon</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21210'">
        <xsl:text>polyculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21218'">
        <xsl:text>osiériculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21222'">
        <xsl:text>culture serre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21223'">
        <xsl:text>viticulture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21226'">
        <xsl:text>culture spécialisée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21228'">
        <xsl:text>machinisme horticole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21230'">
        <xsl:text>agronomie tropicale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21236'">
        <xsl:text>grande culture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21239'">
        <xsl:text>pépinière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21249'">
        <xsl:text>floriculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21250'">
        <xsl:text>agriculture biologique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21252'">
        <xsl:text>agronomie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21254'">
        <xsl:text>production végétale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21256'">
        <xsl:text>horticulture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21258'">
        <xsl:text>culture légumière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21259'">
        <xsl:text>maraîchage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21268'">
        <xsl:text>production fruit</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21270'">
        <xsl:text>recherche agronomique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21274'">
        <xsl:text>sylviculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21276'">
        <xsl:text>gestion arbre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21277'">
        <xsl:text>travaux paysagers</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21279'">
        <xsl:text>terrain sport engazonné</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21280'">
        <xsl:text>fertilisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21283'">
        <xsl:text>épandage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21285'">
        <xsl:text>exploitation forestière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21289'">
        <xsl:text>jardin historique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21290'">
        <xsl:text>semence</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21291'">
        <xsl:text>analyse terre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21292'">
        <xsl:text>protection phytosanitaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21294'">
        <xsl:text>bûcheronnage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21295'">
        <xsl:text>engin forestier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21296'">
        <xsl:text>scierie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21297'">
        <xsl:text>taille arbre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21298'">
        <xsl:text>maçonnerie paysagère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21299'">
        <xsl:text>arrosage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21312'">
        <xsl:text>garde pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21317'">
        <xsl:text>culture algue</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21319'">
        <xsl:text>crevette</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21320'">
        <xsl:text>mécanique pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21325'">
        <xsl:text>plongée pêche aquaculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21329'">
        <xsl:text>crustacé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21333'">
        <xsl:text>marin pêcheur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21336'">
        <xsl:text>gestion exploitation pêche aquaculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21340'">
        <xsl:text>capitaine pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21347'">
        <xsl:text>aquaculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21349'">
        <xsl:text>pisciculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21352'">
        <xsl:text>pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21354'">
        <xsl:text>pêche aquaculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21360'">
        <xsl:text>patron pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21368'">
        <xsl:text>conchyliculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21376'">
        <xsl:text>mareyage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21381'">
        <xsl:text>lieutenant pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21383'">
        <xsl:text>sécurité pêche aquaculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21385'">
        <xsl:text>marais salant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21387'">
        <xsl:text>mytiliculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21389'">
        <xsl:text>ostréiculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21501'">
        <xsl:text>spiritueux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21503'">
        <xsl:text>beurre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21504'">
        <xsl:text>fromage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21508'">
        <xsl:text>spécialisation pâtisserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21510'">
        <xsl:text>vinification oenologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21511'">
        <xsl:text>dégustation vin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21512'">
        <xsl:text>cidre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21515'">
        <xsl:text>corps gras</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21516'">
        <xsl:text>produit diététique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21517'">
        <xsl:text>épicerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21519'">
        <xsl:text>confiserie chocolaterie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21520'">
        <xsl:text>brasserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21523'">
        <xsl:text>ovoproduit</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21524'">
        <xsl:text>industrie laitière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21527'">
        <xsl:text>industrie sucrière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21528'">
        <xsl:text>pâtisserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21529'">
        <xsl:text>glacerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21530'">
        <xsl:text>boisson non alcoolisée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21532'">
        <xsl:text>boisson</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21538'">
        <xsl:text>boulangerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21539'">
        <xsl:text>spécialisation boulangerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21540'">
        <xsl:text>commercialisation vin spiritueux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21542'">
        <xsl:text>formation formateur agroalimentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21546'">
        <xsl:text>haccp</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21549'">
        <xsl:text>transformation céréale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21551'">
        <xsl:text>commercialisation agroalimentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21554'">
        <xsl:text>agroalimentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21558'">
        <xsl:text>aliment animaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21560'">
        <xsl:text>dégustation alimentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21567'">
        <xsl:text>transformation produit mer</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21570'">
        <xsl:text>hygiène agroalimentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21571'">
        <xsl:text>qualité agroalimentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21573'">
        <xsl:text>conservation alimentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21575'">
        <xsl:text>conditionnement agroalimentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21576'">
        <xsl:text>traiteur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21577'">
        <xsl:text>viande</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21578'">
        <xsl:text>volaillerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21579'">
        <xsl:text>foie gras</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21580'">
        <xsl:text>cuisson sous vide</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21585'">
        <xsl:text>salaison</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21589'">
        <xsl:text>charcuterie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21591'">
        <xsl:text>ionisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21592'">
        <xsl:text>conserve</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21593'">
        <xsl:text>lyophilisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21594'">
        <xsl:text>surgélation congélation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21595'">
        <xsl:text>stérilisation pasteurisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21597'">
        <xsl:text>triperie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21598'">
        <xsl:text>abattage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21599'">
        <xsl:text>boucherie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21601'">
        <xsl:text>métier à tricoter</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21603'">
        <xsl:text>raccoutrage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21605'">
        <xsl:text>blanchiment textile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21606'">
        <xsl:text>teinture textile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21607'">
        <xsl:text>impression textile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21611'">
        <xsl:text>tricotage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21613'">
        <xsl:text>colletage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21617'">
        <xsl:text>apprêt textile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21620'">
        <xsl:text>métier à tisser</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21622'">
        <xsl:text>maille bonneterie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21625'">
        <xsl:text>ennoblissement textile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21627'">
        <xsl:text>création textile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21629'">
        <xsl:text>contrôle textile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21631'">
        <xsl:text>tissage industriel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21634'">
        <xsl:text>machine textile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21638'">
        <xsl:text>qualité textile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21641'">
        <xsl:text>matelas</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21650'">
        <xsl:text>machine filature</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21651'">
        <xsl:text>filature</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21654'">
        <xsl:text>textile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21662'">
        <xsl:text>corderie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21667'">
        <xsl:text>fibre textile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21669'">
        <xsl:text>fibre chimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21670'">
        <xsl:text>guipure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21674'">
        <xsl:text>étoffe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21676'">
        <xsl:text>textile usage technique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21678'">
        <xsl:text>fibre naturelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21680'">
        <xsl:text>broderie mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21691'">
        <xsl:text>tulle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21692'">
        <xsl:text>dentellerie mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21693'">
        <xsl:text>passementerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21694'">
        <xsl:text>tapis</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21695'">
        <xsl:text>non tissé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21696'">
        <xsl:text>tissu</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21697'">
        <xsl:text>bâche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21698'">
        <xsl:text>étoffe maille</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21701'">
        <xsl:text>monitorat confection</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21707'">
        <xsl:text>vêtement protection</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21708'">
        <xsl:text>vêtement enfant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21710'">
        <xsl:text>montage piquage confection</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21711'">
        <xsl:text>encadrement atelier confection</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21712'">
        <xsl:text>finition confection</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21713'">
        <xsl:text>guide machine à coudre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21719'">
        <xsl:text>lingerie corseterie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21723'">
        <xsl:text>machine à coudre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21728'">
        <xsl:text>vêtement féminin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21730'">
        <xsl:text>coupe confection</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21733'">
        <xsl:text>matériel confection</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21736'">
        <xsl:text>vêtement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21739'">
        <xsl:text>vêtement masculin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21740'">
        <xsl:text>repassage confection</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21742'">
        <xsl:text>confection</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21749'">
        <xsl:text>chemiserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21752'">
        <xsl:text>accessoire mode</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21754'">
        <xsl:text>habillement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21757'">
        <xsl:text>couture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21759'">
        <xsl:text>couture flou</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21762'">
        <xsl:text>commercialisation habillement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21766'">
        <xsl:text>essayage retouche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21771'">
        <xsl:text>coupe patronnage gradation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21775'">
        <xsl:text>stylisme habillement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21779'">
        <xsl:text>couture tailleur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21780'">
        <xsl:text>patronnage gradation informatisé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21781'">
        <xsl:text>prototype habillement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21783'">
        <xsl:text>modélisme habillement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21788'">
        <xsl:text>mannequin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21792'">
        <xsl:text>moulage toile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21794'">
        <xsl:text>costume scène</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21796'">
        <xsl:text>collection vêtement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21798'">
        <xsl:text>dessin mode</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21802'">
        <xsl:text>impression cuir</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21803'">
        <xsl:text>collage cuir</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21804'">
        <xsl:text>piquage cuir</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21812'">
        <xsl:text>coupe cuir</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21814'">
        <xsl:text>montage cuir</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21815'">
        <xsl:text>finition cuir peau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21817'">
        <xsl:text>fourrure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21821'">
        <xsl:text>entretien matériel cuir</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21823'">
        <xsl:text>technique confection cuir</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21828'">
        <xsl:text>cuir synthétique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21831'">
        <xsl:text>triage peau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21838'">
        <xsl:text>modélisme cuir</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21842'">
        <xsl:text>tannerie mégisserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21850'">
        <xsl:text>teinture peau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21854'">
        <xsl:text>cuir peau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21859'">
        <xsl:text>podo-orthèse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21860'">
        <xsl:text>qualité cuir peau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21867'">
        <xsl:text>chaussure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21869'">
        <xsl:text>cordonnerie botterie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21871'">
        <xsl:text>cuir ameublement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21879'">
        <xsl:text>injection chaussure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21882'">
        <xsl:text>maroquinerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21884'">
        <xsl:text>sellerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21886'">
        <xsl:text>vêtement cuir</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21888'">
        <xsl:text>ganterie cuir</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21889'">
        <xsl:text>cordonnerie réparation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21893'">
        <xsl:text>sellerie bourrellerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='21895'">
        <xsl:text>sellerie garnissage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22001'">
        <xsl:text>chaussée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22005'">
        <xsl:text>ouvrage béton</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22010'">
        <xsl:text>aménagement route</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22013'">
        <xsl:text>travaux ferroviaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22016'">
        <xsl:text>pont</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22022'">
        <xsl:text>travaux routiers</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22024'">
        <xsl:text>génie civil</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22026'">
        <xsl:text>barrage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22028'">
        <xsl:text>réhabilitation travaux publics</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22042'">
        <xsl:text>tranchée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22048'">
        <xsl:text>travaux portuaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22050'">
        <xsl:text>travaux aéroportuaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22054'">
        <xsl:text>travaux publics</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22062'">
        <xsl:text>VRD</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22067'">
        <xsl:text>travaux immergés</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22069'">
        <xsl:text>canalisation sous-marine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22070'">
        <xsl:text>canalisation réseau extérieur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22076'">
        <xsl:text>travaux souterrains</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22078'">
        <xsl:text>plateforme marine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22082'">
        <xsl:text>pavage pose bordure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22087'">
        <xsl:text>forage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22093'">
        <xsl:text>lecture plan VRD</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22095'">
        <xsl:text>mécanique sol</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22097'">
        <xsl:text>explosif</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22205'">
        <xsl:text>état parasitaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22206'">
        <xsl:text>pathologie construction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22212'">
        <xsl:text>architecture bioclimatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22214'">
        <xsl:text>aménagement handicapé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22217'">
        <xsl:text>droit BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22223'">
        <xsl:text>architecture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22225'">
        <xsl:text>résistance ouvrage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22230'">
        <xsl:text>métré spécialisé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22232'">
        <xsl:text>métré BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22237'">
        <xsl:text>qualité BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22241'">
        <xsl:text>lecture plan BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22248'">
        <xsl:text>gestion entreprise BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22252'">
        <xsl:text>dessin BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22254'">
        <xsl:text>BTP conception organisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22257'">
        <xsl:text>commercialisation BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22259'">
        <xsl:text>marché travaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22265'">
        <xsl:text>risque amiante</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22269'">
        <xsl:text>maîtrise ouvrage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22270'">
        <xsl:text>implantation traçage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22273'">
        <xsl:text>réception travaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22274'">
        <xsl:text>chantier BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22275'">
        <xsl:text>coordination SPS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22276'">
        <xsl:text>risque plomb</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22278'">
        <xsl:text>terminologie BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22279'">
        <xsl:text>maîtrise oeuvre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22283'">
        <xsl:text>préparation chantier BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22286'">
        <xsl:text>matériel chantier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22291'">
        <xsl:text>terrassement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22293'">
        <xsl:text>conduite travaux BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22294'">
        <xsl:text>conduite chantier BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22295'">
        <xsl:text>sécurité BTP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22297'">
        <xsl:text>échafaudage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22301'">
        <xsl:text>enduit maçonnerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22303'">
        <xsl:text>maçonnerie pierre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22304'">
        <xsl:text>cloisonnement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22305'">
        <xsl:text>jointoiement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22306'">
        <xsl:text>escalier maçonnerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22311'">
        <xsl:text>coffrage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22316'">
        <xsl:text>taille pierre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22318'">
        <xsl:text>bardage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22320'">
        <xsl:text>ferraillage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22329'">
        <xsl:text>démolition</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22331'">
        <xsl:text>briquetage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22334'">
        <xsl:text>maçonnerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22339'">
        <xsl:text>soutènement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22348'">
        <xsl:text>fondation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22350'">
        <xsl:text>sol dallage industriel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22354'">
        <xsl:text>bâtiment gros oeuvre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22357'">
        <xsl:text>restauration réhabilitation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22359'">
        <xsl:text>restauration monument</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22361'">
        <xsl:text>plancher</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22369'">
        <xsl:text>restauration patrimoine rural</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22371'">
        <xsl:text>préfabrication</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22374'">
        <xsl:text>construction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22378'">
        <xsl:text>réhabilitation bâtiment</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22379'">
        <xsl:text>restauration patrimoine urbain</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22380'">
        <xsl:text>panneau façade</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22382'">
        <xsl:text>construction piscine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22387'">
        <xsl:text>construction métallique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22392'">
        <xsl:text>construction terre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22395'">
        <xsl:text>construction béton</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22396'">
        <xsl:text>construction bois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22397'">
        <xsl:text>construction mixte</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22398'">
        <xsl:text>dessin construction métallique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22401'">
        <xsl:text>staff</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22402'">
        <xsl:text>stuc</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22403'">
        <xsl:text>plaque plâtre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22405'">
        <xsl:text>parquet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22406'">
        <xsl:text>escalier bois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22410'">
        <xsl:text>pose tissu mural</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22412'">
        <xsl:text>plâtrerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22415'">
        <xsl:text>dessin traçage menuiserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22416'">
        <xsl:text>menuiserie bois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22417'">
        <xsl:text>menuiserie plastique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22420'">
        <xsl:text>marbrerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22423'">
        <xsl:text>pose sol plastique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22429'">
        <xsl:text>couverture ardoise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22430'">
        <xsl:text>pose moquette</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22434'">
        <xsl:text>vitrerie miroiterie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22435'">
        <xsl:text>menuiserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22437'">
        <xsl:text>couverture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22439'">
        <xsl:text>zinguerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22440'">
        <xsl:text>carrelage mosaïque</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22442'">
        <xsl:text>revêtement sol mur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22448'">
        <xsl:text>couverture tuile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22450'">
        <xsl:text>faux plafond</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22454'">
        <xsl:text>bâtiment second oeuvre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22457'">
        <xsl:text>charpente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22459'">
        <xsl:text>charpente bois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22460'">
        <xsl:text>pose papier peint</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22467'">
        <xsl:text>escalier métallique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22468'">
        <xsl:text>menuiserie aluminium</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22469'">
        <xsl:text>charpente métallique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22470'">
        <xsl:text>peinture décorative</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22472'">
        <xsl:text>peinture bâtiment</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22477'">
        <xsl:text>menuiserie métallique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22478'">
        <xsl:text>métallerie serrurerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22480'">
        <xsl:text>peinture intérieure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22485'">
        <xsl:text>rénovation bâtiment</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22486'">
        <xsl:text>maintenance bâtiment</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22490'">
        <xsl:text>façade</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22493'">
        <xsl:text>ascenseur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22494'">
        <xsl:text>escalier mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22498'">
        <xsl:text>huisserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22499'">
        <xsl:text>immotique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22603'">
        <xsl:text>fluide frigorigène</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22604'">
        <xsl:text>froid commercial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22605'">
        <xsl:text>froid industriel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22606'">
        <xsl:text>régulation thermique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22607'">
        <xsl:text>régulation climatisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22610'">
        <xsl:text>traitement eau chaudière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22612'">
        <xsl:text>four industriel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22618'">
        <xsl:text>isolation thermique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22619'">
        <xsl:text>isolation phonique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22622'">
        <xsl:text>thermique industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22624'">
        <xsl:text>installation frigorifique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22630'">
        <xsl:text>chaufferie chaudière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22633'">
        <xsl:text>appareil à pression</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22635'">
        <xsl:text>climatisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22636'">
        <xsl:text>régulation génie climatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22638'">
        <xsl:text>isolation thermique phonique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22642'">
        <xsl:text>génie thermique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22647'">
        <xsl:text>isolation bâtiment</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22649'">
        <xsl:text>étanchéité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22650'">
        <xsl:text>chauffage gaz</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22654'">
        <xsl:text>génie climatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22657'">
        <xsl:text>fumisterie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22659'">
        <xsl:text>prévention risque légionellose</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22663'">
        <xsl:text>sécurité thermique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22671'">
        <xsl:text>chauffage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22677'">
        <xsl:text>plomberie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22679'">
        <xsl:text>canalisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22680'">
        <xsl:text>chauffage urbain</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22683'">
        <xsl:text>pompe chaleur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22685'">
        <xsl:text>installation thermique sanitaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22689'">
        <xsl:text>chauffe eau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22691'">
        <xsl:text>chauffage solaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22692'">
        <xsl:text>chauffage électrique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22693'">
        <xsl:text>chauffage fuel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22694'">
        <xsl:text>installation gaz</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22697'">
        <xsl:text>installation sanitaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22699'">
        <xsl:text>robinetterie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22802'">
        <xsl:text>superalliage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22805'">
        <xsl:text>métal précieux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22806'">
        <xsl:text>aluminium</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22810'">
        <xsl:text>brique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22811'">
        <xsl:text>tuile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22812'">
        <xsl:text>acier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22816'">
        <xsl:text>fer</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22818'">
        <xsl:text>plastique renforcé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22819'">
        <xsl:text>polymère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22820'">
        <xsl:text>verre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22821'">
        <xsl:text>céramique industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22823'">
        <xsl:text>alliage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22825'">
        <xsl:text>métal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22827'">
        <xsl:text>peinture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22828'">
        <xsl:text>matière plastique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22830'">
        <xsl:text>céramique frittée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22833'">
        <xsl:text>matériau adaptatif</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22834'">
        <xsl:text>matériau métallique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22839'">
        <xsl:text>caoutchouc</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22841'">
        <xsl:text>matériau réfractaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22842'">
        <xsl:text>pierre naturelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22846'">
        <xsl:text>matériau supraconducteur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22849'">
        <xsl:text>colle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22851'">
        <xsl:text>ciment</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22852'">
        <xsl:text>matériau construction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22854'">
        <xsl:text>matériau produit chimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22856'">
        <xsl:text>qualité matériau produit chimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22859'">
        <xsl:text>vernis</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22860'">
        <xsl:text>béton armé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22861'">
        <xsl:text>béton</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22865'">
        <xsl:text>sécurité matériau produit chimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22867'">
        <xsl:text>produit chimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22869'">
        <xsl:text>solvant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22871'">
        <xsl:text>matériau composite</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22879'">
        <xsl:text>colorant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22881'">
        <xsl:text>bois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22884'">
        <xsl:text>papier carton</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22885'">
        <xsl:text>cosmétique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22889'">
        <xsl:text>goudron</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22890'">
        <xsl:text>traitement bois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22892'">
        <xsl:text>fibre optique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22895'">
        <xsl:text>parfum</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22896'">
        <xsl:text>tensioactif</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22897'">
        <xsl:text>lubrifiant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22898'">
        <xsl:text>encre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='22899'">
        <xsl:text>engrais chimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23001'">
        <xsl:text>thermoformage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23002'">
        <xsl:text>stratification</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23004'">
        <xsl:text>soudage électrode enrobée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23005'">
        <xsl:text>soudage faisceau électrons</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23006'">
        <xsl:text>soudage oxyacétylénique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23007'">
        <xsl:text>soudage matière plastique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23008'">
        <xsl:text>soudage autre procédé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23009'">
        <xsl:text>soudage laser</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23010'">
        <xsl:text>moulage plastique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23011'">
        <xsl:text>extrusion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23012'">
        <xsl:text>collage matière plastique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23013'">
        <xsl:text>soudage plasma</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23014'">
        <xsl:text>soudage arc</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23015'">
        <xsl:text>soudage TIG</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23016'">
        <xsl:text>soudage MIG MAG</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23017'">
        <xsl:text>soudage par résistance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23021'">
        <xsl:text>plasturgie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23023'">
        <xsl:text>collage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23025'">
        <xsl:text>soudage sous flux électroconducteur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23026'">
        <xsl:text>soudage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23028'">
        <xsl:text>brasage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23029'">
        <xsl:text>pliage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23030'">
        <xsl:text>traitement thermochimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23031'">
        <xsl:text>décoration plastique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23035'">
        <xsl:text>montage assemblage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23037'">
        <xsl:text>soudobrasage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23039'">
        <xsl:text>repoussage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23040'">
        <xsl:text>anodisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23042'">
        <xsl:text>traitement surface</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23044'">
        <xsl:text>sécurité travail matériau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23048'">
        <xsl:text>formage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23049'">
        <xsl:text>forgeage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23051'">
        <xsl:text>émaillage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23052'">
        <xsl:text>décapage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23053'">
        <xsl:text>peinture industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23054'">
        <xsl:text>travail matériau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23058'">
        <xsl:text>étirage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23059'">
        <xsl:text>emboutissage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23060'">
        <xsl:text>métallurgie poudres</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23062'">
        <xsl:text>métallurgie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23064'">
        <xsl:text>qualité travail matériau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23067'">
        <xsl:text>fraisage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23069'">
        <xsl:text>coupage plasma</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23070'">
        <xsl:text>laminage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23071'">
        <xsl:text>sidérurgie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23074'">
        <xsl:text>traitement thermique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23075'">
        <xsl:text>électroérosion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23076'">
        <xsl:text>usinage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23078'">
        <xsl:text>découpage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23079'">
        <xsl:text>coupage laser</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23080'">
        <xsl:text>tréfilage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23081'">
        <xsl:text>moulage métal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23082'">
        <xsl:text>chaudronnerie inox</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23083'">
        <xsl:text>chaudronnerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23084'">
        <xsl:text>tuyauterie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23085'">
        <xsl:text>perçage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23088'">
        <xsl:text>tournage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23089'">
        <xsl:text>décolletage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23090'">
        <xsl:text>fonderie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23091'">
        <xsl:text>aciérie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23092'">
        <xsl:text>chaudronnerie plastique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23093'">
        <xsl:text>traçage chaudronnerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23094'">
        <xsl:text>tôlerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23095'">
        <xsl:text>alésage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23096'">
        <xsl:text>ajustage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23097'">
        <xsl:text>affûtage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23098'">
        <xsl:text>rectification usinage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23099'">
        <xsl:text>filetage mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23506'">
        <xsl:text>mesure mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23512'">
        <xsl:text>oléopneumatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23517'">
        <xsl:text>rupture mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23518'">
        <xsl:text>élasticité matériau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23521'">
        <xsl:text>aérodynamique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23523'">
        <xsl:text>pneumatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23525'">
        <xsl:text>essai mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23529'">
        <xsl:text>plasticité matériau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23530'">
        <xsl:text>aéraulique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23534'">
        <xsl:text>technique vide</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23539'">
        <xsl:text>fiabilité mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23542'">
        <xsl:text>mécanique fluide</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23546'">
        <xsl:text>résistance matériau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23549'">
        <xsl:text>vibration mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23551'">
        <xsl:text>hydraulique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23554'">
        <xsl:text>mécanique théorique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23556'">
        <xsl:text>rhéologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23567'">
        <xsl:text>calcul structure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23569'">
        <xsl:text>mécanique plaque</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23570'">
        <xsl:text>oléohydraulique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23576'">
        <xsl:text>calcul élément fini</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23581'">
        <xsl:text>hydrodynamique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23583'">
        <xsl:text>mécanique surface</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23585'">
        <xsl:text>balistique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23587'">
        <xsl:text>calcul état limite</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23589'">
        <xsl:text>structure hyperstatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23594'">
        <xsl:text>mécanique vol</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23598'">
        <xsl:text>structure isostatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23601'">
        <xsl:text>charpente marine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23602'">
        <xsl:text>entretien marine plaisance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23605'">
        <xsl:text>électronique automobile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23606'">
        <xsl:text>électricité automobile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23607'">
        <xsl:text>peinture carrosserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23608'">
        <xsl:text>pneu</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23609'">
        <xsl:text>hydraulique automobile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23610'">
        <xsl:text>mécanique navale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23613'">
        <xsl:text>construction aéronautique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23615'">
        <xsl:text>réparation véhicule industriel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23617'">
        <xsl:text>carrosserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23619'">
        <xsl:text>suspension</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23622'">
        <xsl:text>construction maintenance navale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23624'">
        <xsl:text>construction aérospatiale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23629'">
        <xsl:text>freinage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23630'">
        <xsl:text>voilerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23635'">
        <xsl:text>mécanique cycle motocycle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23637'">
        <xsl:text>mécanique automobile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23641'">
        <xsl:text>armement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23642'">
        <xsl:text>sécurité mécanique construction réparation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23646'">
        <xsl:text>construction automobile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23649'">
        <xsl:text>contrôle automobile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23650'">
        <xsl:text>optique instrumentale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23652'">
        <xsl:text>transmission mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23654'">
        <xsl:text>mécanique construction réparation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23657'">
        <xsl:text>aménagement véhicule</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23658'">
        <xsl:text>climatisation véhicule</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23660'">
        <xsl:text>armurerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23661'">
        <xsl:text>mécanique précision</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23662'">
        <xsl:text>construction mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23663'">
        <xsl:text>qualité mécanique construction réparation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23670'">
        <xsl:text>horlogerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23673'">
        <xsl:text>ressort</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23675'">
        <xsl:text>mécanique sportive</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23677'">
        <xsl:text>moteur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23679'">
        <xsl:text>allumage carburation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23680'">
        <xsl:text>modelage mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23681'">
        <xsl:text>outillage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23684'">
        <xsl:text>entretien mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23689'">
        <xsl:text>moteur essence</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23690'">
        <xsl:text>coutellerie taillanderie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23692'">
        <xsl:text>dessin construction mécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23694'">
        <xsl:text>lubrification</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23696'">
        <xsl:text>pompe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23697'">
        <xsl:text>injection moteur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='23698'">
        <xsl:text>moteur diesel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24016'">
        <xsl:text>montage câblage électrique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24021'">
        <xsl:text>électroménager</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24024'">
        <xsl:text>mesure électrique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24027'">
        <xsl:text>schéma électrique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24039'">
        <xsl:text>norme électrique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24047'">
        <xsl:text>sécurité électrique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24049'">
        <xsl:text>habilitation électrique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24052'">
        <xsl:text>électromécanique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24054'">
        <xsl:text>électrotechnique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24057'">
        <xsl:text>perturbation électrique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24061'">
        <xsl:text>machine électrique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24066'">
        <xsl:text>installation électrique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24069'">
        <xsl:text>maintenance installation électrique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24070'">
        <xsl:text>moteur électrique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24072'">
        <xsl:text>électrotechnique mise à niveau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24079'">
        <xsl:text>onduleur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24081'">
        <xsl:text>transformateur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24086'">
        <xsl:text>distribution électricité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24089'">
        <xsl:text>éclairage public</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24095'">
        <xsl:text>basse tension</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24096'">
        <xsl:text>moyenne tension</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24097'">
        <xsl:text>haute tension</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24099'">
        <xsl:text>électricité équipement industriel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24101'">
        <xsl:text>radioprotection milieu médical</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24102'">
        <xsl:text>mesure radioactivité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24103'">
        <xsl:text>combustible nucléaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24112'">
        <xsl:text>radioactivité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24114'">
        <xsl:text>retraitement combustible irradié</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24118'">
        <xsl:text>énergie solaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24121'">
        <xsl:text>radioprotection</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24127'">
        <xsl:text>énergie géothermique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24129'">
        <xsl:text>biomasse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24130'">
        <xsl:text>habilitation nucléaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24132'">
        <xsl:text>prévention sécurité nucléaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24134'">
        <xsl:text>énergie nucléaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24136'">
        <xsl:text>énergie éolienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24139'">
        <xsl:text>énergie hydraulique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24141'">
        <xsl:text>cogénération</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24147'">
        <xsl:text>énergie renouvelable</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24154'">
        <xsl:text>énergie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24158'">
        <xsl:text>énergie électrique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24160'">
        <xsl:text>bilan énergétique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24162'">
        <xsl:text>gestion énergie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24167'">
        <xsl:text>hydrocarbure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24170'">
        <xsl:text>conseil énergie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24174'">
        <xsl:text>fuel domestique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24176'">
        <xsl:text>pétrole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24179'">
        <xsl:text>charbon</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24181'">
        <xsl:text>maîtrise énergie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24183'">
        <xsl:text>réglementation thermique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24189'">
        <xsl:text>gaz</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24195'">
        <xsl:text>fuel lourd</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24196'">
        <xsl:text>gazole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24197'">
        <xsl:text>carburant aéronautique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24203'">
        <xsl:text>extranet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24209'">
        <xsl:text>vidéocommunication</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24210'">
        <xsl:text>réseau local</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24211'">
        <xsl:text>réseau large bande</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24213'">
        <xsl:text>intranet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24216'">
        <xsl:text>commutation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24217'">
        <xsl:text>multiplexage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24218'">
        <xsl:text>transmission numérique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24220'">
        <xsl:text>gestion réseau informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24222'">
        <xsl:text>internet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24229'">
        <xsl:text>transmission fibre optique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24231'">
        <xsl:text>réseau informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24232'">
        <xsl:text>interconnexion réseau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24234'">
        <xsl:text>câble coaxial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24237'">
        <xsl:text>protocole télécommunication</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24240'">
        <xsl:text>RNIS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24245'">
        <xsl:text>liaison satellite</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24246'">
        <xsl:text>transmission</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24252'">
        <xsl:text>réseau télécom</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24254'">
        <xsl:text>télécommunication</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24256'">
        <xsl:text>transmission radioélectrique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24259'">
        <xsl:text>modem</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24260'">
        <xsl:text>installation réseau téléphonique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24266'">
        <xsl:text>hyperfréquence</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24267'">
        <xsl:text>communication radiomobile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24268'">
        <xsl:text>transmission données informatiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24270'">
        <xsl:text>équipement télécom</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24273'">
        <xsl:text>architecture réseau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24276'">
        <xsl:text>technique télévision</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24277'">
        <xsl:text>télédétection</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24280'">
        <xsl:text>TRANSPAC</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24284'">
        <xsl:text>architecture client serveur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24286'">
        <xsl:text>antenne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24289'">
        <xsl:text>photo interprétation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24293'">
        <xsl:text>sécurité télécommunication</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24306'">
        <xsl:text>circuit hybride</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24307'">
        <xsl:text>microcontrôleur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24308'">
        <xsl:text>microprocesseur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24314'">
        <xsl:text>optoélectronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24317'">
        <xsl:text>circuit intégré</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24319'">
        <xsl:text>tube électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24322'">
        <xsl:text>montage câblage électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24323'">
        <xsl:text>conception circuit électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24326'">
        <xsl:text>microélectronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24328'">
        <xsl:text>composant actif</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24329'">
        <xsl:text>semiconducteur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24331'">
        <xsl:text>circuit imprimé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24332'">
        <xsl:text>schéma électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24335'">
        <xsl:text>circuit électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24336'">
        <xsl:text>composant électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24338'">
        <xsl:text>composant passif</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24340'">
        <xsl:text>maintenance électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24346'">
        <xsl:text>électronique embarquée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24348'">
        <xsl:text>traitement parole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24351'">
        <xsl:text>mesure électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24354'">
        <xsl:text>électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24356'">
        <xsl:text>traitement signal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24358'">
        <xsl:text>visionique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24360'">
        <xsl:text>qualité électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24369'">
        <xsl:text>lecture optique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24374'">
        <xsl:text>maintenance audiovisuelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24386'">
        <xsl:text>radar</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24387'">
        <xsl:text>électronique puissance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24391'">
        <xsl:text>électronique analogique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24392'">
        <xsl:text>électronique numérique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24396'">
        <xsl:text>matériel électronique sécurité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24398'">
        <xsl:text>variation vitesse électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24404'">
        <xsl:text>commande numérique fraisage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24406'">
        <xsl:text>commande numérique tournage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24407'">
        <xsl:text>programmation MOCN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24413'">
        <xsl:text>centre usinage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24420'">
        <xsl:text>maintenance distributeur automatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24422'">
        <xsl:text>automate programmable</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24425'">
        <xsl:text>MOCN</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24427'">
        <xsl:text>commande processus</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24429'">
        <xsl:text>supervision</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24431'">
        <xsl:text>maintenance système automatisé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24439'">
        <xsl:text>réseau local industriel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24447'">
        <xsl:text>automatisme hydraulique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24451'">
        <xsl:text>robotique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24454'">
        <xsl:text>automatisme informatique industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24457'">
        <xsl:text>automatisme pneumatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24461'">
        <xsl:text>grafcet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24469'">
        <xsl:text>capteur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24470'">
        <xsl:text>gemma</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24472'">
        <xsl:text>automatisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24478'">
        <xsl:text>régulation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24485'">
        <xsl:text>logique câblée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24487'">
        <xsl:text>logique programmée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24489'">
        <xsl:text>système asservi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='24491'">
        <xsl:text>programmation informatique industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30823'">
        <xsl:text>langage ado.net</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30825'">
        <xsl:text>langage power builder</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30827'">
        <xsl:text>langage assembleur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30831'">
        <xsl:text>langage asp.net</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30840'">
        <xsl:text>langage biztalk</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30847'">
        <xsl:text>langage sgml</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30854'">
        <xsl:text>langage informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30857'">
        <xsl:text>langage html</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30860'">
        <xsl:text>langage visual basic.net</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30867'">
        <xsl:text>langage javascript</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30873'">
        <xsl:text>langage cobol</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30881'">
        <xsl:text>langage basic</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30882'">
        <xsl:text>langage c#.net</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30884'">
        <xsl:text>langage turbo pascal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30885'">
        <xsl:text>langage turbo c</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30887'">
        <xsl:text>langage sql</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30896'">
        <xsl:text>langage smalltalk</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30901'">
        <xsl:text>logiciel ms-project</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30903'">
        <xsl:text>logiciel ordicompta</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30904'">
        <xsl:text>logiciel saari</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30905'">
        <xsl:text>logiciel ciel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30906'">
        <xsl:text>logiciel painter</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30907'">
        <xsl:text>logiciel designer</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30910'">
        <xsl:text>logiciel dreamweaver mx</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30912'">
        <xsl:text>logiciel gestion projet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30914'">
        <xsl:text>logiciel maestria</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30915'">
        <xsl:text>logiciel cadds</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30916'">
        <xsl:text>logiciel macpaint</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30917'">
        <xsl:text>logiciel macdraw</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30918'">
        <xsl:text>logiciel autocad</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30920'">
        <xsl:text>logiciel director</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30921'">
        <xsl:text>logiciel frontpage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30924'">
        <xsl:text>logiciel comptabilité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30925'">
        <xsl:text>logiciel catia</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30926'">
        <xsl:text>logiciel dao/cao</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30931'">
        <xsl:text>logiciel page web</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30935'">
        <xsl:text>logiciel euclid</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30938'">
        <xsl:text>logiciel photoshop</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30940'">
        <xsl:text>logiciel internet explorer</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30941'">
        <xsl:text>logiciel navigateur internet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30943'">
        <xsl:text>logiciel aide décision</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30945'">
        <xsl:text>logiciel cfao</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30946'">
        <xsl:text>logiciel traitement image</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30949'">
        <xsl:text>logiciel open access</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30950'">
        <xsl:text>logiciel netscape</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30954'">
        <xsl:text>logiciel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30955'">
        <xsl:text>logiciel graphique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30957'">
        <xsl:text>logiciel intégré</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30958'">
        <xsl:text>logiciel works</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30960'">
        <xsl:text>logiciel outlook</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30962'">
        <xsl:text>logiciel documentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30963'">
        <xsl:text>logiciel gestion intégré</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30964'">
        <xsl:text>logiciel business objects</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30965'">
        <xsl:text>logiciel bureautique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30967'">
        <xsl:text>logiciel tableur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30968'">
        <xsl:text>logiciel lotus</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30970'">
        <xsl:text>logiciel lotus notes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30971'">
        <xsl:text>logiciel gestion messagerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30972'">
        <xsl:text>logiciel informix</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30973'">
        <xsl:text>logiciel sgbd</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30974'">
        <xsl:text>logiciel sap</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30975'">
        <xsl:text>logiciel pao</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30976'">
        <xsl:text>logiciel preao</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30977'">
        <xsl:text>logiciel traitement texte</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30979'">
        <xsl:text>logiciel excel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30980'">
        <xsl:text>logiciel novell netware</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30981'">
        <xsl:text>logiciel gestion réseau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30982'">
        <xsl:text>logiciel dbase</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30983'">
        <xsl:text>logiciel foxpro</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30984'">
        <xsl:text>logiciel illustrator</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30985'">
        <xsl:text>logiciel quark XPress</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30986'">
        <xsl:text>logiciel publisher</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30987'">
        <xsl:text>logiciel power point</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30988'">
        <xsl:text>logiciel word</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30990'">
        <xsl:text>logiciel lan manager</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30992'">
        <xsl:text>logiciel oracle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30993'">
        <xsl:text>logiciel filemaker pro</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30994'">
        <xsl:text>logiciel access</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30995'">
        <xsl:text>logiciel InDesign</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30997'">
        <xsl:text>logiciel corel draw</xsl:text>
      </xsl:when>
      <xsl:when test="$key='30999'">
        <xsl:text>logiciel visio</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31001'">
        <xsl:text>WINDOWS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31002'">
        <xsl:text>WINDOWS NT</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31003'">
        <xsl:text>temps réel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31006'">
        <xsl:text>sécurité informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31011'">
        <xsl:text>MS-DOS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31017'">
        <xsl:text>numérisation données</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31018'">
        <xsl:text>micro-informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31019'">
        <xsl:text>système expert</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31020'">
        <xsl:text>unix</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31021'">
        <xsl:text>linux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31028'">
        <xsl:text>intelligence artificielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31029'">
        <xsl:text>informatique linguistique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31032'">
        <xsl:text>système exploitation informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31034'">
        <xsl:text>administration système</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31038'">
        <xsl:text>audit informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31041'">
        <xsl:text>AS/400</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31042'">
        <xsl:text>SGBD</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31043'">
        <xsl:text>système informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31049'">
        <xsl:text>qualité informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31050'">
        <xsl:text>assistance utilisateur informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31051'">
        <xsl:text>maintenance informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31052'">
        <xsl:text>data warehouse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31054'">
        <xsl:text>informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31057'">
        <xsl:text>génie logiciel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31067'">
        <xsl:text>analyse programmation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31068'">
        <xsl:text>méthode analyse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31069'">
        <xsl:text>merise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31070'">
        <xsl:text>vente informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31073'">
        <xsl:text>compression données</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31077'">
        <xsl:text>interface</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31080'">
        <xsl:text>matériel informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31081'">
        <xsl:text>gestion parc informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31084'">
        <xsl:text>informatique de gestion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31085'">
        <xsl:text>informatisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31087'">
        <xsl:text>méthode OMT</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31088'">
        <xsl:text>programmation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31089'">
        <xsl:text>MCP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31091'">
        <xsl:text>bureautique conception</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31092'">
        <xsl:text>formation formateur informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31093'">
        <xsl:text>correspondance informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31094'">
        <xsl:text>conduite projet informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31095'">
        <xsl:text>schéma directeur informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31096'">
        <xsl:text>cahier charges informatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31097'">
        <xsl:text>programmation structurée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31098'">
        <xsl:text>programmation orientée objet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31301'">
        <xsl:text>test étanchéité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31302'">
        <xsl:text>magnétoscopie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31303'">
        <xsl:text>courant Foucault</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31304'">
        <xsl:text>radiographie industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31308'">
        <xsl:text>méthode Taguchi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31310'">
        <xsl:text>thermographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31315'">
        <xsl:text>émission acoustique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31320'">
        <xsl:text>examen ultrason</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31325'">
        <xsl:text>interférométrie holographique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31327'">
        <xsl:text>contrôle statistique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31328'">
        <xsl:text>hoschin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31329'">
        <xsl:text>spc</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31331'">
        <xsl:text>ressuage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31333'">
        <xsl:text>contrôle non destructif</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31336'">
        <xsl:text>contrôle qualité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31337'">
        <xsl:text>outil qualité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31338'">
        <xsl:text>autocontrôle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31339'">
        <xsl:text>six sigma</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31348'">
        <xsl:text>certification</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31349'">
        <xsl:text>connaissance norme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31350'">
        <xsl:text>contrôle destructif</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31354'">
        <xsl:text>qualité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31356'">
        <xsl:text>assurance qualité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31358'">
        <xsl:text>kaizen</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31359'">
        <xsl:text>audit qualité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31360'">
        <xsl:text>micrographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31361'">
        <xsl:text>métallographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31367'">
        <xsl:text>smed</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31368'">
        <xsl:text>manuel qualité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31369'">
        <xsl:text>5S</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31371'">
        <xsl:text>métrologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31374'">
        <xsl:text>fiabilité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31375'">
        <xsl:text>animation qualité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31378'">
        <xsl:text>cercle qualité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31380'">
        <xsl:text>métrologie dimensionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31382'">
        <xsl:text>formation formateur qualité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31383'">
        <xsl:text>analyse contraintes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31386'">
        <xsl:text>qualité totale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31388'">
        <xsl:text>gestion qualité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31392'">
        <xsl:text>extensométrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31394'">
        <xsl:text>amdec</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31601'">
        <xsl:text>CFAO</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31603'">
        <xsl:text>maintenance préventive</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31604'">
        <xsl:text>maintenance corrective</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31605'">
        <xsl:text>pièce rechange</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31613'">
        <xsl:text>travaux neufs</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31618'">
        <xsl:text>mesure temps</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31620'">
        <xsl:text>conduite installation industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31622'">
        <xsl:text>FAO</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31624'">
        <xsl:text>maintenance industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31625'">
        <xsl:text>gestion maintenance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31629'">
        <xsl:text>bureau méthodes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31632'">
        <xsl:text>productique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31633'">
        <xsl:text>maintenance totale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31635'">
        <xsl:text>documentation maintenance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31637'">
        <xsl:text>organisation travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31641'">
        <xsl:text>contrôle gestion production</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31648'">
        <xsl:text>transfert technologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31650'">
        <xsl:text>GPAO</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31652'">
        <xsl:text>gestion production</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31654'">
        <xsl:text>gestion industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31660'">
        <xsl:text>approvisionnement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31662'">
        <xsl:text>ordonnancement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31663'">
        <xsl:text>gestion stock</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31664'">
        <xsl:text>qualité gestion industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31667'">
        <xsl:text>audit gestion industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31672'">
        <xsl:text>PERT</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31676'">
        <xsl:text>bureau études</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31684'">
        <xsl:text>CAO</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31685'">
        <xsl:text>DAO</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31686'">
        <xsl:text>dessin industriel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31688'">
        <xsl:text>juste à temps</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31695'">
        <xsl:text>lecture plan</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31696'">
        <xsl:text>cotation fonctionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31697'">
        <xsl:text>MRP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31698'">
        <xsl:text>kanban</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31703'">
        <xsl:text>stockage matière dangereuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31706'">
        <xsl:text>chouleur chargeur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31708'">
        <xsl:text>grue</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31709'">
        <xsl:text>grue mobile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31711'">
        <xsl:text>silo</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31713'">
        <xsl:text>stockage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31715'">
        <xsl:text>nacelle élévatrice</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31717'">
        <xsl:text>engin chantier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31721'">
        <xsl:text>entrepôt</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31726'">
        <xsl:text>élévateur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31728'">
        <xsl:text>pont roulant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31729'">
        <xsl:text>élingage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31731'">
        <xsl:text>messagerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31734'">
        <xsl:text>magasinage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31736'">
        <xsl:text>réparation engin manutention</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31738'">
        <xsl:text>pont élingage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31740'">
        <xsl:text>mise en bouteille</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31742'">
        <xsl:text>étiquetage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31743'">
        <xsl:text>expédition</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31747'">
        <xsl:text>engin manutention levage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31752'">
        <xsl:text>conditionnement emballage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31754'">
        <xsl:text>manutention</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31759'">
        <xsl:text>engin traction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31760'">
        <xsl:text>packaging</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31768'">
        <xsl:text>cariste</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31769'">
        <xsl:text>engin ferroviaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31770'">
        <xsl:text>manutention produit chimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31771'">
        <xsl:text>manutention matière dangereuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31776'">
        <xsl:text>geste posture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31779'">
        <xsl:text>accrochage wagon</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31782'">
        <xsl:text>ramassage déchet ménager</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31784'">
        <xsl:text>palettisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31793'">
        <xsl:text>monitorat manutention</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31795'">
        <xsl:text>sécurité manutention</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31801'">
        <xsl:text>conduite taxi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31802'">
        <xsl:text>monitorat auto-école</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31803'">
        <xsl:text>conduite voiture direction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31804'">
        <xsl:text>conduite véhicule articulé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31805'">
        <xsl:text>transport en commun</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31806'">
        <xsl:text>transport scolaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31807'">
        <xsl:text>livraison</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31808'">
        <xsl:text>transport frigorifique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31809'">
        <xsl:text>déménagement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31810'">
        <xsl:text>conduite tout terrain</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31811'">
        <xsl:text>conduite moto</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31812'">
        <xsl:text>conduite auto</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31813'">
        <xsl:text>gestion parc véhicule</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31816'">
        <xsl:text>conduite poids lourd</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31819'">
        <xsl:text>transport produit chimique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31820'">
        <xsl:text>perfectionnement conduite</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31826'">
        <xsl:text>FIMO FCOS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31827'">
        <xsl:text>transport marchandise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31828'">
        <xsl:text>transport matière dangereuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31831'">
        <xsl:text>conduite économique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31833'">
        <xsl:text>transport routier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31834'">
        <xsl:text>logistique transport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31835'">
        <xsl:text>europermis</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31836'">
        <xsl:text>logistique agroalimentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31837'">
        <xsl:text>logistique distribution</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31839'">
        <xsl:text>transit</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31840'">
        <xsl:text>transport réseau guidé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31842'">
        <xsl:text>transport filoguidé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31844'">
        <xsl:text>transitique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31845'">
        <xsl:text>logistique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31847'">
        <xsl:text>transport international</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31850'">
        <xsl:text>transport fluvial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31854'">
        <xsl:text>transport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31856'">
        <xsl:text>affrètement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31858'">
        <xsl:text>douane</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31859'">
        <xsl:text>déclaration douane</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31861'">
        <xsl:text>permis bateau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31863'">
        <xsl:text>qualité transport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31866'">
        <xsl:text>activité aéroportuaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31867'">
        <xsl:text>transport aérien</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31869'">
        <xsl:text>fret aérien</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31870'">
        <xsl:text>navigation commerce plaisance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31872'">
        <xsl:text>transport maritime</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31873'">
        <xsl:text>formation formateur transport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31875'">
        <xsl:text>sécurité transport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31876'">
        <xsl:text>personnel au sol</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31879'">
        <xsl:text>hôtesse air steward</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31880'">
        <xsl:text>électricien bord</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31883'">
        <xsl:text>mécanicien bord</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31886'">
        <xsl:text>mécanicien navigant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31887'">
        <xsl:text>pilotage hélicoptère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31888'">
        <xsl:text>pilotage avion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31889'">
        <xsl:text>pilote de ligne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31890'">
        <xsl:text>opérateur radio</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31891'">
        <xsl:text>activité portuaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31892'">
        <xsl:text>capitaine marine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31893'">
        <xsl:text>officier marine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31894'">
        <xsl:text>droit transport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31895'">
        <xsl:text>gestion entreprise transport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31898'">
        <xsl:text>instruction pilote</xsl:text>
      </xsl:when>
      <xsl:when test="$key='31899'">
        <xsl:text>qualification vol instruments</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32001'">
        <xsl:text>motivation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32002'">
        <xsl:text>conduite réunion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32005'">
        <xsl:text>stratégie internationale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32006'">
        <xsl:text>gestion entreprise internationale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32007'">
        <xsl:text>cahier charges fonctionnel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32010'">
        <xsl:text>maîtrise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32014'">
        <xsl:text>conseil administration</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32015'">
        <xsl:text>conduite changement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32016'">
        <xsl:text>conduite changement technologique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32018'">
        <xsl:text>qualité administrative</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32020'">
        <xsl:text>encadrement interculturel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32023'">
        <xsl:text>prise décision</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32024'">
        <xsl:text>gestion centre profit</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32025'">
        <xsl:text>stratégie entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32030'">
        <xsl:text>animation équipe travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32031'">
        <xsl:text>projet entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32032'">
        <xsl:text>encadrement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32035'">
        <xsl:text>conduite projet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32036'">
        <xsl:text>conduite projet international</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32040'">
        <xsl:text>délégation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32042'">
        <xsl:text>management participatif</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32043'">
        <xsl:text>conseil entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32045'">
        <xsl:text>management par projet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32047'">
        <xsl:text>création entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32050'">
        <xsl:text>coaching entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32054'">
        <xsl:text>direction entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32062'">
        <xsl:text>recherche développement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32067'">
        <xsl:text>analyse valeur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32070'">
        <xsl:text>innovation entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32072'">
        <xsl:text>sous-traitance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32074'">
        <xsl:text>gestion prévisionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32076'">
        <xsl:text>gestion entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32079'">
        <xsl:text>gestion performance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32089'">
        <xsl:text>tableau bord</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32091'">
        <xsl:text>benchmarking</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32094'">
        <xsl:text>gestion PME-PMI</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32095'">
        <xsl:text>direction par objectifs</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32096'">
        <xsl:text>gestion entreprise culturelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32097'">
        <xsl:text>gestion entreprise artisanale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32098'">
        <xsl:text>audit entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32601'">
        <xsl:text>trésorerie internationale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32603'">
        <xsl:text>investissement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32605'">
        <xsl:text>ratio financier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32607'">
        <xsl:text>tableau financement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32611'">
        <xsl:text>trésorerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32618'">
        <xsl:text>évaluation financière entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32623'">
        <xsl:text>financement entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32626'">
        <xsl:text>analyse financière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32632'">
        <xsl:text>consolidation comptable</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32646'">
        <xsl:text>comptabilité étrangère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32650'">
        <xsl:text>gestion budgétaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32652'">
        <xsl:text>contrôle gestion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32654'">
        <xsl:text>gestion financière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32663'">
        <xsl:text>comptabilité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32667'">
        <xsl:text>comptabilité générale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32669'">
        <xsl:text>bilan</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32673'">
        <xsl:text>gestion par activités</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32679'">
        <xsl:text>compte résultat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32680'">
        <xsl:text>calcul rentabilité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32682'">
        <xsl:text>comptabilité analytique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32684'">
        <xsl:text>expertise comptable</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32688'">
        <xsl:text>comptabilité charges personnel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32689'">
        <xsl:text>inventaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32691'">
        <xsl:text>prix revient</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32692'">
        <xsl:text>coût</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32694'">
        <xsl:text>révision comptable</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32695'">
        <xsl:text>audit comptabilité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32696'">
        <xsl:text>provision comptable</xsl:text>
      </xsl:when>
      <xsl:when test="$key='32697'">
        <xsl:text>plan comptable</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33004'">
        <xsl:text>retraite</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33006'">
        <xsl:text>bilan social</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33008'">
        <xsl:text>comité entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33012'">
        <xsl:text>expatriation impatriation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33016'">
        <xsl:text>tableau bord social</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33019'">
        <xsl:text>représentation syndicale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33021'">
        <xsl:text>épargne salariale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33024'">
        <xsl:text>protection sociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33028'">
        <xsl:text>représentation personnel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33030'">
        <xsl:text>licenciement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33031'">
        <xsl:text>système rémunération</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33035'">
        <xsl:text>audit ressources humaines</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33036'">
        <xsl:text>stratégie sociale entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33039'">
        <xsl:text>négociation sociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33040'">
        <xsl:text>plan social</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33043'">
        <xsl:text>élection dans entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33047'">
        <xsl:text>relation travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33049'">
        <xsl:text>conflit travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33052'">
        <xsl:text>administration personnel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33054'">
        <xsl:text>ressources humaines</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33060'">
        <xsl:text>recrutement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33070'">
        <xsl:text>travail temporaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33071'">
        <xsl:text>contrat travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33072'">
        <xsl:text>intégration salarié</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33074'">
        <xsl:text>gestion connaissances</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33082'">
        <xsl:text>360°</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33083'">
        <xsl:text>entretien évaluation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33086'">
        <xsl:text>aménagement temps travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33091'">
        <xsl:text>gestion prévisionnelle emplois compétences</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33092'">
        <xsl:text>évaluation personnel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33096'">
        <xsl:text>travail à distance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='33097'">
        <xsl:text>réduction temps travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34002'">
        <xsl:text>négociation achat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34003'">
        <xsl:text>centrale achat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34005'">
        <xsl:text>qualité achat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34016'">
        <xsl:text>marketing achat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34018'">
        <xsl:text>devis</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34022'">
        <xsl:text>gestion fournisseur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34024'">
        <xsl:text>achat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34026'">
        <xsl:text>développement sens commercial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34027'">
        <xsl:text>service après-vente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34030'">
        <xsl:text>chef produit</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34031'">
        <xsl:text>marketing mix</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34037'">
        <xsl:text>satisfaction client</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34038'">
        <xsl:text>prix vente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34040'">
        <xsl:text>étude produit</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34042'">
        <xsl:text>assistance marketing</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34052'">
        <xsl:text>marketing</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34054'">
        <xsl:text>gestion commerciale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34056'">
        <xsl:text>administration ventes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34058'">
        <xsl:text>relance impayés</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34060'">
        <xsl:text>étude marché</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34071'">
        <xsl:text>marketing sectoriel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34072'">
        <xsl:text>marketing direct</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34074'">
        <xsl:text>publicité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34076'">
        <xsl:text>gestion relation client</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34078'">
        <xsl:text>facturation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34085'">
        <xsl:text>stratégie commerciale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34088'">
        <xsl:text>gestion risque client</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34091'">
        <xsl:text>marketing industriel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34092'">
        <xsl:text>publipostage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34093'">
        <xsl:text>marketing électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34205'">
        <xsl:text>crédit documentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34211'">
        <xsl:text>incoterm</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34216'">
        <xsl:text>paiement international</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34218'">
        <xsl:text>crédit export</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34225'">
        <xsl:text>risque change</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34232'">
        <xsl:text>réglementation commerce international</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34236'">
        <xsl:text>financement international</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34239'">
        <xsl:text>assurance export</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34240'">
        <xsl:text>implantation étranger</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34254'">
        <xsl:text>commerce international</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34256'">
        <xsl:text>exportation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34258'">
        <xsl:text>TACE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34260'">
        <xsl:text>marketing international</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34271'">
        <xsl:text>négociation internationale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34273'">
        <xsl:text>importation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34279'">
        <xsl:text>assistance export</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34285'">
        <xsl:text>import export</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34287'">
        <xsl:text>logistique internationale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34292'">
        <xsl:text>achat international</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34501'">
        <xsl:text>vente par téléphone</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34502'">
        <xsl:text>vente spécialisée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34504'">
        <xsl:text>vente fruit légume</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34505'">
        <xsl:text>vente produit carné</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34506'">
        <xsl:text>vente produit mer</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34513'">
        <xsl:text>vente vin spiritueux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34516'">
        <xsl:text>vente produit laitier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34517'">
        <xsl:text>vente automobile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34519'">
        <xsl:text>étalage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34520'">
        <xsl:text>vente par correspondance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34525'">
        <xsl:text>vente produit alimentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34529'">
        <xsl:text>emballage commercial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34530'">
        <xsl:text>commerce électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34534'">
        <xsl:text>vente produit horticole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34536'">
        <xsl:text>vente équipement maison</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34539'">
        <xsl:text>franchisage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34549'">
        <xsl:text>libre-service</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34554'">
        <xsl:text>commercialisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34559'">
        <xsl:text>gestion rayon</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34560'">
        <xsl:text>VRP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34561'">
        <xsl:text>force vente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34566'">
        <xsl:text>vente distribution</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34569'">
        <xsl:text>démarque inconnue</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34570'">
        <xsl:text>encadrement force vente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34572'">
        <xsl:text>négociation grand compte</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34573'">
        <xsl:text>action commerciale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34574'">
        <xsl:text>formation formateur commercial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34575'">
        <xsl:text>commerce gros</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34580'">
        <xsl:text>visiteur médical</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34581'">
        <xsl:text>technicocommercial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34582'">
        <xsl:text>négociation commerciale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34584'">
        <xsl:text>animation vente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34587'">
        <xsl:text>grande distribution</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34588'">
        <xsl:text>gestion point vente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34589'">
        <xsl:text>caisse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34590'">
        <xsl:text>ingénieur affaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34592'">
        <xsl:text>entretien vente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34593'">
        <xsl:text>prospection vente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='34597'">
        <xsl:text>marchandisage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35003'">
        <xsl:text>audiotypie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35004'">
        <xsl:text>secrétariat formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35006'">
        <xsl:text>secrétariat juridique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35007'">
        <xsl:text>secrétariat PME-PMI</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35010'">
        <xsl:text>sténographie étrangère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35012'">
        <xsl:text>dactylographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35014'">
        <xsl:text>secrétariat ressources humaines</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35015'">
        <xsl:text>secrétariat médicosocial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35018'">
        <xsl:text>secrétariat multilingue</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35019'">
        <xsl:text>secrétariat commerce international</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35021'">
        <xsl:text>sténographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35028'">
        <xsl:text>secrétariat commercial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35029'">
        <xsl:text>correspondance commerciale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35030'">
        <xsl:text>sténotypie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35032'">
        <xsl:text>sténodactylographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35035'">
        <xsl:text>secrétariat spécialisé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35039'">
        <xsl:text>secrétariat technique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35041'">
        <xsl:text>écriture abrégée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35047'">
        <xsl:text>assistance direction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35049'">
        <xsl:text>secrétariat comptable</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35050'">
        <xsl:text>entretien téléphonique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35052'">
        <xsl:text>accueil</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35054'">
        <xsl:text>secrétariat assistance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35056'">
        <xsl:text>télésecrétariat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35066'">
        <xsl:text>bureautique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35071'">
        <xsl:text>technique administrative</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35082'">
        <xsl:text>standard</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35084'">
        <xsl:text>qualité secrétariat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35091'">
        <xsl:text>classement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='35093'">
        <xsl:text>correspondance professionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41002'">
        <xsl:text>produit financier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41003'">
        <xsl:text>gestion portefeuille</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41006'">
        <xsl:text>assurance construction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41008'">
        <xsl:text>assurance accident</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41011'">
        <xsl:text>bourse initiation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41014'">
        <xsl:text>gestion patrimoine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41015'">
        <xsl:text>inspection assurance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41016'">
        <xsl:text>réassurance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41019'">
        <xsl:text>assurance incendie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41020'">
        <xsl:text>formation formateur banque assurance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41023'">
        <xsl:text>marché financier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41028'">
        <xsl:text>assurance dommages</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41031'">
        <xsl:text>monétique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41035'">
        <xsl:text>assurance responsabilité civile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41036'">
        <xsl:text>assurance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41039'">
        <xsl:text>droit assurance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41040'">
        <xsl:text>marketing bancaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41045'">
        <xsl:text>assurance automobile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41049'">
        <xsl:text>assurance entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41050'">
        <xsl:text>sécurité bancaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41054'">
        <xsl:text>banque assurance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41058'">
        <xsl:text>assurance biens particulier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41062'">
        <xsl:text>banque</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41064'">
        <xsl:text>assurance collectivité locale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41066'">
        <xsl:text>assurance transport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41068'">
        <xsl:text>assurance personnes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41069'">
        <xsl:text>assurance vie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41070'">
        <xsl:text>gestion exploitation banque</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41075'">
        <xsl:text>service bancaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41077'">
        <xsl:text>gestion risque banque assurance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41079'">
        <xsl:text>actuariat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41080'">
        <xsl:text>relation banque</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41083'">
        <xsl:text>clientèle particulier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41084'">
        <xsl:text>crédit bancaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41087'">
        <xsl:text>opération sur fonds propres</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41090'">
        <xsl:text>clientèle collectivité territoriale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41091'">
        <xsl:text>droit bancaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41092'">
        <xsl:text>clientèle entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41094'">
        <xsl:text>crédit immobilier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41095'">
        <xsl:text>front office</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41096'">
        <xsl:text>back office</xsl:text>
      </xsl:when>
      <xsl:when test="$key='41097'">
        <xsl:text>opération bancaire étranger</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42001'">
        <xsl:text>drainage lymphatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42003'">
        <xsl:text>toilettage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42004'">
        <xsl:text>vente en animalerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42020'">
        <xsl:text>massage esthétique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42025'">
        <xsl:text>animal compagnie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42027'">
        <xsl:text>service location</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42030'">
        <xsl:text>manucure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42032'">
        <xsl:text>esthétique soin corporel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42040'">
        <xsl:text>parfumerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42050'">
        <xsl:text>coiffure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42052'">
        <xsl:text>maquillage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42054'">
        <xsl:text>services divers</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42057'">
        <xsl:text>service funéraire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42060'">
        <xsl:text>assistance coiffure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42061'">
        <xsl:text>spécialisation coiffure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42062'">
        <xsl:text>maquillage spectacle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42069'">
        <xsl:text>employé collectivité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42077'">
        <xsl:text>casino</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42079'">
        <xsl:text>service maison</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42083'">
        <xsl:text>entretien sol</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42084'">
        <xsl:text>entretien vitre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42087'">
        <xsl:text>lingerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42091'">
        <xsl:text>gestion entreprise nettoyage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42093'">
        <xsl:text>nettoyage locaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42095'">
        <xsl:text>teinturerie pressing</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42096'">
        <xsl:text>entretien cuir peau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42097'">
        <xsl:text>blanchisserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42116'">
        <xsl:text>mesurage locaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42121'">
        <xsl:text>gestion locative</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42133'">
        <xsl:text>gestion immobilière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42137'">
        <xsl:text>expertise immobilière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42151'">
        <xsl:text>copropriété</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42154'">
        <xsl:text>immobilier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42158'">
        <xsl:text>commercialisation immobilière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42171'">
        <xsl:text>logement social</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42178'">
        <xsl:text>financement immobilier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42184'">
        <xsl:text>agence immobilière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42186'">
        <xsl:text>promotion immobilière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42602'">
        <xsl:text>guide interprète</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42603'">
        <xsl:text>conducteur tourisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42608'">
        <xsl:text>parc loisir</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42611'">
        <xsl:text>guide accompagnateur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42619'">
        <xsl:text>camping caravaning</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42623'">
        <xsl:text>animation tourisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42625'">
        <xsl:text>tourisme affaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42627'">
        <xsl:text>gestion entreprise touristique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42632'">
        <xsl:text>tourisme sportif</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42638'">
        <xsl:text>développement touristique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42641'">
        <xsl:text>tourisme social</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42648'">
        <xsl:text>conception produit touristique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42651'">
        <xsl:text>tourisme culturel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42654'">
        <xsl:text>tourisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42659'">
        <xsl:text>marketing touristique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42668'">
        <xsl:text>commercialisation produit touristique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42671'">
        <xsl:text>tourisme thermal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42677'">
        <xsl:text>agence voyage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42679'">
        <xsl:text>billetterie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42681'">
        <xsl:text>tourisme pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42686'">
        <xsl:text>accueil tourisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42691'">
        <xsl:text>tourisme équestre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42692'">
        <xsl:text>tourisme montagne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42693'">
        <xsl:text>tourisme rural</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42694'">
        <xsl:text>tourisme fluvial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42695'">
        <xsl:text>information tourisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42701'">
        <xsl:text>cuisine sauce</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42704'">
        <xsl:text>pizzeria</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42705'">
        <xsl:text>crêperie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42708'">
        <xsl:text>cuisine collectivité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42710'">
        <xsl:text>cuisine dessert</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42712'">
        <xsl:text>cuisine poisson</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42716'">
        <xsl:text>café brasserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42719'">
        <xsl:text>économat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42720'">
        <xsl:text>cuisine exotique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42723'">
        <xsl:text>bar</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42725'">
        <xsl:text>restauration rapide</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42727'">
        <xsl:text>gestion menu</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42729'">
        <xsl:text>gestion restauration collective</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42730'">
        <xsl:text>cuisine régionale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42734'">
        <xsl:text>néorestauration</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42739'">
        <xsl:text>sommellerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42740'">
        <xsl:text>cuisine viande</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42742'">
        <xsl:text>cuisine spécialisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42745'">
        <xsl:text>décoration hôtel restaurant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42746'">
        <xsl:text>restauration</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42749'">
        <xsl:text>buffet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42752'">
        <xsl:text>cuisine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42754'">
        <xsl:text>hôtellerie restauration</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42756'">
        <xsl:text>qualité hôtellerie restauration</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42757'">
        <xsl:text>service salle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42759'">
        <xsl:text>écailler</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42760'">
        <xsl:text>gastronomie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42766'">
        <xsl:text>gestion hôtel restaurant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42769'">
        <xsl:text>maître d'hôtel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42773'">
        <xsl:text>cuisine diététique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42776'">
        <xsl:text>hôtellerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42778'">
        <xsl:text>service hall</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42780'">
        <xsl:text>entretien cuisine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42782'">
        <xsl:text>gestion cuisine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42785'">
        <xsl:text>formation formateur hôtellerie restauration</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42786'">
        <xsl:text>service étage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42788'">
        <xsl:text>service réception</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42791'">
        <xsl:text>néocuisine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42793'">
        <xsl:text>hygiène sécurité hôtel restaurant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42797'">
        <xsl:text>gouvernante hôtel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42801'">
        <xsl:text>télésurveillance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42805'">
        <xsl:text>sécurité secourisme mer</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42806'">
        <xsl:text>sécurité secourisme montagne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42808'">
        <xsl:text>secourisme routier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42810'">
        <xsl:text>gardiennage immeuble</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42820'">
        <xsl:text>surveillance magasin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42822'">
        <xsl:text>surveillance gardiennage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42824'">
        <xsl:text>maître chien</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42826'">
        <xsl:text>secourisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42829'">
        <xsl:text>sauvetage secourisme travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42831'">
        <xsl:text>sécurité aérienne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42835'">
        <xsl:text>salle blanche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42837'">
        <xsl:text>police gendarmerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42838'">
        <xsl:text>détective</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42841'">
        <xsl:text>sécurité IGH</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42848'">
        <xsl:text>sécurité aire jeux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42850'">
        <xsl:text>sécurité ERP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42854'">
        <xsl:text>prévention sécurité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42856'">
        <xsl:text>formation formateur sécurité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42861'">
        <xsl:text>sécurité domestique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42863'">
        <xsl:text>audit sécurité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42865'">
        <xsl:text>sécurité travaux en hauteur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42866'">
        <xsl:text>prévention sécurité travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42868'">
        <xsl:text>CHSCT</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42870'">
        <xsl:text>évacuation locaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42872'">
        <xsl:text>sécurité incendie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42875'">
        <xsl:text>risque professionnel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42879'">
        <xsl:text>hygiène travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42880'">
        <xsl:text>pompier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42883'">
        <xsl:text>prévention risque naturel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42884'">
        <xsl:text>risque industriel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42887'">
        <xsl:text>condition travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42889'">
        <xsl:text>environnement physique travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42891'">
        <xsl:text>équipement sécurité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42895'">
        <xsl:text>sécurité machine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42896'">
        <xsl:text>sécurité sous-traitance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='42898'">
        <xsl:text>ergonomie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43001'">
        <xsl:text>recherche médicale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43002'">
        <xsl:text>phytothérapie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43003'">
        <xsl:text>acupuncture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43004'">
        <xsl:text>chiropractie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43005'">
        <xsl:text>homéopathie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43006'">
        <xsl:text>préparation pharmacie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43008'">
        <xsl:text>médecine travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43009'">
        <xsl:text>épidémiologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43010'">
        <xsl:text>neurochirurgie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43013'">
        <xsl:text>naturothérapie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43015'">
        <xsl:text>ostéopathie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43019'">
        <xsl:text>médecine sportive</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43020'">
        <xsl:text>traumatologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43021'">
        <xsl:text>chirurgie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43022'">
        <xsl:text>droit médical</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43023'">
        <xsl:text>traitement douleur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43024'">
        <xsl:text>médecine douce</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43025'">
        <xsl:text>sophrologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43026'">
        <xsl:text>pharmacie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43029'">
        <xsl:text>médecine thermale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43030'">
        <xsl:text>microchirurgie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43039'">
        <xsl:text>cancérologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43040'">
        <xsl:text>stomatologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43041'">
        <xsl:text>angiologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43042'">
        <xsl:text>médecine générale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43046'">
        <xsl:text>urgence médicale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43048'">
        <xsl:text>psychiatrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43050'">
        <xsl:text>chirurgie dentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43051'">
        <xsl:text>odontologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43054'">
        <xsl:text>médecine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43057'">
        <xsl:text>neurologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43058'">
        <xsl:text>allergologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43059'">
        <xsl:text>gériatrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43061'">
        <xsl:text>imagerie médicale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43064'">
        <xsl:text>spécialité médicale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43069'">
        <xsl:text>pédiatrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43070'">
        <xsl:text>nutrition</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43071'">
        <xsl:text>ophtalmologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43079'">
        <xsl:text>sexologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43080'">
        <xsl:text>ORL</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43083'">
        <xsl:text>gynécologie obstétrique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43085'">
        <xsl:text>cardiologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43088'">
        <xsl:text>toxicologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43089'">
        <xsl:text>dermatologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43090'">
        <xsl:text>endocrinologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43091'">
        <xsl:text>médecine exotique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43092'">
        <xsl:text>sage-femme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43093'">
        <xsl:text>hépatologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43094'">
        <xsl:text>anesthésie réanimation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43095'">
        <xsl:text>rhumatologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43096'">
        <xsl:text>pneumologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43097'">
        <xsl:text>gastro-entérologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43098'">
        <xsl:text>urologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43099'">
        <xsl:text>hématologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43401'">
        <xsl:text>dossier soin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43402'">
        <xsl:text>manutention malade</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43403'">
        <xsl:text>hygiène hospitalière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43404'">
        <xsl:text>accompagnement fin vie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43406'">
        <xsl:text>hospitalisation à domicile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43409'">
        <xsl:text>préparation concours paramédical</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43411'">
        <xsl:text>hôpital jour</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43412'">
        <xsl:text>soin malade</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43413'">
        <xsl:text>brancardier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43414'">
        <xsl:text>assistance psychologique malade</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43417'">
        <xsl:text>gestion technique hôpital</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43418'">
        <xsl:text>thalasso-thermalisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43419'">
        <xsl:text>ambulancier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43421'">
        <xsl:text>accueil hôpital</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43426'">
        <xsl:text>gestion hospitalière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43429'">
        <xsl:text>agent service hospitalier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43431'">
        <xsl:text>santé mentale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43432'">
        <xsl:text>qualité santé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43434'">
        <xsl:text>hôpital</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43435'">
        <xsl:text>encadrement santé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43436'">
        <xsl:text>aide soignant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43437'">
        <xsl:text>personnel paramédical</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43439'">
        <xsl:text>puériculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43440'">
        <xsl:text>diététique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43448'">
        <xsl:text>infirmier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43449'">
        <xsl:text>infirmier salle opération</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43454'">
        <xsl:text>santé secteur sanitaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43457'">
        <xsl:text>infirmier réanimation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43458'">
        <xsl:text>encadrement infirmier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43459'">
        <xsl:text>infirmier psychiatrique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43460'">
        <xsl:text>orthopédie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43469'">
        <xsl:text>transfusion sanguine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43470'">
        <xsl:text>psychomotricité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43472'">
        <xsl:text>rééducation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43476'">
        <xsl:text>analyse médicale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43479'">
        <xsl:text>instrumentation médicale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43480'">
        <xsl:text>orthophonie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43484'">
        <xsl:text>prothèse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43485'">
        <xsl:text>assistance dentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43486'">
        <xsl:text>optique lunetterie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43490'">
        <xsl:text>kinésithérapie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43491'">
        <xsl:text>ergothérapie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43493'">
        <xsl:text>podologie pédicurie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43494'">
        <xsl:text>prothèse dentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43495'">
        <xsl:text>audioprothèse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='43497'">
        <xsl:text>manipulation électroradiologique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44003'">
        <xsl:text>qualité secteur social</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44004'">
        <xsl:text>aide médico-psychologique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44005'">
        <xsl:text>veilleur nuit</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44006'">
        <xsl:text>relation parent enfant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44007'">
        <xsl:text>conseil familial conjugal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44011'">
        <xsl:text>maltraitance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44012'">
        <xsl:text>addiction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44019'">
        <xsl:text>auxiliaire ménagère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44020'">
        <xsl:text>écoute sociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44021'">
        <xsl:text>relation aide</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44022'">
        <xsl:text>aide médicosociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44024'">
        <xsl:text>personne handicapée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44026'">
        <xsl:text>famille</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44028'">
        <xsl:text>auxiliaire vie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44030'">
        <xsl:text>assistance maternelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44031'">
        <xsl:text>atelier conte</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44032'">
        <xsl:text>aide sociale enfance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44035'">
        <xsl:text>médiation socioculturelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44037'">
        <xsl:text>direction établissement socioculturel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44038'">
        <xsl:text>projet établissement médicosocial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44039'">
        <xsl:text>gestion maison retraite</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44041'">
        <xsl:text>petite enfance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44042'">
        <xsl:text>enfance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44047'">
        <xsl:text>direction établissement médicosocial</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44049'">
        <xsl:text>gestion crèche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44050'">
        <xsl:text>éducateur jeune enfant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44051'">
        <xsl:text>auxiliaire puériculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44052'">
        <xsl:text>adolescence</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44054'">
        <xsl:text>action sociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44056'">
        <xsl:text>travail social en réseau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44057'">
        <xsl:text>atelier éducatif</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44058'">
        <xsl:text>vie associative</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44061'">
        <xsl:text>éducation surveillée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44067'">
        <xsl:text>animation socioculturelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44069'">
        <xsl:text>éducation populaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44071'">
        <xsl:text>formation formateur travail social</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44072'">
        <xsl:text>travail social</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44074'">
        <xsl:text>prévention éducation santé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44077'">
        <xsl:text>migrant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44079'">
        <xsl:text>animation personne handicapée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44080'">
        <xsl:text>délégation tutelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44083'">
        <xsl:text>assistant social</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44084'">
        <xsl:text>économie sociale familiale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44086'">
        <xsl:text>marketing social</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44089'">
        <xsl:text>animation troisième âge</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44090'">
        <xsl:text>analyse pratique sociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44091'">
        <xsl:text>prévention sociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44092'">
        <xsl:text>éducateur spécialisé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44094'">
        <xsl:text>conseiller travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44095'">
        <xsl:text>économie familiale rurale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44096'">
        <xsl:text>maîtresse maison</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44501'">
        <xsl:text>lutte contre illettrisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44502'">
        <xsl:text>pédagogie public en difficulté</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44508'">
        <xsl:text>plan formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44510'">
        <xsl:text>pédagogie interculturelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44513'">
        <xsl:text>pédagogie alternance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44514'">
        <xsl:text>enseignement individualisé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44515'">
        <xsl:text>didactique langue</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44517'">
        <xsl:text>conception action formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44521'">
        <xsl:text>pédagogie public spécifique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44528'">
        <xsl:text>responsable formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44529'">
        <xsl:text>besoin formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44534'">
        <xsl:text>didactique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44536'">
        <xsl:text>normalisation formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44538'">
        <xsl:text>achat formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44539'">
        <xsl:text>évaluation formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44540'">
        <xsl:text>relation pédagogique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44542'">
        <xsl:text>pédagogie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44548'">
        <xsl:text>gestion alternance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44549'">
        <xsl:text>gestion entreprise FPC</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44554'">
        <xsl:text>ingénierie formation pédagogie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44557'">
        <xsl:text>ingénierie formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44559'">
        <xsl:text>gestion formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44560'">
        <xsl:text>PEI</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44562'">
        <xsl:text>méthode pédagogique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44569'">
        <xsl:text>conseil insertion professionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44570'">
        <xsl:text>ARL</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44571'">
        <xsl:text>éducabilité cognitive</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44573'">
        <xsl:text>outil pédagogique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44577'">
        <xsl:text>formation formateur occasionnel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44579'">
        <xsl:text>audit formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44580'">
        <xsl:text>tanagra</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44582'">
        <xsl:text>jeu pédagogique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44584'">
        <xsl:text>multimédia pédagogique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44586'">
        <xsl:text>formation formateur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44587'">
        <xsl:text>formation formateur spécialisé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44588'">
        <xsl:text>tutorat entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44589'">
        <xsl:text>conseil formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44593'">
        <xsl:text>conception multimédia pédagogique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='44595'">
        <xsl:text>technique orientation professionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45001'">
        <xsl:text>masque</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45002'">
        <xsl:text>mime</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45003'">
        <xsl:text>expression théâtrale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45005'">
        <xsl:text>menuiserie agencement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45006'">
        <xsl:text>esthétique industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45007'">
        <xsl:text>art floral</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45008'">
        <xsl:text>maquettisme modèle réduit</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45009'">
        <xsl:text>architecture paysagère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45010'">
        <xsl:text>marionnette</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45014'">
        <xsl:text>conception cuisine salle de bains</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45015'">
        <xsl:text>architecture intérieure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45018'">
        <xsl:text>paysagisme intérieur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45019'">
        <xsl:text>ameublement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45020'">
        <xsl:text>cirque</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45022'">
        <xsl:text>art dramatique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45027'">
        <xsl:text>art décoratif</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45029'">
        <xsl:text>encadrement art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45034'">
        <xsl:text>spectacle pyrotechnique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45038'">
        <xsl:text>restauration objet art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45040'">
        <xsl:text>décor spectacle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45042'">
        <xsl:text>technique spectacle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45048'">
        <xsl:text>bande dessinée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45050'">
        <xsl:text>mise en scène</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45052'">
        <xsl:text>gestion spectacle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45054'">
        <xsl:text>art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45057'">
        <xsl:text>dessin art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45059'">
        <xsl:text>aérographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45060'">
        <xsl:text>régie spectacle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45066'">
        <xsl:text>art plastique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45069'">
        <xsl:text>mosaïque art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45071'">
        <xsl:text>danse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45072'">
        <xsl:text>direction orchestre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45073'">
        <xsl:text>musique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45075'">
        <xsl:text>commerce art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45076'">
        <xsl:text>sculpture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45079'">
        <xsl:text>gravure art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45080'">
        <xsl:text>chorégraphie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45082'">
        <xsl:text>pratique instrumentale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45083'">
        <xsl:text>chant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45085'">
        <xsl:text>formation formateur art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45089'">
        <xsl:text>lithographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45090'">
        <xsl:text>danse salon</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45091'">
        <xsl:text>danse traditionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45093'">
        <xsl:text>chant choral</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45094'">
        <xsl:text>musique synthèse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45096'">
        <xsl:text>sculpture bois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45097'">
        <xsl:text>sculpture pierre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45098'">
        <xsl:text>peinture art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45099'">
        <xsl:text>icône</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45503'">
        <xsl:text>garniture siège</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45504'">
        <xsl:text>tissage artisanal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45507'">
        <xsl:text>orfèvrerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45508'">
        <xsl:text>art pierre précieuse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45509'">
        <xsl:text>joaillerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45511'">
        <xsl:text>tapisserie lice</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45512'">
        <xsl:text>couture ameublement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45515'">
        <xsl:text>peinture bois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45516'">
        <xsl:text>peinture tissu</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45518'">
        <xsl:text>vannerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45519'">
        <xsl:text>gemmologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45520'">
        <xsl:text>macramé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45523'">
        <xsl:text>tapisserie ameublement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45529'">
        <xsl:text>BJO</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45531'">
        <xsl:text>dentellerie broderie artisanale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45539'">
        <xsl:text>dinanderie ferblanterie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45540'">
        <xsl:text>dorure art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45548'">
        <xsl:text>art bronze</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45549'">
        <xsl:text>fonderie art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45551'">
        <xsl:text>peinture lettres</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45554'">
        <xsl:text>artisanat art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45558'">
        <xsl:text>ferronnerie art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45560'">
        <xsl:text>tournage bois</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45567'">
        <xsl:text>verrerie art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45571'">
        <xsl:text>cartonnage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45577'">
        <xsl:text>tabletterie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45579'">
        <xsl:text>vitrail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45582'">
        <xsl:text>ébénisterie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45583'">
        <xsl:text>facture instrumentale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45585'">
        <xsl:text>fabrication jouet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45586'">
        <xsl:text>décoration céramique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45587'">
        <xsl:text>modelage céramique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45591'">
        <xsl:text>marqueterie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45592'">
        <xsl:text>archet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45594'">
        <xsl:text>reliure art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45595'">
        <xsl:text>lutherie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45597'">
        <xsl:text>céramique poterie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45598'">
        <xsl:text>émaillage art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='45599'">
        <xsl:text>brosserie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46001'">
        <xsl:text>quadrichromie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46002'">
        <xsl:text>chromie électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46004'">
        <xsl:text>photomontage électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46005'">
        <xsl:text>montage offset</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46007'">
        <xsl:text>photo reproduction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46008'">
        <xsl:text>reprographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46011'">
        <xsl:text>sélection couleur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46013'">
        <xsl:text>scanner imprimerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46016'">
        <xsl:text>conduite offset</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46019'">
        <xsl:text>typographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46025'">
        <xsl:text>montage imprimerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46027'">
        <xsl:text>impression</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46029'">
        <xsl:text>sérigraphie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46030'">
        <xsl:text>photocomposition</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46033'">
        <xsl:text>photogravure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46039'">
        <xsl:text>héliogravure</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46041'">
        <xsl:text>composition</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46047'">
        <xsl:text>préparation copie correction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46050'">
        <xsl:text>maquette électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46052'">
        <xsl:text>PAO</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46054'">
        <xsl:text>industrie graphique imprimerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46061'">
        <xsl:text>préao</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46069'">
        <xsl:text>dorure imprimerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46072'">
        <xsl:text>art graphique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46077'">
        <xsl:text>façonnage imprimerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46079'">
        <xsl:text>pliage imprimerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46081'">
        <xsl:text>conception graphique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46084'">
        <xsl:text>qualité imprimerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46085'">
        <xsl:text>fabrication imprimerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46089'">
        <xsl:text>massicotage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46090'">
        <xsl:text>graphisme publicité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46091'">
        <xsl:text>maquette mise en page</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46092'">
        <xsl:text>calligraphie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46093'">
        <xsl:text>chaîne graphique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46095'">
        <xsl:text>devis imprimerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46098'">
        <xsl:text>reliure brochure industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46099'">
        <xsl:text>vernissage pelliculage imprimerie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46210'">
        <xsl:text>service presse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46211'">
        <xsl:text>vente matériel audiovisuel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46212'">
        <xsl:text>projection audiovisuelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46220'">
        <xsl:text>mécénat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46221'">
        <xsl:text>communication crise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46225'">
        <xsl:text>mixage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46226'">
        <xsl:text>sonorisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46227'">
        <xsl:text>audionumérique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46232'">
        <xsl:text>communication interne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46233'">
        <xsl:text>matériel audiovisuel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46236'">
        <xsl:text>son</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46240'">
        <xsl:text>conception manifestation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46241'">
        <xsl:text>communication externe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46242'">
        <xsl:text>communication entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46244'">
        <xsl:text>gestion production audiovisuelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46245'">
        <xsl:text>radio</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46247'">
        <xsl:text>éclairagisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46251'">
        <xsl:text>jeu vidéo</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46254'">
        <xsl:text>communication audiovisuel multimédia</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46255'">
        <xsl:text>relations publiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46257'">
        <xsl:text>effets spéciaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46258'">
        <xsl:text>infographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46259'">
        <xsl:text>diaporama</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46260'">
        <xsl:text>cédérom dévédérom</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46262'">
        <xsl:text>multimédia</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46264'">
        <xsl:text>conception écriture réalisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46267'">
        <xsl:text>montage postproduction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46268'">
        <xsl:text>laboratoire ciné-photo</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46269'">
        <xsl:text>montage audiovisuel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46270'">
        <xsl:text>site internet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46271'">
        <xsl:text>navigation internet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46273'">
        <xsl:text>scénario</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46274'">
        <xsl:text>cinéma</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46275'">
        <xsl:text>film documentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46276'">
        <xsl:text>film animation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46277'">
        <xsl:text>technique vidéo</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46278'">
        <xsl:text>photographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46279'">
        <xsl:text>holographie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46280'">
        <xsl:text>analyse image</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46288'">
        <xsl:text>retouche photo</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46289'">
        <xsl:text>photographie amateur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46305'">
        <xsl:text>écriture journalistique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46306'">
        <xsl:text>journalisme audiovisuel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46307'">
        <xsl:text>secrétariat rédaction</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46309'">
        <xsl:text>diffusion presse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46325'">
        <xsl:text>journalisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46326'">
        <xsl:text>journalisme presse écrite</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46327'">
        <xsl:text>édition</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46329'">
        <xsl:text>librairie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46338'">
        <xsl:text>muséologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46342'">
        <xsl:text>intelligence économique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46345'">
        <xsl:text>technique documentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46346'">
        <xsl:text>recherche documentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46351'">
        <xsl:text>qualité information</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46354'">
        <xsl:text>information</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46362'">
        <xsl:text>marketing documentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46363'">
        <xsl:text>réseau documentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46371'">
        <xsl:text>informatique documentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46373'">
        <xsl:text>documentation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46374'">
        <xsl:text>système information</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46375'">
        <xsl:text>information scientifique et technique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46382'">
        <xsl:text>documentation audiovisuelle et multimédia</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46385'">
        <xsl:text>archivage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46391'">
        <xsl:text>médiathèque</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46392'">
        <xsl:text>photothèque</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46393'">
        <xsl:text>discothèque</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46394'">
        <xsl:text>bibliothèque</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46395'">
        <xsl:text>ludothèque</xsl:text>
      </xsl:when>
      <xsl:when test="$key='46396'">
        <xsl:text>archivage électronique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51001'">
        <xsl:text>DIPLOME MINISTERE CHARGE DE L'EDUCATION NATIONALE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51101'">
        <xsl:text>ENSEIGNEMENT GENERAL PRIMAIRE SECONDAIRE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51103'">
        <xsl:text>CFG certificat de formation générale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51104'">
        <xsl:text>BREVET diplôme national du brevet</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51105'">
        <xsl:text>BAC baccalauréat enseignement général</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51106'">
        <xsl:text>CFES certificat de fin d'études secondaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51108'">
        <xsl:text>capacité en droit</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51109'">
        <xsl:text>DAEU diplôme d'accès aux études universitaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51201'">
        <xsl:text>ENSEIGNEMENT TECHNOLOGIQUE PRIMAIRE SECONDAIRE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51202'">
        <xsl:text>CAP certificat d'aptitude professionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51203'">
        <xsl:text>BEP brevet d'études professionnelles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51204'">
        <xsl:text>BT brevet de technicien</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51205'">
        <xsl:text>BTn baccalauréat technologique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51206'">
        <xsl:text>BAC PRO baccalauréat professionnel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51207'">
        <xsl:text>CFEPS certificat de fin d'études professionnelles secondaires</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51208'">
        <xsl:text>BP brevet professionnel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51209'">
        <xsl:text>MC mention complémentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51210'">
        <xsl:text>BMA brevet des métiers d'art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51401'">
        <xsl:text>ENSEIGNEMENT SUPERIEUR GENERAL ET TECHNOLOGIQUE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51410'">
        <xsl:text>ENSEIGNEMENT A BAC + 2</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51411'">
        <xsl:text>BTS brevet de technicien supérieur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51412'">
        <xsl:text>DUT diplôme universitaire de technologie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51413'">
        <xsl:text>DNTS diplôme national de technologie spécialisée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51414'">
        <xsl:text>diplôme d'Etat d'expert en automobile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51415'">
        <xsl:text>DTA diplôme de technologie approfondie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51416'">
        <xsl:text>DUTA diplôme universitaire de technologie approfondie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51417'">
        <xsl:text>DEUG diplôme d'études universitaires générales</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51418'">
        <xsl:text>DEUST diplôme d'études universitaires scientifiques et techniques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51419'">
        <xsl:text>DEUP diplôme d'études universitaires professionnalisées</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51420'">
        <xsl:text>DMA diplôme des métiers d'art</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51440'">
        <xsl:text>ENSEIGNEMENT A BAC + 3</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51441'">
        <xsl:text>licence</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51442'">
        <xsl:text>licence professionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51460'">
        <xsl:text>ENSEIGNEMENT A BAC + 4</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51461'">
        <xsl:text>maîtrise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51462'">
        <xsl:text>MST maîtrise sciences et techniques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51463'">
        <xsl:text>MSG maîtrise sciences de gestion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51464'">
        <xsl:text>MIAGE maîtrise méthodes informatiques appliquées à la gestion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51465'">
        <xsl:text>diplôme d'ingénieur-maître IUP</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51480'">
        <xsl:text>ENSEIGNEMENT A BAC + 5 ET PLUS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51481'">
        <xsl:text>master</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51482'">
        <xsl:text>DESS diplôme d'études supérieures spécialisées</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51483'">
        <xsl:text>DESS-CAAE certificat d'aptitude à l'administration des entreprises</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51484'">
        <xsl:text>DEA diplôme d'études approfondies</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51485'">
        <xsl:text>doctorat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51486'">
        <xsl:text>DRT diplôme de recherche technologique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51487'">
        <xsl:text>magistère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51488'">
        <xsl:text>diplôme d'ingénieur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51501'">
        <xsl:text>DIPLOME D'ETUDES COMPTABLES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51510'">
        <xsl:text>DPECF diplôme préparatoire aux études comptables et financières</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51512'">
        <xsl:text>DECF diplôme d'études comptables financières</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51513'">
        <xsl:text>DESCF diplôme d'études supérieures comptables et financières</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51514'">
        <xsl:text>DEC diplôme d'expertise comptable</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51516'">
        <xsl:text>DCU diplôme comptable d'université</xsl:text>
      </xsl:when>
      <xsl:when test="$key='51517'">
        <xsl:text>MSTCF maîtrise sciences et techniques comptables et financières</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52001'">
        <xsl:text>DIPLOME SPECIFIQUE ETABLISSEMENTS ENSEIGNEMENT SUPERIEUR</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52101'">
        <xsl:text>DIPLOME UNIVERSITAIRE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52102'">
        <xsl:text>DU diplôme d'université</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52103'">
        <xsl:text>DCL diplôme de compétence en langue</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52130'">
        <xsl:text>DHEPS diplôme des hautes études de pratique sociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52150'">
        <xsl:text>diplôme spécifique des grands établissements</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52151'">
        <xsl:text>diplôme d'écoles normales supérieures</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52152'">
        <xsl:text>CAPES certificat d'aptitude pédagogique à l'enseignement du second degré</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52153'">
        <xsl:text>agrégation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52154'">
        <xsl:text>diplôme des instituts d'études politiques (IEP)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52155'">
        <xsl:text>diplôme de l'Institut des langues et civilisations orientales (INALCO)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52156'">
        <xsl:text>diplôme de l'école nationale supérieure des sciences de l'information et des bibliothèques (ENSSIB)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52157'">
        <xsl:text>CAPET certificat d'aptitude pédagogique à l'enseignement technique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52301'">
        <xsl:text>DIPLOME CNAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52302'">
        <xsl:text>DPCT diplôme de premier cycle technique CNAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52303'">
        <xsl:text>DPCE diplôme de premier cycle économique CNAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52304'">
        <xsl:text>DPC diplôme de premier cycle CNAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52305'">
        <xsl:text>DEST diplôme d'études supérieures techniques CNAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52306'">
        <xsl:text>DESE diplôme d'études supérieures économiques CNAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52307'">
        <xsl:text>DESA diplôme d'études supérieures appliquées CNAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52308'">
        <xsl:text>diplôme d'ergonomiste CNAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52309'">
        <xsl:text>diplôme d'économiste CNAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52310'">
        <xsl:text>diplôme de psychologue du travail CNAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52311'">
        <xsl:text>diplôme d'administration et gestion du personnel CNAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52316'">
        <xsl:text>DPA diplôme professionnel approfondi CNAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52321'">
        <xsl:text>certificat de compétence CNAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52322'">
        <xsl:text>certificat professionnel CNAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52323'">
        <xsl:text>DSG diplôme supérieur de gestion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52324'">
        <xsl:text>DSC diplôme supérieur de comptabilité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52325'">
        <xsl:text>bachelor</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52350'">
        <xsl:text>diplôme des instituts du CNAM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52351'">
        <xsl:text>diplôme des hautes études d'assurances (CHEA)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52352'">
        <xsl:text>diplôme d'études d'assurances (ENASS)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52353'">
        <xsl:text>diplôme d'études économiques et juridiques appliquées à la construction et à l'habitation (ICH)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52354'">
        <xsl:text>diplôme d'études supérieures des techniques d'organisation (IESTO)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52355'">
        <xsl:text>diplôme supérieur du froid industriel (IFFI)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52356'">
        <xsl:text>diplôme de formation des cadres supérieurs de la vente (ICSV)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52357'">
        <xsl:text>diplôme de topométrie (IT)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52358'">
        <xsl:text>diplôme supérieur des sciences et techniques de l'information et de la documentation (INTD)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52359'">
        <xsl:text>diplôme technique de documentaliste (INTD)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52360'">
        <xsl:text>diplôme de l'Institut National des techniques économiques et comptables (INTEC)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52361'">
        <xsl:text>diplôme de technicien supérieur de la mer (INTECHMER)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52362'">
        <xsl:text>diplôme des transports internationaux et des ports (ITIP)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52363'">
        <xsl:text>diplôme technique de prévision économique et sociale (ITPES)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52364'">
        <xsl:text>diplôme de cadre de la fonction formation (FFPS)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52365'">
        <xsl:text>diplôme d'études supérieures de l'institut technique de banque (ITB)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52401'">
        <xsl:text>DIPLOME GRANDES ECOLES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52402'">
        <xsl:text>mastère spécialisé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52403'">
        <xsl:text>diplôme des écoles supérieures commerciales</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52501'">
        <xsl:text>DIPLOME INTERNATIONAL</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52510'">
        <xsl:text>diplôme de langue</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52511'">
        <xsl:text>diplôme de langue de Chambre de commerce étrangère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52512'">
        <xsl:text>diplôme de langue d'université étrangère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52513'">
        <xsl:text>TOEFL test of english as a foreign language</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52514'">
        <xsl:text>GMAT graduate management aptitude test</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52515'">
        <xsl:text>diplôme de langue de l'université de Cambridge</xsl:text>
      </xsl:when>
      <xsl:when test="$key='52520'">
        <xsl:text>MBA master of business administration</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53001'">
        <xsl:text>DIPLOME D'ETAT A FINALITE PROFESSIONNELLE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53101'">
        <xsl:text>DIPLOME MINISTERE CHARGE DE L'AGRICULTURE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53102'">
        <xsl:text>CAPA certificat d'aptitude professionnelle agricole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53103'">
        <xsl:text>BEPA brevet d'études professionnelles agricoles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53104'">
        <xsl:text>BPA brevet professionnel agricole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53105'">
        <xsl:text>CCTAR certificat de capacité technique agricole rurale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53106'">
        <xsl:text>BTA brevet de technicien agricole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53107'">
        <xsl:text>BTSA brevet de technicien supérieur agricole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53110'">
        <xsl:text>CS certificat de spécialisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53201'">
        <xsl:text>DIPLOME MINISTERE CHARGE DE LA CULTURE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53203'">
        <xsl:text>DNAT diplôme national d'arts et techniques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53204'">
        <xsl:text>DSAA diplôme supérieur d'arts appliqués</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53205'">
        <xsl:text>DNSAP diplôme national supérieur d'arts plastiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53206'">
        <xsl:text>DNSEP diplôme national supérieur d'expression plastique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53207'">
        <xsl:text>diplôme national des Beaux-Arts</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53208'">
        <xsl:text>diplôme d'architecte DPLG</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53210'">
        <xsl:text>CEAP certificat d'études d'arts plastiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53211'">
        <xsl:text>DNAP diplôme national d'arts plastiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53212'">
        <xsl:text>CESAP certificat d'études supérieures d'arts plastiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53213'">
        <xsl:text>diplôme de l'ENSAD - Ecole nationale supérieure des arts décoratifs</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53214'">
        <xsl:text>diplôme de l'ENSCI - Ecole nationale supérieure de création industrielle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53216'">
        <xsl:text>DPEA diplôme propre des écoles d'architecture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53220'">
        <xsl:text>diplôme d'écoles d'art régionales ou municipales</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53230'">
        <xsl:text>diplôme d'écoles de musique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53231'">
        <xsl:text>diplôme d'Etat de professeur de musique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53232'">
        <xsl:text>certificat d'aptitude aux fonctions de professeur ou directeur d'écoles de musique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53233'">
        <xsl:text>DUMI diplôme universitaire de musicien intervenant à l'école élémentaire et préélémentaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53240'">
        <xsl:text>diplôme d'écoles de danse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53241'">
        <xsl:text>diplôme d'Etat de professeur de danse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53301'">
        <xsl:text>DIPLOME MINISTERE CHARGE DU COMMERCE ET DE l'ARTISANAT</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53310'">
        <xsl:text>DIPLOME CHAMBRES DE METIERS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53312'">
        <xsl:text>BC brevet de compagnon</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53313'">
        <xsl:text>BM brevet de maîtrise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53314'">
        <xsl:text>BCCEA brevet de collaborateur de chef d'entreprise artisanale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53315'">
        <xsl:text>BTM brevet technique des métiers</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53316'">
        <xsl:text>BMS brevet de maîtrise supérieur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53317'">
        <xsl:text>CTM certificat technique des métiers</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53318'">
        <xsl:text>BTMS brevet technique des métiers supérieur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53320'">
        <xsl:text>DIPLOME CHAMBRES DE COMMERCE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53321'">
        <xsl:text>diplôme des centres d'enseignement des langues (CEL)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53322'">
        <xsl:text>diplôme des instituts de promotion commerciale (IPC)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53323'">
        <xsl:text>CCS certificat consulaire de spécialisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53401'">
        <xsl:text>DIPLOME MINISTERE CHARGE DE LA DEFENSE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53501'">
        <xsl:text>DIPLOME MINISTERE CHARGE DES SPORTS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53510'">
        <xsl:text>DIPLOME SPORTIF</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53511'">
        <xsl:text>BEAAPT brevet d'Etat d'animateur d'activités physiques pour tous</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53512'">
        <xsl:text>BEES brevet d'Etat d'éducateur sportif 1er degré</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53513'">
        <xsl:text>BEES brevet d'Etat d'éducateur sportif 2ème degré</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53514'">
        <xsl:text>BEES brevet d'Etat d'éducateur sportif 3ème degré</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53515'">
        <xsl:text>BEES AN (activités de la natation)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53516'">
        <xsl:text>brevet d'Etat alpinisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53517'">
        <xsl:text>brevet d'Etat alpinisme - accompagnateur moyenne montagne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53519'">
        <xsl:text>brevet d'Etat alpinisme - aspirant guide</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53520'">
        <xsl:text>brevet d'Etat alpinisme - guide haute montagne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53521'">
        <xsl:text>BEAECPC brevet d'Etat d'aptitude à l'enseignement de la culture physique et du culturisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53530'">
        <xsl:text>DIPLOME JEUNESSE ET VIE ASSOCIATIVE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53531'">
        <xsl:text>BAFA brevet d'aptitude aux fonctions d'animateur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53532'">
        <xsl:text>BASE brevet d'aptitude à l'animation socio-éducative</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53533'">
        <xsl:text>BEATEP brevet d'Etat d'animateur technicien de l'éducation populaire et de jeunesse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53534'">
        <xsl:text>DEFA diplôme d'Etat aux fonctions d'animation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53535'">
        <xsl:text>BAFD brevet d'aptitude aux fonctions de directeur de centre de vacances et de loisirs</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53536'">
        <xsl:text>BAPAAT brevet d'aptitude professionnelle d'assistant animateur technicien de la jeunesse et des sports</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53537'">
        <xsl:text>DEDPAD diplôme d'Etat de directeur de projet d'animation et de développement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53538'">
        <xsl:text>BP JEPS brevet professionnel de la jeunesse, de l'éducation populaire et du sport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53701'">
        <xsl:text>DIPLOME MINISTERE CHARGE DE L'AVIATION CIVILE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53705'">
        <xsl:text>MN brevet licence mécanicien navigant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53708'">
        <xsl:text>brevet de pilote professionnel hélicoptère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53709'">
        <xsl:text>qualification vol aux instruments - hélicoptère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53710'">
        <xsl:text>brevet de pilote de ligne - hélicoptère</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53720'">
        <xsl:text>brevet licence d'ingénieur navigant de l'aviation civile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53721'">
        <xsl:text>certificat de sécurité et sauvetage hôtesses et stewards</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53722'">
        <xsl:text>qualification vol aux instruments avion - IR(A)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53723'">
        <xsl:text>licence de pilote de ligne - ATPL(A)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53724'">
        <xsl:text>licence de pilote privé - PPL</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53725'">
        <xsl:text>licence de pilote professionnel avion - CPL(A)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53726'">
        <xsl:text>MCC multicrew coordination</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53801'">
        <xsl:text>DIPLOME MINISTERE CHARGE DES AFFAIRES SOCIALES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53810'">
        <xsl:text>DIPLOME SECTEUR SANITAIRE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53811'">
        <xsl:text>diplôme d'Etat d'infirmier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53812'">
        <xsl:text>diplôme d'Etat de puéricultrice</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53813'">
        <xsl:text>diplôme d'Etat de sage-femme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53814'">
        <xsl:text>diplôme d'Etat de masseur kinésithérapeute</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53815'">
        <xsl:text>diplôme d'Etat d'ergothérapeute</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53816'">
        <xsl:text>diplôme d'Etat de psychomotricien</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53817'">
        <xsl:text>diplôme d'Etat d'audioprothésiste</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53820'">
        <xsl:text>diplôme d'Etat de pédicure podologue</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53821'">
        <xsl:text>diplôme professionnel d'aide-soignant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53824'">
        <xsl:text>diplôme professionnel d'auxiliaire de puériculture</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53825'">
        <xsl:text>CCA certificat de capacité ambulancier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53826'">
        <xsl:text>certificat de capacité orthophoniste</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53827'">
        <xsl:text>certificat de capacité orthoptiste</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53830'">
        <xsl:text>certificat de cadre sage-femme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53836'">
        <xsl:text>AFPS attestation de formation aux premiers secours</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53838'">
        <xsl:text>diplôme d'Etat de technicien en analyses bio-médicales</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53839'">
        <xsl:text>diplôme d'Etat de manipulateur en électroradiologie médicale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53840'">
        <xsl:text>diplôme d'Etat d'infirmier de bloc opératoire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53841'">
        <xsl:text>diplôme d'Etat d'infirmier anesthésiste</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53842'">
        <xsl:text>diplôme de cadre de santé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53850'">
        <xsl:text>DIPLOME SECTEUR SOCIAL</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53851'">
        <xsl:text>diplôme d'Etat d'assistant de service social</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53852'">
        <xsl:text>DEES diplôme d'Etat d'éducateur spécialisé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53853'">
        <xsl:text>diplôme d'Etat d'éducateur de jeunes enfants</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53854'">
        <xsl:text>diplôme d'Etat de conseiller en économie sociale et familiale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53855'">
        <xsl:text>CAFME certificat d'aptitude aux fonctions de moniteur éducateur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53856'">
        <xsl:text>DSTS diplôme supérieur en travail social</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53860'">
        <xsl:text>certificat d'aptitude aux fonctions d'aide médico-psychologique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53862'">
        <xsl:text>diplôme d'Etat de technicien de l'intervention sociale et familiale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53863'">
        <xsl:text>diplôme d'Etat d'auxiliaire de vie sociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53864'">
        <xsl:text>certificat d'aptitude aux fonctions de directeur d'établissement ou de service d'intervention sociale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53865'">
        <xsl:text>CAFETS certificat d'aptitude aux fonctions d'éducateur technique spécialisé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53901'">
        <xsl:text>DIPLOME MINISTERE CHARGE DU TRAVAIL</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53920'">
        <xsl:text>CPP certificat de perfectionnement professionnel AFPA</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53940'">
        <xsl:text>titre professionnel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53941'">
        <xsl:text>CCP certificat de compétences professionnelles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='53942'">
        <xsl:text>CCS certificat complémentaire de spécialisation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54001'">
        <xsl:text>DIPLOME MINISTERE CHARGE DE LA MER</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54010'">
        <xsl:text>CAPM certificat d'aptitude professionnelle maritime</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54011'">
        <xsl:text>BEPM brevet d'études professionnelles maritimes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54012'">
        <xsl:text>certificat d'initiation nautique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54020'">
        <xsl:text>DIPLOME MARINE MARCHANDE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54021'">
        <xsl:text>brevet de chef de quart</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54022'">
        <xsl:text>brevet de chef de quart de navire de mer</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54023'">
        <xsl:text>brevet de second polyvalent</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54024'">
        <xsl:text>brevet de capitaine de 1ère classe de la navigation maritime</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54025'">
        <xsl:text>DESMM diplôme d'études supérieures de la marine marchande</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54026'">
        <xsl:text>brevet de chef de quart passerelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54027'">
        <xsl:text>brevet de second capitaine 3000 UMS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54028'">
        <xsl:text>brevet de capitaine 3000 UMS</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54029'">
        <xsl:text>brevet de second capitaine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54030'">
        <xsl:text>brevet de capitaine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54032'">
        <xsl:text>brevet de chef de quart machine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54033'">
        <xsl:text>brevet de second mécanicien 3000 KW</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54034'">
        <xsl:text>brevet de chef mécanicien 3000 KW</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54035'">
        <xsl:text>brevet de second mécanicien</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54036'">
        <xsl:text>brevet de chef mécanicien</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54037'">
        <xsl:text>brevet de patron de petite navigation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54038'">
        <xsl:text>brevet de chef de quart de navigation côtière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54039'">
        <xsl:text>brevet de patron de navigation côtière</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54040'">
        <xsl:text>permis de conduire les moteurs marins 250 KW</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54041'">
        <xsl:text>brevet de mécanicien 750 KW</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54060'">
        <xsl:text>DIPLOME PECHE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54061'">
        <xsl:text>brevet de capitaine de pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54062'">
        <xsl:text>brevet d'officier mécanicien à la pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54063'">
        <xsl:text>brevet d'officier mécanicien de 3ème classe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54064'">
        <xsl:text>certificat de capacité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54065'">
        <xsl:text>brevet de lieutenant de pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54066'">
        <xsl:text>brevet de patron de pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54067'">
        <xsl:text>certificat de motoriste à la pêche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54068'">
        <xsl:text>permis de conduire les moteurs</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54069'">
        <xsl:text>certificat d'aptitude à la conduite des navires conchylicoles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54080'">
        <xsl:text>DIPLOME PLAISANCE PROFESSIONNELLE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54081'">
        <xsl:text>brevet de patron à la plaisance voile</xsl:text>
      </xsl:when>
      <xsl:when test="$key='54082'">
        <xsl:text>mention capitaine de yacht</xsl:text>
      </xsl:when>
      <xsl:when test="$key='55001'">
        <xsl:text>TITRES ET DIPLOMES HOMOLOGUES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='55010'">
        <xsl:text>diplôme homologué au niveau V</xsl:text>
      </xsl:when>
      <xsl:when test="$key='55020'">
        <xsl:text>diplôme homologué au niveau IV</xsl:text>
      </xsl:when>
      <xsl:when test="$key='55030'">
        <xsl:text>diplôme homologué au niveau III</xsl:text>
      </xsl:when>
      <xsl:when test="$key='55040'">
        <xsl:text>diplôme homologué au niveau II</xsl:text>
      </xsl:when>
      <xsl:when test="$key='55045'">
        <xsl:text>diplôme homologué au niveau I/II</xsl:text>
      </xsl:when>
      <xsl:when test="$key='55050'">
        <xsl:text>diplôme homologué au niveau I</xsl:text>
      </xsl:when>
      <xsl:when test="$key='56001'">
        <xsl:text>DIPLOME DE BRANCHES PROFESSIONNELLES</xsl:text>
      </xsl:when>
      <xsl:when test="$key='56010'">
        <xsl:text>CQP certificat de qualification professionnelle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61001'">
        <xsl:text>EUROPE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61101'">
        <xsl:text>EUROPE OCCIDENTALE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61119'">
        <xsl:text>Allemagne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61120'">
        <xsl:text>Andorre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61122'">
        <xsl:text>Autriche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61123'">
        <xsl:text>Belgique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61124'">
        <xsl:text>Danemark</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61125'">
        <xsl:text>Espagne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61126'">
        <xsl:text>Finlande</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61127'">
        <xsl:text>France</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61129'">
        <xsl:text>Irlande</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61130'">
        <xsl:text>Islande</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61131'">
        <xsl:text>Italie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61139'">
        <xsl:text>Liechtenstein</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61140'">
        <xsl:text>Luxembourg</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61141'">
        <xsl:text>Malte</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61142'">
        <xsl:text>Norvège</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61143'">
        <xsl:text>Pays-Bas</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61144'">
        <xsl:text>Portugal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61145'">
        <xsl:text>Royaume-Uni</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61146'">
        <xsl:text>Suède</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61147'">
        <xsl:text>Suisse</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61201'">
        <xsl:text>EUROPE ORIENTALE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61202'">
        <xsl:text>Albanie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61221'">
        <xsl:text>Biélorussie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61222'">
        <xsl:text>Bosnie-Herzégovine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61223'">
        <xsl:text>Bulgarie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61224'">
        <xsl:text>Croatie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61225'">
        <xsl:text>Estonie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61226'">
        <xsl:text>Géorgie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61239'">
        <xsl:text>Grèce</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61240'">
        <xsl:text>Hongrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61241'">
        <xsl:text>Lettonie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61242'">
        <xsl:text>Lituanie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61243'">
        <xsl:text>Macédoine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61244'">
        <xsl:text>Moldavie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61245'">
        <xsl:text>Pologne</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61246'">
        <xsl:text>République tchèque</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61247'">
        <xsl:text>Roumanie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61248'">
        <xsl:text>Russie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61249'">
        <xsl:text>Slovaquie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61250'">
        <xsl:text>Slovénie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61251'">
        <xsl:text>Ukraine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61253'">
        <xsl:text>Chypre</xsl:text>
      </xsl:when>
      <xsl:when test="$key='61254'">
        <xsl:text>Serbie Monténégro</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62001'">
        <xsl:text>ASIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62101'">
        <xsl:text>ASIE DU SUD</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62102'">
        <xsl:text>Bangladesh</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62103'">
        <xsl:text>Bhoutan</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62104'">
        <xsl:text>Inde</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62105'">
        <xsl:text>Maldives</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62106'">
        <xsl:text>Népal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62107'">
        <xsl:text>Pakistan</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62108'">
        <xsl:text>Sri Lanka</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62201'">
        <xsl:text>ASIE DU SUD EST</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62212'">
        <xsl:text>Brunei</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62213'">
        <xsl:text>Cambodge</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62214'">
        <xsl:text>Indonésie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62215'">
        <xsl:text>Laos</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62216'">
        <xsl:text>Malaisie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62217'">
        <xsl:text>Myanmar</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62218'">
        <xsl:text>Palau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62219'">
        <xsl:text>Philippines</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62220'">
        <xsl:text>Singapour</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62221'">
        <xsl:text>Thaïlande</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62222'">
        <xsl:text>Viêt Nam</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62223'">
        <xsl:text>Timor Oriental</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62301'">
        <xsl:text>EXTREME ORIENT</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62302'">
        <xsl:text>Chine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62303'">
        <xsl:text>Corée du Nord</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62304'">
        <xsl:text>Corée du Sud</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62306'">
        <xsl:text>Japon</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62308'">
        <xsl:text>Taiwan</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62401'">
        <xsl:text>ASIE CENTRALE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62402'">
        <xsl:text>Afghanistan</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62403'">
        <xsl:text>Kazakhstan</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62404'">
        <xsl:text>Kirghizistan</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62405'">
        <xsl:text>Mongolie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62406'">
        <xsl:text>Ouzbékistan</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62407'">
        <xsl:text>Tadjikistan</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62408'">
        <xsl:text>Turkménistan</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62501'">
        <xsl:text>MOYEN ORIENT</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62502'">
        <xsl:text>Arabie Saoudite</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62503'">
        <xsl:text>Arménie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62504'">
        <xsl:text>Azerbaïdjan</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62505'">
        <xsl:text>Bahreïn</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62507'">
        <xsl:text>Emirats Arabes Unis</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62508'">
        <xsl:text>Irak</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62509'">
        <xsl:text>Iran</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62510'">
        <xsl:text>Israël</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62511'">
        <xsl:text>Jordanie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62512'">
        <xsl:text>Koweït</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62513'">
        <xsl:text>Liban</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62514'">
        <xsl:text>Oman</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62520'">
        <xsl:text>Qatar</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62521'">
        <xsl:text>Syrie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62522'">
        <xsl:text>Turquie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='62523'">
        <xsl:text>Yémen</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63001'">
        <xsl:text>AFRIQUE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63101'">
        <xsl:text>AFRIQUE DU NORD</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63102'">
        <xsl:text>Algérie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63103'">
        <xsl:text>Egypte</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63104'">
        <xsl:text>Libye</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63105'">
        <xsl:text>Maroc</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63106'">
        <xsl:text>Tunisie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63201'">
        <xsl:text>AFRIQUE CENTRALE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63211'">
        <xsl:text>Burundi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63212'">
        <xsl:text>Congo</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63213'">
        <xsl:text>Congo (Rép. Démocratique)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63214'">
        <xsl:text>Gabon</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63215'">
        <xsl:text>Guinée Equatoriale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63216'">
        <xsl:text>République Centrafricaine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63217'">
        <xsl:text>Rwanda</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63219'">
        <xsl:text>Tchad</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63301'">
        <xsl:text>AFRIQUE DE L'OUEST</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63302'">
        <xsl:text>Bénin</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63303'">
        <xsl:text>Burkina Faso</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63304'">
        <xsl:text>Cameroun</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63305'">
        <xsl:text>Cap-Vert</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63306'">
        <xsl:text>Côte d'Ivoire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63307'">
        <xsl:text>Gambie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63308'">
        <xsl:text>Ghana</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63309'">
        <xsl:text>Guinée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63310'">
        <xsl:text>Guinée Bissau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63311'">
        <xsl:text>Libéria</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63312'">
        <xsl:text>Mali</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63313'">
        <xsl:text>Mauritanie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63314'">
        <xsl:text>Niger</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63315'">
        <xsl:text>Nigéria</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63316'">
        <xsl:text>Sénégal</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63317'">
        <xsl:text>Sierra Léone</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63318'">
        <xsl:text>Togo</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63319'">
        <xsl:text>Sao Tome et Principe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63401'">
        <xsl:text>AFRIQUE DE L'EST</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63405'">
        <xsl:text>Comores</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63412'">
        <xsl:text>Djibouti</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63413'">
        <xsl:text>Erythrée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63414'">
        <xsl:text>Ethiopie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63415'">
        <xsl:text>Kenya</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63416'">
        <xsl:text>Madagascar</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63417'">
        <xsl:text>Maurice</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63418'">
        <xsl:text>Ouganda</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63419'">
        <xsl:text>Seychelles</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63420'">
        <xsl:text>Somalie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63421'">
        <xsl:text>Soudan</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63422'">
        <xsl:text>Tanzanie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63501'">
        <xsl:text>AFRIQUE AUSTRALE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63502'">
        <xsl:text>Afrique du Sud</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63503'">
        <xsl:text>Angola</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63504'">
        <xsl:text>Botswana</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63506'">
        <xsl:text>Lesotho</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63507'">
        <xsl:text>Malawi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63508'">
        <xsl:text>Mozambique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63509'">
        <xsl:text>Namibie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63510'">
        <xsl:text>Swaziland</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63511'">
        <xsl:text>Zambie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='63512'">
        <xsl:text>Zimbabwe</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64001'">
        <xsl:text>AMERIQUE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64101'">
        <xsl:text>AMERIQUE DU NORD</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64102'">
        <xsl:text>Canada</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64103'">
        <xsl:text>Etats-Unis</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64104'">
        <xsl:text>Mexique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64201'">
        <xsl:text>AMERIQUE CENTRALE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64226'">
        <xsl:text>Antigua et Barbuda</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64227'">
        <xsl:text>Bahamas</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64228'">
        <xsl:text>Barbade</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64229'">
        <xsl:text>Belize</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64230'">
        <xsl:text>Costa Rica</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64231'">
        <xsl:text>Cuba</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64232'">
        <xsl:text>El Salvador</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64233'">
        <xsl:text>Grenade</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64234'">
        <xsl:text>Guatemala</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64235'">
        <xsl:text>Haïti</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64236'">
        <xsl:text>Honduras</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64237'">
        <xsl:text>Jamaïque</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64238'">
        <xsl:text>Nicaragua</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64239'">
        <xsl:text>Panama</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64240'">
        <xsl:text>République Dominicaine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64241'">
        <xsl:text>Trinité-et-Tobago</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64301'">
        <xsl:text>AMERIQUE DU SUD</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64302'">
        <xsl:text>Argentine</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64303'">
        <xsl:text>Bolivie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64304'">
        <xsl:text>Brésil</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64305'">
        <xsl:text>Chili</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64306'">
        <xsl:text>Colombie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64307'">
        <xsl:text>Equateur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64314'">
        <xsl:text>Guyana</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64316'">
        <xsl:text>Paraguay</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64317'">
        <xsl:text>Pérou</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64318'">
        <xsl:text>Surinam</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64319'">
        <xsl:text>Uruguay</xsl:text>
      </xsl:when>
      <xsl:when test="$key='64321'">
        <xsl:text>Venezuela</xsl:text>
      </xsl:when>
      <xsl:when test="$key='65001'">
        <xsl:text>OCEANIE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='65102'">
        <xsl:text>Australie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='65103'">
        <xsl:text>Fidji</xsl:text>
      </xsl:when>
      <xsl:when test="$key='65108'">
        <xsl:text>Iles Marshall</xsl:text>
      </xsl:when>
      <xsl:when test="$key='65109'">
        <xsl:text>Iles Salomon</xsl:text>
      </xsl:when>
      <xsl:when test="$key='65110'">
        <xsl:text>Kiribati</xsl:text>
      </xsl:when>
      <xsl:when test="$key='65111'">
        <xsl:text>Micronésie</xsl:text>
      </xsl:when>
      <xsl:when test="$key='65112'">
        <xsl:text>Nauru</xsl:text>
      </xsl:when>
      <xsl:when test="$key='65113'">
        <xsl:text>Nouvelle-Zélande</xsl:text>
      </xsl:when>
      <xsl:when test="$key='65114'">
        <xsl:text>Papouasie-Nouvelle-Guinée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='65115'">
        <xsl:text>Samoa</xsl:text>
      </xsl:when>
      <xsl:when test="$key='65116'">
        <xsl:text>Tonga</xsl:text>
      </xsl:when>
      <xsl:when test="$key='65117'">
        <xsl:text>Tuvalu</xsl:text>
      </xsl:when>
      <xsl:when test="$key='65118'">
        <xsl:text>Vanuatu</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80001'">
        <xsl:text>agent de l'hospitalisation publique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80002'">
        <xsl:text>agent de maîtrise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80003'">
        <xsl:text>agent de l'Etat et des collectivités locales</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80004'">
        <xsl:text>artisan, salarié de l'artisanat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80005'">
        <xsl:text>bas niveau de qualification</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80006'">
        <xsl:text>cadre d'entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80007'">
        <xsl:text>cadre privé d'emploi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80008'">
        <xsl:text>chef d'entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80009'">
        <xsl:text>commerçant, salarié du commerce</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80010'">
        <xsl:text>consultant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80011'">
        <xsl:text>demandeur d'emploi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80012'">
        <xsl:text>demandeur d'emploi longue durée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80013'">
        <xsl:text>détenu</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80014'">
        <xsl:text>élu local</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80015'">
        <xsl:text>employé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80016'">
        <xsl:text>enseignant</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80017'">
        <xsl:text>exploitant agricole, salarié agricole</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80018'">
        <xsl:text>femme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80019'">
        <xsl:text>formateur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80020'">
        <xsl:text>handicapé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80021'">
        <xsl:text>illettré</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80022'">
        <xsl:text>immigré</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80023'">
        <xsl:text>ingénieur, cadre technique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80024'">
        <xsl:text>jeune 16-25 ans</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80025'">
        <xsl:text>jeune demandeur d'emploi (16-25 ans)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80026'">
        <xsl:text>jeune sous contrat d'adaptation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80027'">
        <xsl:text>jeune sous contrat d'apprentissage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80028'">
        <xsl:text>jeune sous contrat d'orientation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80029'">
        <xsl:text>jeune sous contrat de qualification</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80030'">
        <xsl:text>licencié pour motif économique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80031'">
        <xsl:text>médecin du travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80032'">
        <xsl:text>ouvrier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80033'">
        <xsl:text>ouvrier qualifié</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80034'">
        <xsl:text>policier et militaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80035'">
        <xsl:text>profession de l'information</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80036'">
        <xsl:text>profession de la santé et du travail social</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80037'">
        <xsl:text>profession des arts et du spectacle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80038'">
        <xsl:text>profession libérale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80039'">
        <xsl:text>profession scientifique, chercheur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80040'">
        <xsl:text>public dépendant des services sociaux</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80041'">
        <xsl:text>public DOM-TOM</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80042'">
        <xsl:text>réfugié</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80043'">
        <xsl:text>représentant du personnel, syndicaliste</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80044'">
        <xsl:text>salarié</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80045'">
        <xsl:text>salarié en reconversion</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80047'">
        <xsl:text>salarié sous contrat de travail particulier</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80048'">
        <xsl:text>salarié sous contrat initiative emploi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80049'">
        <xsl:text>technicien</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80050'">
        <xsl:text>individuel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80051'">
        <xsl:text>intermittent du spectacle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80052'">
        <xsl:text>jeune en alternance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80055'">
        <xsl:text>agent fonction publique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80056'">
        <xsl:text>tout public</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80057'">
        <xsl:text>salarié sous contrat accès emploi (DOM-TOM)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80058'">
        <xsl:text>public en emploi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80059'">
        <xsl:text>public sans emploi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='80999'">
        <xsl:text>autre public</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94000'">
        <xsl:text>accompagnement stagiaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94001'">
        <xsl:text>accueil stagiaires étrangers</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94002'">
        <xsl:text>atelier pédagogique personnalisé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94003'">
        <xsl:text>conception de formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94004'">
        <xsl:text>conception jeu pédagogique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94005'">
        <xsl:text>conception multimédia</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94006'">
        <xsl:text>conseil et service en formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94007'">
        <xsl:text>étude et recherche</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94008'">
        <xsl:text>formation continue</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94009'">
        <xsl:text>formation initiale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94010'">
        <xsl:text>FPC activité principale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94011'">
        <xsl:text>FPC activité secondaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94012'">
        <xsl:text>gestion parcours de formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94013'">
        <xsl:text>ingénierie de formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94014'">
        <xsl:text>mise en place de formation à l'étranger</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94015'">
        <xsl:text>orientation information</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94016'">
        <xsl:text>prestation de bilan de compétence</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94017'">
        <xsl:text>recrutement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94018'">
        <xsl:text>validation des acquis</xsl:text>
      </xsl:when>
      <xsl:when test="$key='94019'">
        <xsl:text>aide à la recherche d'emploi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='95001'">
        <xsl:text>formation à temps partiel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='95002'">
        <xsl:text>formation à temps plein</xsl:text>
      </xsl:when>
      <xsl:when test="$key='95003'">
        <xsl:text>formation en continu</xsl:text>
      </xsl:when>
      <xsl:when test="$key='95004'">
        <xsl:text>formation en discontinu</xsl:text>
      </xsl:when>
      <xsl:when test="$key='95005'">
        <xsl:text>formation en alternance</xsl:text>
      </xsl:when>
      <xsl:when test="$key='95006'">
        <xsl:text>entrée/sortie permanente</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96001'">
        <xsl:text>cours théorique en salle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96002'">
        <xsl:text>cours en centre de formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96003'">
        <xsl:text>formation en partie à l'étranger</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96004'">
        <xsl:text>formation hors temps de travail</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96005'">
        <xsl:text>admission après entretien avec l'ANPE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96006'">
        <xsl:text>admission après entretien avec l'organisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96007'">
        <xsl:text>admission après test avec l'ANPE</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96008'">
        <xsl:text>admission après test avec l'organisme</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96009'">
        <xsl:text>formation individualisée</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96010'">
        <xsl:text>formation inter-entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96011'">
        <xsl:text>formation intra-entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96012'">
        <xsl:text>formation modulaire</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96013'">
        <xsl:text>contrat d'apprentissage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96014'">
        <xsl:text>contrat de qualification</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96015'">
        <xsl:text>suivi à 3 mois des demandeurs d'emploi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96016'">
        <xsl:text>suivi à 6 mois des demandeurs d'emploi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96017'">
        <xsl:text>restauration</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96018'">
        <xsl:text>hébergement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96019'">
        <xsl:text>transport</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96020'">
        <xsl:text>accès handicapés</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96021'">
        <xsl:text>formation à l'étranger</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96022'">
        <xsl:text>cours par correspondance uniquement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96023'">
        <xsl:text>cours par téléphone</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96024'">
        <xsl:text>autoformation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96025'">
        <xsl:text>autoformation avec tutorat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96026'">
        <xsl:text>autoformation sans tutorat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96027'">
        <xsl:text>chantier-école</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96028'">
        <xsl:text>document pédagogique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96029'">
        <xsl:text>e-formation (via réseaux)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96030'">
        <xsl:text>entreprise ou chantier d'entraînement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96031'">
        <xsl:text>évaluation des acquis de la formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96032'">
        <xsl:text>évaluation pertinence projet professionnel</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96033'">
        <xsl:text>étude de cas</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96034'">
        <xsl:text>formation action</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96035'">
        <xsl:text>jeux de rôle</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96036'">
        <xsl:text>jeu pédagogique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96037'">
        <xsl:text>multimédia hors réseaux (EAO, CD-ROM)</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96038'">
        <xsl:text>recherche action</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96039'">
        <xsl:text>séjour linguistique</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96040'">
        <xsl:text>stage avec période en entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='96041'">
        <xsl:text>travaux pratiques</xsl:text>
      </xsl:when>
      <xsl:when test="$key='97001'">
        <xsl:text>qualification</xsl:text>
      </xsl:when>
      <xsl:when test="$key='97002'">
        <xsl:text>perfectionnement</xsl:text>
      </xsl:when>
      <xsl:when test="$key='97003'">
        <xsl:text>élargissement des compétences</xsl:text>
      </xsl:when>
      <xsl:when test="$key='97004'">
        <xsl:text>création d'entreprise</xsl:text>
      </xsl:when>
      <xsl:when test="$key='97005'">
        <xsl:text>remise à niveau</xsl:text>
      </xsl:when>
      <xsl:when test="$key='97500'">
        <xsl:text>attestation de capacité</xsl:text>
      </xsl:when>
      <xsl:when test="$key='97501'">
        <xsl:text>attestation suivi ou présence</xsl:text>
      </xsl:when>
      <xsl:when test="$key='97502'">
        <xsl:text>certificat de l'école ou titre privé</xsl:text>
      </xsl:when>
      <xsl:when test="$key='97503'">
        <xsl:text>livret de stage</xsl:text>
      </xsl:when>
      <xsl:when test="$key='97504'">
        <xsl:text>unité capitalisable</xsl:text>
      </xsl:when>
      <xsl:when test="$key='97505'">
        <xsl:text>unité de valeur</xsl:text>
      </xsl:when>
      <xsl:when test="$key='97506'">
        <xsl:text>titre ou diplôme homologué</xsl:text>
      </xsl:when>
      <xsl:when test="$key='97507'">
        <xsl:text>diplôme d'Etat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='97508'">
        <xsl:text>cqp</xsl:text>
      </xsl:when>
      <xsl:when test="$key='98001'">
        <xsl:text>dispositif de formation</xsl:text>
      </xsl:when>
      <xsl:when test="$key='98002'">
        <xsl:text>dispositif de formation demandeurs d'emploi</xsl:text>
      </xsl:when>
      <xsl:when test="$key='98003'">
        <xsl:text>dispositif de formation jeunes</xsl:text>
      </xsl:when>
      <xsl:when test="$key='98004'">
        <xsl:text>stage agréé par l'Etat</xsl:text>
      </xsl:when>
      <xsl:when test="$key='98005'">
        <xsl:text>stage conventionné par la région</xsl:text>
      </xsl:when>
      <xsl:when test="$key='98006'">
        <xsl:text>stage conventionné par une collectivité territoriale hors région</xsl:text>
      </xsl:when>
      <xsl:when test="$key='99001'">
        <xsl:text>inter-régionale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='99002'">
        <xsl:text>régionale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='99003'">
        <xsl:text>locale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='99004'">
        <xsl:text>départementale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='99005'">
        <xsl:text>nationale</xsl:text>
      </xsl:when>
      <xsl:when test="$key='99999'">
        <xsl:text>information non communiquée</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>UNKNOWN</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
