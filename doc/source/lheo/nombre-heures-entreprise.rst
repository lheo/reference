.. _nombre-heures-entreprise:

Nombre d'heures en entreprise
+++++++++++++++++++++++++++++

.. code-block:: xml

  <nombre-heures-entreprise>200</nombre-heures-entreprise>


Cet élément permet d'indiquer en heures la durée totale de la formation en entreprise.

Type
""""

Entier [0,99999].


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`nombre-heures-total-entreprise-terme`


   Cette donnée correspond au nombre d’heures maximum en entreprise indiqué par l’organisme responsable de l’action de formation. 


