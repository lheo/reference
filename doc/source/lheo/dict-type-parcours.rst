.. _dict-type-parcours:

Type de parcours de formation
+++++++++++++++++++++++++++++



.. list-table:: Table ``dict-type-parcours``
   :widths: 25 75
   :header-rows: 1

   * - Clé
     - Valeur
   * - ``1``
     - en groupe (non personnalisable)
   * - ``2``
     - individualisé
   * - ``3``
     - modularisé
   * - ``4``
     - mixte


Utilisée dans  :ref:`parcours-de-formation`.

