.. _frais-restants:

Frais restants à la charge du stagiaire
+++++++++++++++++++++++++++++++++++++++

.. code-block:: xml

  <frais-restants>...</frais-restants>




Type
""""

Texte de 0 à 200 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

