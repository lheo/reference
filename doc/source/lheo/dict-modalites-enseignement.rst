.. _dict-modalites-enseignement:

Formation présentielle ou a distance
++++++++++++++++++++++++++++++++++++



.. list-table:: Table ``dict-modalites-enseignement``
   :widths: 25 75
   :header-rows: 1

   * - Clé
     - Valeur
   * - ``0``
     - formation entièrement présentielle
   * - ``1``
     - formation mixte
   * - ``2``
     - formation entièrement à distance


Utilisée dans  :ref:`modalites-enseignement`.

