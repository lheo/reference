.. _infos-perimetre-recrutement:

Informations supplémentaires sur le périmètre de recrutement
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: xml

  <infos-perimetre-recrutement>formation européenne</infos-perimetre-recrutement>


Cet élément permet de donner des informations supplémentaires par rapport à l'élément 'code-perimetre-recrutement' sur le périmètre de recrutement. Cet élément est surtout utile dans le cas où le code du périmètre de recrutement est égal à la valeur "Autres", puisqu'il permet de préciser une information non présente dans la table.

Type
""""

Texte de 0 à 50 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`perimetre-recrutement-terme`


   Il correspond, pour une action de formation donnée, à l'espace potentiel de recrutement des apprenants. 


