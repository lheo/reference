.. _acces-handicapes:

Accès handicapés
++++++++++++++++

.. code-block:: xml

  <acces-handicapes>...</acces-handicapes>


Cet élément précise l'organisation matérielle de la formation, en précisant les aspects pratiques liés aux accès et aménagements prévus pour les handicapés. Si cet élément est vide, cela implique qu'il n'y a pas d'aménagement spécifique.

Type
""""

Texte de 1 à 250 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

