.. _prise-en-charge-frais-possible:

Prise en charge des frais de formation possible
+++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: xml

  <prise-en-charge-frais-possible>1</prise-en-charge-frais-possible>


Cet élément indique si une prise en charge des frais de formation est possible (renvoi vers les conseillers en charge de l'élaboration des parcours). Si une prise en charge des frais est possible, le détail des conditions de cette prise en charge devrait être donné dans l'élément «Détails des conditions de prise en charge».

Type
""""

Clé de la table :ref:`dict-boolean`


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`prise-charge-frais-terme`


   Indique si une prise en charge des frais de formation est possible. 


