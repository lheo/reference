.. _codepostal:

Code postal
+++++++++++

.. code-block:: xml

  <codepostal>69000</codepostal>


Cet élément contient un code postal.

Type
""""

Texte de 5 à 5 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`adresse`.

