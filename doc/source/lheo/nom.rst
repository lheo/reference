.. _nom:

Nom
+++

.. code-block:: xml

  <nom>Darcueil</nom>


Le nom d'une personne physique ou le nom d'une entité quelconque.

Type
""""

Texte de 1 à 50 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`coordonnees`.

