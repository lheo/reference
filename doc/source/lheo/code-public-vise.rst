.. _code-public-vise:

Code de public visé
+++++++++++++++++++

.. code-block:: xml

  <code-public-vise>80056</code-public-vise>


Cet élément contient un code de public visé. Ce code est issu du FORMACODE. Il est obligatoire de préciser grâce à l'attribut 'ref' la version du FORMACODE utilisée (par exemple 'V10' pour la version 10 du FORMACODE).

Type
""""

Texte de 5 à 5 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (obligatoire) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`public-vise-terme`


   Il s'agit des différentes catégories de population auxquelles s'adresse l'action de formation. Ces catégories sont définies en fonction de différents types de critères (ex: administratifs, sociaux-économiques, âge, sexe, etc...) 


