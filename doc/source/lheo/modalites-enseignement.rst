.. _modalites-enseignement:

Formation présentielle ou à distance
++++++++++++++++++++++++++++++++++++

.. code-block:: xml

  <modalites-enseignement>0</modalites-enseignement>


Cet élément indique si la formation se déroule en présentiel, à distance ou une combinaison des deux. L’élément 'modalites-pedagogiques' permet de préciser les modalités d’enseignement à distance sous la forme d’un texte.

Type
""""

Clé de la table :ref:`dict-modalites-enseignement`


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`foad-terme`


   Indication permettant de savoir si la formation à lieu dans un centre de formation ou à distance. 


