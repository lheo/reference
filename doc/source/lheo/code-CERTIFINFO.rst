.. _code-CERTIFINFO:

Code CERTIFINFO
+++++++++++++++

.. code-block:: xml

  <code-CERTIFINFO>52747</code-CERTIFINFO>


Cet élément contient un code d'une certification dans le référentiel Certif Info. Certif Info est un référentiel des certifications élaboré en partenariat entre les CARIF-OREF, la CNCP, l'ONISEP, la DGEFP, Pôle emploi et le Centre INFFO (voir http://www.certifinfo.org). Règle de gestion: les codes de Certif Info qui seront utilisés ne doivent être que ceux correspondant à des certifications de droit, ou certifications sur demande qui ont été enregistrées au RNCP.

Type
""""

Texte de 1 à 6 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`certification`.

