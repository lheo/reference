.. _certifiante:

Formation certifiante
+++++++++++++++++++++

.. code-block:: xml

  <certifiante>1</certifiante>


Cet élément indique si la formation permet d'obtenir une certification (diplôme, titre ou certificat de qualification).

Type
""""

Clé de la table :ref:`dict-boolean`


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`formation`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`diplome-terme`


   Document écrit établissant un privilège ou un droit. Il émane d'une autorité compétente, sous le contrôle de l'État. Il conditionne l'accès à certaines professions et à certaines formations ou concours. Il reconnaît au titulaire un niveau de capacité vérifié. 


