.. _prix-total-TTC:

Prix total TTC
++++++++++++++

.. code-block:: xml

  <prix-total-TTC>2500</prix-total-TTC>


Cette donnée correspond au prix total par apprenant affiché par l’organisme responsable de l’action de formation, toutes taxes et tous frais compris.

Type
""""

Texte de 1 à 10 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`prix-total-TTC-terme`


   Cette donnée correspond au coût total maximum par apprenant, affiché par l’organisme responsable de l’action de formation, toutes taxes et tout frais compris. 


