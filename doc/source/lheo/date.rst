.. _date:

Date
++++

.. code-block:: xml

  <date>20050630</date>


Cet élément indique une date. Si une date est inconnue, indiquer 00000000. Pour indiquer un mois, utiliser le format AAAAMM00, pour indiquer une année, utiliser le format AAAA0000.

Type
""""

Date (format ISO ``AAAAMMJJ``). Date inconnue: ``00000000`` (min) ou ``99999999`` (max).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`date-information`.

