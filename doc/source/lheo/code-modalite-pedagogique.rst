.. _code-modalite-pedagogique:

Modalité pédagogique
++++++++++++++++++++

.. code-block:: xml

  <code-modalite-pedagogique>46278</code-modalite-pedagogique>


Cet élément contient un code de modalité pédagogique. Ce code est issu du FORMACODE. Il est obligatoire de préciser grâce à l'attribut 'ref' la version du FORMACODE utilisée (par exemple 'V10' pour la version 10 du FORMACODE).

Type
""""

Texte de 5 à 5 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (obligatoire) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`modalites-pedagogiques-terme`


   Pédagogies mises en œuvres dans le déroulement de la formation, comme des études de cas, des mises en situation, l'individualisation de la formation, les possibilités d'autoformation, etc. 


