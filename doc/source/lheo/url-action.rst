.. _url-action:

URL de l'action
+++++++++++++++

.. code-block:: xml

  <url-action>
    <urlweb>...</urlweb> <!-- [1,3] -->
    <extras>...</extras> <!-- [0,N] -->
  </url-action>


Cet élément contient une adresse de site web de l'action de formation.

Éléments
""""""""

Séquence ordonnée des éléments suivants:

- ``<urlweb>`` :ref:`urlweb` [1, 3]
- ``<extras>`` :ref:`extras` [0, N]



Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

