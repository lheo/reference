.. _modalites-recrutement:

Modalités de recrutement
++++++++++++++++++++++++

.. code-block:: xml

  <modalites-recrutement>Entretien individuel</modalites-recrutement>


Cet élément permet de préciser les modalités de recrutement et d'admission (tests, entretiens).

Type
""""

Texte de 0 à 3000 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`modalites-recrutement-terme`


   Procédés mis en œuvre pour recruter. 


