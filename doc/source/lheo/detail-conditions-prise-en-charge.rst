.. _detail-conditions-prise-en-charge:

Détails des conditions de prise en charge
+++++++++++++++++++++++++++++++++++++++++

.. code-block:: xml

  <detail-conditions-prise-en-charge>Tout stagiaire handicapé reconnu par la COTOREP, peut, sous certaines conditions, bénéficier d'une rémunération versée par la région.</detail-conditions-prise-en-charge>


Cet élément permet d'indiquer les conditions particulières de prise en charge: existence d'agréments (nombre, public, durée), etc.

Type
""""

Texte de 1 à 600 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`detail-conditions-terme`


   Cette donnée permettra d'indiquer les conditions particulières de prise en charge de l'action par le financeur, comme par exemple le conventionnement du conseil régional (nombre, public, durée). 


