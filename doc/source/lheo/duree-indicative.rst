.. _duree-indicative:

Informations sur le nombre d'heures
+++++++++++++++++++++++++++++++++++

.. code-block:: xml

  <duree-indicative>2 ans</duree-indicative>


Cet élément donne la durée indicative moyenne de la formation pour le stagiaire. Elle peut être exprimée en année, semestre, mois, jour ou heure.

Type
""""

Texte de 0 à 150 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

