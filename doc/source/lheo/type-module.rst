.. _type-module:

Type de module
++++++++++++++

.. code-block:: xml

  <type-module>1</type-module>


Cet élément contient un code permettant d'indiquer le type de module (obligatoire ou personnalisable).

Type
""""

Clé de la table :ref:`dict-type-module`


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`sous-module`.

