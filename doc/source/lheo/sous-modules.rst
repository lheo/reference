.. _sous-modules:

Sous modules
++++++++++++

.. code-block:: xml

  <sous-modules>
    <sous-module>...</sous-module> <!-- [1,N] -->
    <extras>...</extras> <!-- [0,N] -->
  </sous-modules>


Cet élément permet d'indiquer les sous-modules d'une action/module.

Éléments
""""""""

Séquence ordonnée des éléments suivants:

- ``<sous-module>`` :ref:`sous-module` [1, N]
- ``<extras>`` :ref:`extras` [0, N]



Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`formation`.

