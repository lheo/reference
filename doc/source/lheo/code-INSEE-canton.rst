.. _code-INSEE-canton:

Code canton INSEE
+++++++++++++++++

.. code-block:: xml

  <code-INSEE-canton>6916</code-INSEE-canton>


Cet élément contient un code INSEE de canton.

Type
""""

Texte de 4 à 5 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`adresse`.

