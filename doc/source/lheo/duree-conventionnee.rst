.. _duree-conventionnee:

Durée du conventionnement
+++++++++++++++++++++++++

.. code-block:: xml

  <duree-conventionnee>160</duree-conventionnee>


Cet élément contient le nombre d'heures conventionnées.

Type
""""

Entier [0,99999].


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`duree-conventionnee-terme`


   Durée pendant laquelle l'action bénéficie d'un financement. 


