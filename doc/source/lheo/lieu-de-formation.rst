.. _lieu-de-formation:

Lieu de la formation
++++++++++++++++++++

.. code-block:: xml

  <lieu-de-formation>
    <coordonnees>...</coordonnees> <!-- [1,1] -->
    <extras>...</extras> <!-- [0,N] -->
  </lieu-de-formation>


Cet élément permet de donner le lieu où se déroule la formation.

Éléments
""""""""

Séquence ordonnée des éléments suivants:

- ``<coordonnees>`` :ref:`coordonnees` [1, 1]
- ``<extras>`` :ref:`extras` [0, N]



Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`lieu-formation-terme`


   Adresse complète du lieu où se déroule la formation. 


