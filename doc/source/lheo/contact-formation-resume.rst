.. _contact-formation-resume:

Contact pour une offre de formation
+++++++++++++++++++++++++++++++++++

.. code-block:: xml

  <contact-formation-resume>
    <nom>...</nom> <!-- [0,1] -->
    <telfixe>...</telfixe> <!-- [0,1] -->
    <courriel>...</courriel> <!-- [0,1] -->
    <extras>...</extras> <!-- [0,N] -->
  </contact-formation-resume>




Éléments
""""""""

Séquence ordonnée des éléments suivants:

- ``<nom>`` :ref:`nom` [0, 1]
- ``<telfixe>`` :ref:`telfixe` [0, 1]
- ``<courriel>`` :ref:`courriel` [0, 1]
- ``<extras>`` :ref:`extras` [0, N]



Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

