.. _dict-AIS:

Objectif général de formation
+++++++++++++++++++++++++++++



.. list-table:: Table ``dict-AIS``
   :widths: 25 75
   :header-rows: 1

   * - Clé
     - Valeur
   * - ``1``
     - Code(s) obsolète(s)
   * - ``2``
     - Perfectionnement, élargissement des compétences
   * - ``3``
     - Perfectionnement, élargissement des compétences
   * - ``4``
     - Création d'entreprise
   * - ``5``
     - Remise à niveau, maîtrise des savoirs de base, initiation
   * - ``6``
     - Certification
   * - ``7``
     - Professionnalisation
   * - ``8``
     - Préparation à la qualification
   * - ``9``
     - (Re)mobilisation, aide à l'élaboration de projet professionnel


Utilisée dans  :ref:`objectif-general-formation`.

