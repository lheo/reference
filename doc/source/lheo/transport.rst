.. _transport:

Transport
+++++++++

.. code-block:: xml

  <transport>Bus 96 et métro</transport>


Cet élément précise l'organisation matérielle de la formation, en précisant les aspects pratiques liés aux transports. Si cet élément est vide, cela implique qu'il n'y a pas de transport prévu dans le cadre de la formation.

Type
""""

Texte de 1 à 250 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`restauration-terme`


   Informations relatives aux services annexes à une action de formation qui permettent de se déplacer puis d'accéder au lieu de formation, d'assurer l'accueil, la restauration et l'hébergement des apprenants. 


