.. _dict-niveaux:

Niveaux
+++++++



.. list-table:: Table ``dict-niveaux``
   :widths: 25 75
   :header-rows: 1

   * - Clé
     - Valeur
   * - ``0``
     - information non communiquée
   * - ``1``
     - sans niveau spécifique
   * - ``2``
     - niveau VI (illettrisme, analphabétisme)
   * - ``3``
     - niveau V bis (préqualification)
   * - ``4``
     - niveau V (CAP, BEP, CFPA du premier degré)
   * - ``5``
     - niveau IV (BP, BT, baccalauréat professionnel ou technologique)
   * - ``6``
     - niveau III (BTS, DUT)
   * - ``7``
     - niveau II (licence ou maîtrise universitaire)
   * - ``8``
     - niveau I (supérieur à la maîtrise)


Utilisée dans  :ref:`code-niveau-entree`,  :ref:`code-niveau-sortie`.

