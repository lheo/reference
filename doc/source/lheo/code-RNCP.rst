.. _code-RNCP:

Code RNCP
+++++++++

.. code-block:: xml

  <code-RNCP>653</code-RNCP>


Cet élément contient un code d'une certification dans le Répertoire National des Certifications Professionnelles.

Type
""""

Texte de 1 à 6 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`certification`.

