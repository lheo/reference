.. _resumes-organismes:

Résumés d'organisme
+++++++++++++++++++

.. code-block:: xml

  <resumes-organismes>
    <resume-organisme>...</resume-organisme> <!-- [0,N] -->
    <extras>...</extras> <!-- [0,N] -->
  </resumes-organismes>




Éléments
""""""""

Séquence ordonnée des éléments suivants:

- ``<resume-organisme>`` :ref:`resume-organisme` [0, N]
- ``<extras>`` :ref:`extras` [0, N]



Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

