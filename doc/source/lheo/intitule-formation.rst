.. _intitule-formation:

Intitulé de la formation
++++++++++++++++++++++++

.. code-block:: xml

  <intitule-formation>CAP photographe</intitule-formation>


Cet élément décrit l'intitulé de la formation. Si la formation a comme résultat l'obtention d'un diplôme, le contenu de cet élément devrait utiliser une dénomination conforme aux tables de l'Éducation Nationale. Si la formation a comme résultat un titre ou une certification, le contenu devrait utiliser une dénomination conforme au contenu du Répertoire National des Certifications Professionnelles (RNCP).

Type
""""

Texte de 1 à 255 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`formation`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`intitule-action-terme`


   Intitulé qui sert à caractériser et singulariser une action de formation. Il en indique le titre. 


