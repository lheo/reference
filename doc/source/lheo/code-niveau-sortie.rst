.. _code-niveau-sortie:

Niveau à la sortie de la formation
++++++++++++++++++++++++++++++++++

.. code-block:: xml

  <code-niveau-sortie>4</code-niveau-sortie>


Cet élément décrit le niveau de sortie de la formation.

Type
""""

Clé de la table :ref:`dict-niveaux`


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`formation`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`niveaux-terme`


   Les niveaux de formation correspondent à une position hiérarchique dans une nomenclature définie par l'éducation nationale d'un diplôme ou d'une formation. La personne peut soit être titulaire du diplôme ou titre correspondant aux entrées de la nomenclature, soit occuper un emploi exigeant normalement un niveau comparable. 

   La nomenclature des niveaux actuelle a été construite en 1969 en s'appuyant sur une grille établie en 1967 pour classer les formations conduisant aux diplômes de l'éducation nationale. 


