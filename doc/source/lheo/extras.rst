.. _extras:

Conteneur d'éléments d'extension (cercle 3)
+++++++++++++++++++++++++++++++++++++++++++

.. code-block:: xml

  <extras>...</extras>


Cet élément permet d'ajouter des informations qui ne sont pas normalisées dans l'un des éléments existant de LHÉO.

Éléments
""""""""

Série de 0 à N répétition des éléments suivants dans le désordre:

- ``<extras>`` :ref:`extras`
- ``<extra>`` :ref:`extra`



Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`domaine-formation`, :ref:`contact-formation`, :ref:`certification`, :ref:`lheo`, :ref:`offres`, :ref:`formation`, :ref:`action`, :ref:`organisme-formation-responsable`, :ref:`potentiel`, :ref:`organisme-formateur`, :ref:`url-formation`, :ref:`lieu-de-formation`, :ref:`session`, :ref:`adresse-inscription`, :ref:`adresse-information`, :ref:`date-information`, :ref:`url-action`, :ref:`SIRET-organisme-formation`, :ref:`coordonnees-organisme`, :ref:`contact-organisme`, :ref:`contact-formateur`, :ref:`organisme-financeur`, :ref:`SIRET-formateur`, :ref:`sous-modules`, :ref:`modules-prerequis`, :ref:`sous-module`, :ref:`coordonnees`, :ref:`adresse`, :ref:`geolocalisation`, :ref:`web`, :ref:`telfixe`, :ref:`portable`, :ref:`fax`, :ref:`periode`, :ref:`extras`.

