.. _identifiant-module:

Identifiant de module
+++++++++++++++++++++

.. code-block:: xml

  <identifiant-module>UV13</identifiant-module>


Cet identifiant permet de donner un identifiant à une action (qui dès lors devient un module). Cet identifiant n'a pas de forme normalisée, il peut donc être créé librement en fonction des besoins de chaque structure.

Type
""""

Texte de 1 à 3000 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`formation`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`module-formation-terme`


   Unité de formation autonome qui constitue un tout cohérent en soi, et fait partie d'un cursus de formation. Il est construit à partir des éléments suivants : objectifs, objectifs pédagogiques généraux, contenu, durée, pré-requis. Un module correspond à un ensemble de séquences de formation (NFX 50-750 voir  :ref:`bibliographie-terme` ). Un module de formation vise à faire acquérir des compétences, c'est-à-dire une articulation de savoirs contextualisés dans une activité professionnelle. 


