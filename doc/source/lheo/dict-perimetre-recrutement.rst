.. _dict-perimetre-recrutement:

Périmètre de recrutement
++++++++++++++++++++++++



.. list-table:: Table ``dict-perimetre-recrutement``
   :widths: 25 75
   :header-rows: 1

   * - Clé
     - Valeur
   * - ``0``
     - Autres
   * - ``1``
     - Commune
   * - ``2``
     - Département
   * - ``3``
     - Région
   * - ``4``
     - Interrégion
   * - ``5``
     - Pays
   * - ``6``
     - International


Utilisée dans  :ref:`code-perimetre-recrutement`.

