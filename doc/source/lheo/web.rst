.. _web:

Web
+++

.. code-block:: xml

  <web>
    <urlweb>...</urlweb> <!-- [1,3] -->
    <extras>...</extras> <!-- [0,N] -->
  </web>


Cet élément contient une liste d'adresses de sites web.

Éléments
""""""""

Séquence ordonnée des éléments suivants:

- ``<urlweb>`` :ref:`urlweb` [1, 3]
- ``<extras>`` :ref:`extras` [0, N]



Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`coordonnees`.

