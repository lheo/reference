.. _modalites-inscription:

Modalités de l'inscription
++++++++++++++++++++++++++

.. code-block:: xml

  <modalites-inscription>Ce programme ne peut être dispensé sans l’abonnement au Concours médical, contenant les articles de référence sur les thèmes du programme.</modalites-inscription>


Cet élément permet de préciser une information sur les modalités de l'inscription.

Type
""""

Texte de 1 à 255 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`session`.

