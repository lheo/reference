.. _nom-organisme:

Nom de l'organisme de formation responsable
+++++++++++++++++++++++++++++++++++++++++++

.. code-block:: xml

  <nom-organisme>Centre de formation d'apprentis académique (CFA)</nom-organisme>


Cet élément décrit le nom usuel de l'organisme responsable de la formation.

Type
""""

Texte de 1 à 250 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`organisme-formation-responsable`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`nom-organisme-terme`


   Il s'agit de la dénomination usuelle de l'organisme responsable de l'offre de formation. Ce peut-être notamment un sigle ou un nom pour les sociétés en nom propre. 


