.. _code-financeur:

Code financeur
++++++++++++++

.. code-block:: xml

  <code-financeur>2</code-financeur>


Cet élément contient le code d'un organisme financeur de la table correspondante.

Type
""""

Clé de la table :ref:`dict-financeurs`


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`organisme-financeur`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`organisme-financeur-terme`


   Organisme qui finance l'action de formation. 


