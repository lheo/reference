.. _offres:

Offres de formation
+++++++++++++++++++

.. code-block:: xml

  <offres>
    <formation>...</formation> <!-- [1,N] -->
    <extras>...</extras> <!-- [0,N] -->
  </offres>


Élément contenant une liste de formations.

Éléments
""""""""

Séquence ordonnée des éléments suivants:

- ``<formation>`` :ref:`formation` [1, N]
- ``<extras>`` :ref:`extras` [0, N]



Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`lheo`.

