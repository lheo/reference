.. _prix-horaire-TTC:

Prix horaire TTC
++++++++++++++++

.. code-block:: xml

  <prix-horaire-TTC>45</prix-horaire-TTC>


Cet élément permet d'indiquer en euros, toutes taxes comprises, le prix de vente de l'heure de formation.

Type
""""

Texte de 1 à 6 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`prix-horaire-TTC-terme`


   Prix de vente toutes taxes comprises de l'heure de formation qui induit l'ensemble des frais entraînés par la réalisation de l'action de formation. 


