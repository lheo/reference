.. _dict-boolean:

Valeurs booléennes
++++++++++++++++++



.. list-table:: Table ``dict-boolean``
   :widths: 25 75
   :header-rows: 1

   * - Clé
     - Valeur
   * - ``0``
     - non
   * - ``1``
     - oui


Utilisée dans  :ref:`certifiante`,  :ref:`niveau-entree-obligatoire`,  :ref:`prise-en-charge-frais-possible`,  :ref:`conventionnement`.

