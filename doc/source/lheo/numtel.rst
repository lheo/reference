.. _numtel:

Numéro de téléphone
+++++++++++++++++++

.. code-block:: xml

  <numtel>04 64 69 48 36</numtel>


Cet élément contient les chiffres d'un numéro de téléphone, avec éventuellement un '+', des parenthèses et des blancs.

Type
""""

Texte de 1 à 25 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`telfixe`, :ref:`portable`, :ref:`fax`.

