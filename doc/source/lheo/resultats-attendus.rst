.. _resultats-attendus:

Résultats attendus de la formation
++++++++++++++++++++++++++++++++++

.. code-block:: xml

  <resultats-attendus>CAP Photographe</resultats-attendus>


Cet élément décrit les résultats attendus de la formation (titre, diplôme, certificat, attestation, ...) et précise les modalités de reconnaissance ou de validation. De la même manière que pour l'élément 'intitule-formation', les diplômes, titres ou certifications devraient utiliser des dénominations conformes aux tables de l'Éducation Nationale ou au Répertoire National des Certifications Professionnelles (RNCP).

Type
""""

Texte de 1 à 3000 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`formation`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`resultats-formation-terme`


   Ils matérialisent le passage en formation et en précisent les modalités de reconnaissance ou de validation. 


