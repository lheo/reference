.. _region:

Région
++++++

.. code-block:: xml

  <region>82</region>


Cet élément contient un numéro de région.

Type
""""

Texte de 2 à 2 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`adresse`.

