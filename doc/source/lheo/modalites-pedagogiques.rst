.. _modalites-pedagogiques:

Modalités pédagogiques
++++++++++++++++++++++

.. code-block:: xml

  <modalites-pedagogiques>Individualisation de la formation</modalites-pedagogiques>


Cet élément précise les modalités pédagogiques, dont l'individualisation de la formation, les possibilités d'autoformation avec une éventuelle durée, etc.

Type
""""

Texte de 0 à 200 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`modalites-pedagogiques-terme`


   Pédagogies mises en œuvres dans le déroulement de la formation, comme des études de cas, des mises en situation, l'individualisation de la formation, les possibilités d'autoformation, etc. 


