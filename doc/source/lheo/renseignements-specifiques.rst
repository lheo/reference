.. _renseignements-specifiques:

Renseignements spécifiques sur l'organisme
++++++++++++++++++++++++++++++++++++++++++

.. code-block:: xml

  <renseignements-specifiques>Démarche qualité ISO9001</renseignements-specifiques>


Cet élément permet de donner des renseignements spécifiques sur l'organisme responsable de l'offre, comme par exemple une démarche qualité, un label, la capacité de positionnement à l'entrée en formation (pédagogique ou réglementaire), etc.

Type
""""

Texte de 0 à 3000 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`organisme-formation-responsable`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`renseignements-specifiques-terme`


   Ils permettent d'indiquer des informations spécifiques (démarche qualité, labels, capacités de positionnement, ...) sur l'organisme responsable juridiquement de l'action de formation, et notamment les garanties proposées pour réaliser l'action. 


