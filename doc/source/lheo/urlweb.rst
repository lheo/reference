.. _urlweb:

URL
+++

.. code-block:: xml

  <urlweb>http://www.cfa.org</urlweb>


Cet élément contient une adresse de site web.

Type
""""

Texte de 3 à 400 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`url-formation`, :ref:`url-action`, :ref:`web`.

