.. _parcours-de-formation:

Type de parcours de formation
+++++++++++++++++++++++++++++

.. code-block:: xml

  <parcours-de-formation>2</parcours-de-formation>


Cet élément permet de qualifier le type de parcours de formation.

Type
""""

Clé de la table :ref:`dict-type-parcours`


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`formation`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`parcours-formation-terme`


   Itinéraire organisé d'acquisition de connaissances. Il comporte des évaluations et peut déboucher sur une validation (AFNOR). 


