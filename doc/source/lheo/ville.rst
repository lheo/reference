.. _ville:

Ville
+++++

.. code-block:: xml

  <ville>Lyon</ville>


Cet élément contient un nom de ville.

Type
""""

Texte de 1 à 50 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`adresse`.

