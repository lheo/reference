.. _dict-etat-recrutement:

Etat du recrutement
+++++++++++++++++++



.. list-table:: Table ``dict-etat-recrutement``
   :widths: 25 75
   :header-rows: 1

   * - Clé
     - Valeur
   * - ``1``
     - ouvert
   * - ``2``
     - fermé
   * - ``3``
     - suspendu


Utilisée dans  :ref:`etat-recrutement`.

