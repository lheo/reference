.. _code-INSEE-commune:

Code commune INSEE
++++++++++++++++++

.. code-block:: xml

  <code-INSEE-commune>69123</code-INSEE-commune>


Cet élément contient un code INSEE de commune.

Type
""""

Texte de 5 à 5 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`adresse`.

