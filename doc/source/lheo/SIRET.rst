.. _SIRET:

SIRET
+++++

.. code-block:: xml

  <SIRET>63445123124916</SIRET>


Cet élément contient un numéro SIRET.

Type
""""

Texte de 14 à 14 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`SIRET-organisme-formation`, :ref:`SIRET-formateur`.

