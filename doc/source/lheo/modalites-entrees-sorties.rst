.. _modalites-entrees-sorties:

Modalités d'entrées/sorties
+++++++++++++++++++++++++++

.. code-block:: xml

  <modalites-entrees-sorties>0</modalites-entrees-sorties>


Cet élément contient un code permettant de dire si la formation est à dates fixes ou avec des entrées/sorties permanentes.

Type
""""

Clé de la table :ref:`dict-modalites-es`


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`modalites-entrees-sorties-terme`


   Caractéristiques temporelles de la session de formation. 


