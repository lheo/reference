.. _code-FORMACODE:

Code FORMACODE
++++++++++++++

.. code-block:: xml

  <code-FORMACODE>46278</code-FORMACODE>


Cet élément contient un code du FORMACODE. Il est obligatoire de préciser grâce à l'attribut 'ref' la version du FORMACODE utilisée (par exemple 'V10' pour la version 10 du FORMACODE).

Type
""""

Texte de 5 à 5 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (obligatoire) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`domaine-formation`, :ref:`potentiel`.

