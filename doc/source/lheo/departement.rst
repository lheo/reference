.. _departement:

Département
+++++++++++

.. code-block:: xml

  <departement>69</departement>


Cet élément contient un numéro de département.

Type
""""

Texte de 2 à 3 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`adresse`.

