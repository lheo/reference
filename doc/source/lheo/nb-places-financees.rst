.. _nb-places-financees:

Nombre de places financées
++++++++++++++++++++++++++

.. code-block:: xml

  <nb-places-financees>...</nb-places-financees>




Type
""""

Entier [0,99999].


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`organisme-financeur`.

