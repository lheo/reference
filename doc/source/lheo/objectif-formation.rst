.. _objectif-formation:

Objectif de formation
+++++++++++++++++++++

.. code-block:: xml

  <objectif-formation>Effectuer des travaux de prises de vue (photos d'identité, travaux de reportage, prises de vue d'un objet technique en 3 dimensions, prises de vue publicitaires type catalogue...) et les travaux de laboratoire: tirage en noir et blanc ou en couleur.</objectif-formation>


Cet élément décrit l'objectif de la formation. Il décrit la ou les compétences à acquérir, à améliorer ou à entretenir.

Type
""""

Texte de 1 à 3000 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`formation`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`objectif-formation-terme`


   Compétence(s) à acquérir, à améliorer ou à entretenir exprimée(s) initialement par les commanditaires et/ou les apprenants. L'objectif de formation est l'élément fondamental des cahiers des charges. Il sert à évaluer les effets de la formation (NFX 50-750 voir  :ref:`bibliographie-terme` ). Il doit exprimer clairement les compétences visées (savoirs, savoir-faire, comportements). Il doit être formalisé. 


