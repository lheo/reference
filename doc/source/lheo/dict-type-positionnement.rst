.. _dict-type-positionnement:

Type de positionnement
++++++++++++++++++++++



.. list-table:: Table ``dict-type-positionnement``
   :widths: 25 75
   :header-rows: 1

   * - Clé
     - Valeur
   * - ``1``
     - réglementaire
   * - ``2``
     - pédagogique


Utilisée dans  :ref:`positionnement`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`positionnement-terme`


   Processus permettant d'évaluer à l'entrée en formation les acquis et les besoins d'un apprenant au regard de l'objectif de la formation. Il permet d'élaborer un parcours personnalisé de formation, réglementaire ou pédagogique. 


