.. _extra:

Élément d'extension (cercle 3)
++++++++++++++++++++++++++++++

.. code-block:: xml

  <extra>SDFKKKFERRLKLKRLTML</extra>




Type
""""

N'importe quel élément XML ou texte.


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`extras`.

