.. _ligne:

Ligne
+++++

.. code-block:: xml

  <ligne>23 rue Victor Hugo</ligne>


Cet élément contient une ligne de texte pour des coordonnées ou une adresse.

Type
""""

Texte de 1 à 50 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`coordonnees`, :ref:`adresse`.

