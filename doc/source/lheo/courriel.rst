.. _courriel:

Courriel
++++++++

.. code-block:: xml

  <courriel>jean.arcueil@cfa.org</courriel>


Cet élément contient une adresse de courrier électronique.

Type
""""

Texte de 3 à 160 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`coordonnees`.

