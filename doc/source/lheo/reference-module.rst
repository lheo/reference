.. _reference-module:

Référence de module
+++++++++++++++++++

.. code-block:: xml

  <reference-module>UV14</reference-module>


Cet identifiant permet de référencer un module existant.

Type
""""

Texte de 1 à 3000 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`modules-prerequis`, :ref:`sous-module`.

