.. _conditions-specifiques:

Conditions spécifiques et prérequis
+++++++++++++++++++++++++++++++++++

.. code-block:: xml

  <conditions-specifiques>Savoir se servir d'un appareil photo</conditions-specifiques>


Cet élément indique les conditions spécifiques d'accès à la formation, les aptitudes requises, une tranche d'âge, etc. Il indique les prérequis pour avoir accès à la formation. Si il n'y a pas de conditions spécifiques, il convient de préciser 'aucune' dans cet élément.

Type
""""

Texte de 1 à 3000 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`conditions-specifiques-terme`


   Cet élément indique les conditions spécifiques d'accès à la formation, les aptitudes requises, une tranche d'âge, etc. 


