.. _dict-type-module:

Type de module
++++++++++++++



.. list-table:: Table ``dict-type-module``
   :widths: 25 75
   :header-rows: 1

   * - Clé
     - Valeur
   * - ``0``
     - information inconnue
   * - ``1``
     - obligatoire
   * - ``2``
     - personnalisable


Utilisée dans  :ref:`type-module`.

