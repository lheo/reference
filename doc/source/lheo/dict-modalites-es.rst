.. _dict-modalites-es:

Modalités d'entrées/sorties
+++++++++++++++++++++++++++



.. list-table:: Table ``dict-modalites-es``
   :widths: 25 75
   :header-rows: 1

   * - Clé
     - Valeur
   * - ``0``
     - entrées/sorties à dates fixes
   * - ``1``
     - entrées/sorties permanentes


Utilisée dans  :ref:`modalites-entrees-sorties`.

