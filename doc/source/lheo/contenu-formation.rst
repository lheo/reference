.. _contenu-formation:

Contenu de la formation
+++++++++++++++++++++++

.. code-block:: xml

  <contenu-formation>Apprendre les techniques de prises de vues. Choisir et fabriquer une mise en scène. Maîtriser les éclairages et les couleurs. Effectuer des travaux de développements et d'agrandissements.</contenu-formation>


Cet élément décrit le contenu de la formation et fournit une description détaillée des différents sujets traités dans la formation.

Type
""""

Texte de 1 à 3000 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`formation`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`contenu-formation-terme`


   Description détaillée des différents sujets traités dans la formation, en fonction d'objectifs pédagogiques et de formation définis explicitement (NFX 50-750 voir  :ref:`bibliographie-terme` ). 


