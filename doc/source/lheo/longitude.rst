.. _longitude:

Longitude
+++++++++

.. code-block:: xml

  <longitude>-2.024975</longitude>




Type
""""

Texte de 0 à 30 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`geolocalisation`.

