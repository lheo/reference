.. _info-public-vise:

Informations sur le public visé
+++++++++++++++++++++++++++++++

.. code-block:: xml

  <info-public-vise>La formation s'adresse, d'une part aux titulaires des baccalauréats généraux S, ES et L option mathématiques, d'autre part aux titulaires des baccalauréats technologiques STI2D et STMG.</info-public-vise>


Cette donnée permet à l'organisme d'ajouter des caractéristiques à l'article sélectionné dans la table "public visé", dès lors qu'elles ne donnent pas lieu à discrimination (portée juridique).

Type
""""

Texte de 1 à 250 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

