.. _raison-sociale:

Raison sociale de l'organisme
+++++++++++++++++++++++++++++

.. code-block:: xml

  <raison-sociale>Centre de formation d'apprentis académique (CFA)</raison-sociale>


Cet élément permet de préciser la raison sociale de l'organisme responsable de l'offre.

Type
""""

Texte de 1 à 250 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`organisme-formation-responsable`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`raison-sociale-organisme-terme`


   Il s'agit de la raison sociale de l'organisme responsable de l'offre. Cette dénomination à valeur juridique peut être identique au nom usuel ou sigle de l'organisme. 


