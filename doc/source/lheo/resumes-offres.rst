.. _resumes-offres:

Résumés d'offres de formation
+++++++++++++++++++++++++++++

.. code-block:: xml

  <resumes-offres>
    <resume-offre>...</resume-offre> <!-- [0,N] -->
    <extras>...</extras> <!-- [0,N] -->
  </resumes-offres>




Éléments
""""""""

Séquence ordonnée des éléments suivants:

- ``<resume-offre>`` :ref:`resume-offre` [0, N]
- ``<extras>`` :ref:`extras` [0, N]



Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

