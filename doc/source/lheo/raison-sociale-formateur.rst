.. _raison-sociale-formateur:

Raison sociale de l'organisme formateur
+++++++++++++++++++++++++++++++++++++++

.. code-block:: xml

  <raison-sociale-formateur>CFA académique</raison-sociale-formateur>


Cet élément permet de préciser la raison sociale de l'organisme formateur, c'est-à-dire de l'organisme qui assure la formation.

Type
""""

Texte de 0 à 250 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`organisme-formateur`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`raison-sociale-formateur-terme`


   Il s'agit de la raison sociale de l'organisme qui assure la formation. Cette dénomination à valeur juridique peut être identique au nom usuel ou sigle de l'organisme. 


