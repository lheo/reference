.. _numero-activite:

Numéro de déclaration d'activité
++++++++++++++++++++++++++++++++

.. code-block:: xml

  <numero-activite>64194292762</numero-activite>


Cet élément permet de préciser le numéro de déclaration d'activité de l'organisme responsable de l'offre.

Type
""""

Texte de 11 à 11 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`organisme-formation-responsable`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`numero-activite-terme`


   Il s'agit de l'immatriculation attribuée par les services de contrôle de la formation professionnelle aux organismes exerçant dans ce secteur. 


