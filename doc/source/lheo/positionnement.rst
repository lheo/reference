.. _positionnement:

Type de positionnement
++++++++++++++++++++++

.. code-block:: xml

  <positionnement>1</positionnement>


Cet élément contient un code permettant d'indiquer le type de positionnement.

Type
""""

Clé de la table :ref:`dict-type-positionnement`


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`formation`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`positionnement-terme`


   Processus permettant d'évaluer à l'entrée en formation les acquis et les besoins d'un apprenant au regard de l'objectif de la formation. Il permet d'élaborer un parcours personnalisé de formation, réglementaire ou pédagogique. 


