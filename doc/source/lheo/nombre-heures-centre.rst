.. _nombre-heures-centre:

Nombre d'heures en centre
+++++++++++++++++++++++++

.. code-block:: xml

  <nombre-heures-centre>430</nombre-heures-centre>


Cet élément permet d'indiquer en heures la durée de la formation en centre.

Type
""""

Entier [0,99999].


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`nombre-heures-total-centre-terme`


   Cette donnée correspond au nombre d’heures maximum en centre de formation indiqué par l’organisme responsable de l’action de formation. 


