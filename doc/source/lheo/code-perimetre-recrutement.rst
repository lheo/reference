.. _code-perimetre-recrutement:

Périmètre de recrutement
++++++++++++++++++++++++

.. code-block:: xml

  <code-perimetre-recrutement>6</code-perimetre-recrutement>


Cet élément indique un code issu de la table correspondante pour le périmètre de recrutement.

Type
""""

Clé de la table :ref:`dict-perimetre-recrutement`


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`perimetre-recrutement-terme`


   Il correspond, pour une action de formation donnée, à l'espace potentiel de recrutement des apprenants. 


