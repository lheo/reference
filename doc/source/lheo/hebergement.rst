.. _hebergement:

Hébergement
+++++++++++

.. code-block:: xml

  <hebergement>résidence étudiante</hebergement>


Cet élément précise les possibilités d'hébergement. Si cet élément est vide, cela implique qu'il n'y pas de possibilités d'hébergement.

Type
""""

Texte de 1 à 250 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`restauration-terme`


   Informations relatives aux services annexes à une action de formation qui permettent de se déplacer puis d'accéder au lieu de formation, d'assurer l'accueil, la restauration et l'hébergement des apprenants. 


