.. _modalites-alternance:

Modalités de l'alternance
+++++++++++++++++++++++++

.. code-block:: xml

  <modalites-alternance>stage en entreprise de juin à août</modalites-alternance>


Cet élément permet de préciser les dates et la durée des périodes en centre ou en entreprise.

Type
""""

Texte de 1 à 3000 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`modalites-alternance-terme`


   Préciser l'organisation de l'alternance : dates, durées des périodes en centre ou en entreprise. 


