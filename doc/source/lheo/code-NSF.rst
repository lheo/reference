.. _code-NSF:

Code NSF
++++++++

.. code-block:: xml

  <code-NSF>323</code-NSF>


Cet élément contient un code de la NSF (3 postes).

Type
""""

Texte de 3 à 3 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`domaine-formation`.

