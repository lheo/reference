.. _niveau-entree-obligatoire:

Niveau à l'entrée en formation obligatoire
++++++++++++++++++++++++++++++++++++++++++

.. code-block:: xml

  <niveau-entree-obligatoire>1</niveau-entree-obligatoire>


Cet élément précise si le niveau à l'entrée en formation décrit dans l'élément 'code-niveau-entree' est obligatoire ou uniquement indicatif.

Type
""""

Clé de la table :ref:`dict-boolean`


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`niveau-entree-terme`


   Il correspond au niveau de titre ou de diplôme acquis par le demandeur de formation au moment de son entrée dans l'action. 


