.. _nombre-heures-total:

Total du nombre d'heures
++++++++++++++++++++++++

.. code-block:: xml

  <nombre-heures-total>800</nombre-heures-total>


Cet élément permet d'indiquer en heures la durée totale maximum de la formation, y compris période de formation à distance et en entreprise.

Type
""""

Entier [0,99999].


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

