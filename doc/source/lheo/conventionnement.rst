.. _conventionnement:

Conventionnement
++++++++++++++++

.. code-block:: xml

  <conventionnement>1</conventionnement>


Cet élément indique si l'action de formation est conventionnée ou non.

Type
""""

Clé de la table :ref:`dict-boolean`


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`conventionnement-terme`


   Cette information indique si une action de formation donnée bénéficie d'une contribution financière du financeur public. 


