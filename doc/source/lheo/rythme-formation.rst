.. _rythme-formation:

Rythme de la formation
++++++++++++++++++++++

.. code-block:: xml

  <rythme-formation>temps plein</rythme-formation>


Cet élément décrit le rythme de la formation: temps plein, temps partiel, etc.

Type
""""

Texte de 1 à 3000 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`action`.

.. admonition:: Dans le glossaire LHÉO

   :ref:`rythme-formation-terme`


   Ce sont la ou les possibilité(s) de suivi offerte(s) aux apprenants pour une action de formation proposée. Ce peut-être un suivi à temps partiel, à temps plein ou encore un mélange de ces 2 modalités (temps plein et temps partiel) sur la même action de formation. 


