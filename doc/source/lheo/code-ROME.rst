.. _code-ROME:

Code ROME
+++++++++

.. code-block:: xml

  <code-ROME>A1201</code-ROME>


Cet élément contient un code du ROME. Il convient de préciser grâce à l'attribut 'ref' la version du ROME utilisée. Si cet attribut n'est pas présent, la 'V3' est considérée comme la référence.

Type
""""

Texte de 5 à 5 caractère(s).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`domaine-formation`.

