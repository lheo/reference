.. _debut:

Début
+++++

.. code-block:: xml

  <debut>20050915</debut>


Cet élément indique une date de début. Si cette date est inconnue, indiquer 00000000. Pour indiquer un mois, utiliser le format AAAAMM00, pour indiquer une année, utiliser le format AAAA0000.

Type
""""

Date (format ISO ``AAAAMMJJ``). Date inconnue: ``00000000`` (min) ou ``99999999`` (max).


Attributs
"""""""""

- ``numero`` (optionnel) texte (0,N)
- ``info`` (optionnel) texte (0,N)
- ``ref`` (optionnel) texte (0,N)
- ``id`` (optionnel) ID (0,N)
- ``idref`` (optionnel) IDREF (0,N)
- ``tag`` (optionnel) texte (0,N)

Utilisé dans :ref:`periode`.

