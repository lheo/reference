Éléments optionnels (deuxième cercle)
-------------------------------------

.. toctree::
   :maxdepth: 3
   :hidden:
   :caption: Sommaire

   lheo/duree-indicative
   lheo/objectif-general-formation
   lheo/certification
   lheo/potentiel
   lheo/code-niveau-sortie
   lheo/url-formation
   lheo/code-modalite-pedagogique
   lheo/frais-restants
   lheo/langue-formation
   lheo/adresse-information
   lheo/date-information
   lheo/url-action
   lheo/contact-formateur
   lheo/renseignements-specifiques
   lheo/organisme-financeur
   lheo/nb-places-financees
   lheo/modalites-recrutement
   lheo/modalites-pedagogiques
   lheo/SIRET-formateur
   lheo/raison-sociale-formateur
   lheo/code-perimetre-recrutement
   lheo/infos-perimetre-recrutement
   lheo/prix-horaire-TTC
   lheo/prix-total-TTC
   lheo/nombre-heures-centre
   lheo/nombre-heures-entreprise
   lheo/nombre-heures-total
   lheo/detail-conditions-prise-en-charge
   lheo/conventionnement
   lheo/duree-conventionnee
   lheo/restauration
   lheo/hebergement
   lheo/transport
   lheo/acces-handicapes
   lheo/positionnement
   lheo/etat-recrutement
   lheo/identifiant-module
   lheo/sous-modules
   lheo/modules-prerequis
   lheo/info-public-vise
   lheo/modalites-inscription
   lheo/periode-inscription

