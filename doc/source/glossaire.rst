.. _glossaire:

Glossaire LHÉO (mars 2016)
++++++++++++++++++++++++++

Ce glossaire constitue une aide à l'utilisation du Langage Harmonisé d'Échange d'informations sur l'Offre de formation (LHÉO), en définissant les termes du champ de la formation utilisés dans le langage. 


Certaines d’entre elles relèvent des normes AFNOR sur la Terminologie de la Formation Professionnelle (NFX 50-750, FDX 50-751, NFX 50-760 voir  :ref:`bibliographie-terme` ). 


.. _acquis-terme:

Acquis
^^^^^^

Ensemble des savoirs et savoir-faire dont une personne manifeste la maîtrise dans une activité professionnelle, sociale ou de formation. 




.. _action-terme:

Action de formation
^^^^^^^^^^^^^^^^^^^

S'entend de tout ce qui contribue à la conception, à l’organisation, et à la mise en œuvre d’une formation. 



Voir aussi  :ref:`module-formation-terme` et  :ref:`session-formation-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`action`.



.. _agrement-terme:

Agrément au titre de la rémunération
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L'  `article L6341-4 du code du travail  <https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006904370&cidTexte=LEGITEXT000006072050>`_ prévoit l'agrément des stages de formation par l'État (le préfet) ou les Régions (président du conseil régional) pour assurer la rémunération de stagiaires demandeurs d'emploi non indemnisés et remplissant les conditions d'accès à la rémunération prévue par le code du travail. 




.. _action-conventionnee-terme:

Action de formation conventionnée
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Une action de formation au sens des  `articles L6313-1 à L6313-11 du code du travail  <https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006178201&cidTexte=LEGITEXT000006072050>`_ est dite conventionnée lorsqu'elle fait l'objet d'une convention de formation professionnelle conforme aux dispositions des articles  `L6353-1  <https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006904411>`_ ,  `L6353-2  <https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000021343606&cidTexte=LEGITEXT000006072050>`_ et  `R6353-1  <https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000018522260&cidTexte=LEGITEXT000006072050>`_ , et  `L6122-1  <https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006903985&cidTexte=LEGITEXT000006072050>`_ et  `L6122-2  <https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031843532&cidTexte=LEGITEXT000006072050>`_ (Etat, Régions et autres collectivités). Ces conventions fixent les contenus, les modalités de réalisation ainsi que les objectifs à atteindre en contrepartie d'une contribution financière à l'action de formation : directe s'il s'agit d'un achat dans le respect des règles du code des marchés publics en vigueur, indirecte s'il s'agit de subventions de fonctionnement consenties à l'organisme pour mettre en œuvre cette action. Il est enfin rappelé que les personnes physiques qui entreprennent des formations à leurs frais doivent conclure un contrat de formation professionnelle avec l'organisme dispensateur, conforme - à peine de nullité - aux exigences des  `articles L6353-3 à L6353-7 du code du travail  <https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006189927&cidTexte=LEGITEXT000006072050>`_ . 




.. _age-terme:

Âge
^^^

Il s'agit des conditions administratives d'âge requises pour pouvoir accéder à une action de formation donnée. 




.. _aptitude-terme:

Aptitude
^^^^^^^^

Capacité supposée à exercer une activité (tâche à accomplir, emploi à occuper, connaissance à acquérir). La reconnaissance juridique de l'aptitude (certificat d'aptitude, liste d'aptitude...) ouvre l'accès à certains droits (emploi, formation...) (FDX 50-751 voir  :ref:`bibliographie-terme` ). 




.. _attestation-acquis-terme:

Attestation des acquis
^^^^^^^^^^^^^^^^^^^^^^

Document délivré au stagiaire par les dispensateurs de formation, reconnaissant l'acquisition de capacités à l'issue de la formation (FDX 50-751 voir  :ref:`bibliographie-terme` ). 




.. _attestation-presence-terme:

Attestation de présence
^^^^^^^^^^^^^^^^^^^^^^^

Document écrit, à usage administratif, remis au commanditaire. Il certifie la présence de l'apprenant (ou des apprenants) à une formation (NFX 50-750 voir  :ref:`bibliographie-terme` ). 




.. _attestation-stage-terme:

Attestation de fin de formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Document écrit, remis à l'apprenant, qui certifie sa participation à une formation et précise les objectifs, la nature et la durée de la formation et le cas échéant les résultats de l’évaluation des acquis (  `article L6353-1 alinéa 2 du code du travail  <https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006904411>`_ ) 




.. _bilan-competences-terme:

Bilan de compétences
^^^^^^^^^^^^^^^^^^^^

Action permettant à un travailleur d'analyser ses compétences professionnelles ainsi que ses aptitudes et ses motivations afin de définir un projet professionnel et, le cas échéant, un projet de formation (articles  `L6313-1  <https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000021341894&cidTexte=LEGITEXT000006072050>`_ et  `L6313-10  <https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006904140&cidTexte=LEGITEXT000006072050>`_ du code du travail). 



Voir aussi  :ref:`action-terme` . 




.. _capacite-terme:

Capacité
^^^^^^^^

Ensemble de dispositions et d'acquis, constatés chez un apprenant, généralement formulés par l'expression : être capable de... (NFX 50-750 voir  :ref:`bibliographie-terme` ) 




.. _certificat-formation-terme:

Certificat de formation
^^^^^^^^^^^^^^^^^^^^^^^

Document écrit, délivré par le dispensateur de formation ou une autorité de référence, reconnaissant au titulaire un niveau de capacité vérifié par un contrôle. 




.. _certification-terme:

Certification
^^^^^^^^^^^^^

Procédure définissant les conditions de délivrance d'un certificat qui valide les acquis d'une formation. 




.. _certification-acquis-terme:

Certification des acquis de la formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Procédure définissant les conditions de délivrance d'un certificat qui valide les acquis d'une formation. 




.. _certification-pro-terme:

Certification professionnelle
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Procédure visant à organiser les modalités de reconnaissance des qualifications et des acquis professionnels nécessaires à l'exercice d'une profession. 




.. _competence-pro-terme:

Compétence professionnelle
^^^^^^^^^^^^^^^^^^^^^^^^^^

Mise en oeuvre, en situation professionnelle, de capacités qui permettent d'exercer convenablement une fonction ou une activité (NFX 50-750 voir  :ref:`bibliographie-terme` ). 




.. _conditions-pedagogiques-terme:

Conditions pédagogiques
^^^^^^^^^^^^^^^^^^^^^^^

Conditions pédagogiques du déroulement de la formation. 



Voir  :ref:`organisation-formation-terme` ,  :ref:`methode-pedagogique-terme` ,  :ref:`moyen-pedagogique-terme` ,  :ref:`support-pedagogique-terme` . 




.. _conditions-specifiques-terme:

Conditions spécifiques
^^^^^^^^^^^^^^^^^^^^^^

Cet élément indique les conditions spécifiques d'accès à la formation, les aptitudes requises, une tranche d'âge, etc. 



Voir aussi  :ref:`aptitude-terme` ,  :ref:`age-terme` ,  :ref:`prerequis-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`conditions-specifiques`.



.. _contact-offre-terme:

Contact sur l'offre de formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il s'agit du nom et du numéro de téléphone, de l'adresse postale ou électronique de la personne habilitée à donner des renseignements sur l'action de formation. Cette personne peut être également le contact de l'organisme responsable de l'offre. 


Éléménts de LHÉO reliés à cette notion : :ref:`contact-formation`.



.. _contact-organisme-terme:

Contact de l'organisme
^^^^^^^^^^^^^^^^^^^^^^

Il s'agit du nom et du numéro de téléphone, de l'adresse postale ou électronique de la personne habilitée à donner des renseignements sur l'action de formation ou l'ensemble des formations proposées par l'organisme responsable de l'offre, voire sur l'organisme lui-même. 


Éléménts de LHÉO reliés à cette notion : :ref:`contact-organisme`.



.. _contenu-formation-terme:

Contenu de formation
^^^^^^^^^^^^^^^^^^^^

Description détaillée des différents sujets traités dans la formation, en fonction d'objectifs pédagogiques et de formation définis explicitement (NFX 50-750 voir  :ref:`bibliographie-terme` ). 


Éléménts de LHÉO reliés à cette notion : :ref:`contenu-formation`.



.. _controle-connaissances-terme:

Contrôle de connaissances
^^^^^^^^^^^^^^^^^^^^^^^^^

Vérification de l'acquisition de savoirs. Ce contrôle peut être oral, écrit ou pratique. 



Voir aussi  :ref:`certificat-formation-terme` . 




.. _conventionnement-terme:

Conventionnement
^^^^^^^^^^^^^^^^

Cette information indique si une action de formation donnée bénéficie d'une contribution financière du financeur public. 



Voir aussi  :ref:`action-conventionnee-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`conventionnement`.



.. _coordonnees-organisme-terme:

Coordonnées organisme
^^^^^^^^^^^^^^^^^^^^^

Elles indiquent les coordonnées de l'organisme juridiquement responsable de l'action de formation. 


Éléménts de LHÉO reliés à cette notion : :ref:`coordonnees-organisme`.



.. _date-limite-inscription-terme:

Dates prévues de début et de fin de période d’inscription
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Ces dates déterminent le début et la fin d’une période d’inscription pour une action de formation à réaliser. 




.. _dates-debut-fin-stage-terme:

Dates prévues de début et de fin de la session ou d'entrées sorties permanentes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Elles indiquent de manière prévisionnelle les périodes de démarrage et d'achèvement de la phase de réalisation de l'action de formation. De plus elles servent également à savoir si ces dates sont prédéterminées ou non. 



Voir respectivement  :ref:`es-dates-fixes-terme` ou  :ref:`es-permanentes-terme` . 




.. _detail-conditions-terme:

Détail des conditions de prise en charge
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Cette donnée permettra d'indiquer les conditions particulières de prise en charge de l'action par le financeur, comme par exemple le conventionnement du conseil régional (nombre, public, durée). 



Voir aussi  :ref:`prix-horaire-TTC-terme` ,  :ref:`prix-total-TTC-terme` ,  :ref:`financement-formation-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`detail-conditions-prise-en-charge`.



.. _diplome-terme:

Diplôme
^^^^^^^

Document écrit établissant un privilège ou un droit. Il émane d'une autorité compétente, sous le contrôle de l'État. Il conditionne l'accès à certaines professions et à certaines formations ou concours. Il reconnaît au titulaire un niveau de capacité vérifié. 



Voir aussi  :ref:`certification-pro-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`certifiante`.



.. _dispensateur-formation-terme:

Dispensateur de formation
^^^^^^^^^^^^^^^^^^^^^^^^^

Toute personne physique ou morale ayant la capacité de souscrire des conventions ou des contrats de prestations de service dont l'objet est la formation. Cette expression désigne à la fois les formateurs indépendants et les organismes de formation. Les dispensateurs de formation sont soumis à des obligations légales et réglementaires particulières. Ils sont tenus notamment de faire une déclaration d'activité en début d'activité dès la conclusion de la première convention ou du premier contrat de formation professionnelle (  `article L6351-1 du code du travail  <https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006904390>`_ ). Les termes "dispensateur de formation", "organisme de formation" et "prestataire de formation" sont synonymes (NFX 50-750 voir  :ref:`bibliographie-terme` ). 




.. _domaine-formation-terme:

Domaine de la formation
^^^^^^^^^^^^^^^^^^^^^^^

Les champs intellectuels dans lesquels vient s’inscrire une action de formation, considérée dans son contenu, son programme ou ses objectifs affichés (NSF, FORMACODE). 


Éléménts de LHÉO reliés à cette notion : :ref:`domaine-formation`.



.. _duree-conventionnee-terme:

Durée conventionnée
^^^^^^^^^^^^^^^^^^^

Durée pendant laquelle l'action bénéficie d'un financement. 


Éléménts de LHÉO reliés à cette notion : :ref:`duree-conventionnee`.



.. _es-dates-fixes-terme:

Entrées-sorties à dates fixes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Actions de formation dont les périodes de démarrage et d'achèvement (avec des dates de début de fin) sont prédéterminées à l'avance. Elles correspondent au concept de session de formation. Antonyme d'entrées-sorties permanentes. 




.. _es-permanentes-terme:

Entrées-sorties permanentes
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Actions de formation pour lesquelles les démarrages ou les fin de périodes de formation interviennent indifféremment au long de l'année civile et sans dates prédéterminées à l'avance. Antonyme d'entrées-sorties à dates fixes. 




.. _etat-recrutement-terme:

Etat du recrutement
^^^^^^^^^^^^^^^^^^^

Cette donnée permet à l’organisme responsable de la session de formation d’indiquer, à un instant donné, s’il recrute sur la session. Elle peut prendre les valeurs : "ouvert", tant que l’organisme dispose de places à proposer sur la session ; "fermé", dès que le nombre de places maximum est atteint ; "suspendu", dès lors que l’inscription est interrompue et qu’il demeure possible de s’inscrire à une date ultérieure sur cette même session (exemple : cas des entrées/sorties permanentes). 


Éléménts de LHÉO reliés à cette notion : :ref:`etat-recrutement`.



.. _evaluation-formation-terme:

Evaluation de la formation
^^^^^^^^^^^^^^^^^^^^^^^^^^

Action d'apprécier, à l'aide de critères définis préalablement, l'atteinte des objectifs pédagogiques et de formation d'une action de formation. Cette évaluation peut être faite à des temps différents, par des acteurs différents (apprenant, formateur, entreprise cliente,). On distingue, par exemple, l'évaluation de satisfaction, l'évaluation du contenu de l'action de formation, l'évaluation des acquis, et l'évaluation des transferts éventuels en situation de travail (NFX 50-750 voir  :ref:`bibliographie-terme` ). 




.. _examen-terme:

Examen
^^^^^^

Épreuve ou série d'épreuves destinées à déterminer l'aptitude d'un candidat à obtenir un titre, un diplôme, un certificat ou à suivre une formation. 




.. _financement-formation-terme:

Financement de la formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Tous les moyens financiers mis en oeuvre par l'État, les collectivités territoriales, les partenaires sociaux, les entreprises et les apprenants pour réaliser la formation (FDX 50-751 voir  :ref:`bibliographie-terme` ). 




.. _formation-terme:

Formation
^^^^^^^^^

S'entend de toutes les initiatives visant à apprendre, à former et notamment celles entrant dans le champ d'application des dispositions relatives à la formation professionnelle continue. Ce sont les actions visées à l'  `article L6313 du code du travail  <https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000021341894&cidTexte=LEGITEXT000006072050>`_ et notamment les actions de préformation et de préparation à la vie professionnelle, d'adaptation, de promotion, de prévention, d'acquisition, d'entretien ou de perfectionnement, de bilan de compétences, et de validation des acquis de l'expérience. 




.. _formation-alternee-terme:

Formation alternée
^^^^^^^^^^^^^^^^^^

Succession de périodes de formation organisées entre lieu de formation et milieu de travail. 




.. _foad-terme:

Formation en centre ou à distance
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Indication permettant de savoir si la formation à lieu dans un centre de formation ou à distance. 



Voir  :ref:`formation-distance-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`modalites-enseignement`.



.. _formation-distance-terme:

Formation à distance (FOAD)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Système de formation conçu pour permettre à des individus de se former sans se déplacer dans un lieu de formation et sans la présence physique d'un formateur (FDX 50-751 voir  :ref:`bibliographie-terme` ). 




.. _individualisation-formation-terme:

Individualisation de la formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Mode d'organisation de la formation visant la mise en oeuvre d'une démarche personnalisée de formation. Elle met à la disposition de l'apprenant l'ensemble des ressources et des moyens pédagogiques nécessaires à son parcours de formation et à ses situations d'apprentissage. Elle prend en compte ses acquis, ses objectifs, son rythme (FDX 50-751 voir  :ref:`bibliographie-terme` ). 




.. _info-nb-heures-total-terme:

Informations sur le nombre d'heures total
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Ce commentaire précise la décomposition du nombre d’heures total en centre de formation et en entreprise, et qui peut de plus comprendre des heures de formation ouverte et à distance par exemple. 




.. _info-public-vise-terme:

Informations sur le public visé
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Cette donnée permet à l’organisme responsable de l’action de formation d’ajouter des caractéristiques à l’article sélectionné dans la table "public visé" et ce, sous forme de commentaires, dès lors qu’ils ne donnent pas lieu à discrimination (portée juridique). 




.. _intitule-action-terme:

Intitulé de l'action de formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Intitulé qui sert à caractériser et singulariser une action de formation. Il en indique le titre. 


Éléménts de LHÉO reliés à cette notion : :ref:`intitule-formation`.



.. _lieu-formation-terme:

Lieu de formation
^^^^^^^^^^^^^^^^^

Adresse complète du lieu où se déroule la formation. 


Éléménts de LHÉO reliés à cette notion : :ref:`lieu-de-formation`.



.. _lieu-date-inscription-terme:

Lieu et date d'information et d'inscription
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Indiquent la ou les adresse(s) précise(s) du/des lieu(x) où se déroulent les actions d'information et d'inscription pour une action de formation donnée. Toutes les dates associées aux actions d'information et aux périodes d'inscription doivent être également précisées. 


Éléménts de LHÉO reliés à cette notion : :ref:`adresse-inscription`, :ref:`adresse-information`, :ref:`date-information`.



.. _logistique-formation-terme:

Logistique de formation
^^^^^^^^^^^^^^^^^^^^^^^

Gestion des moyens matériels, humains et pédagogiques nécessaires à une action de formation (NFX 50-750 voir  :ref:`bibliographie-terme` ). 




.. _methode-pedagogique-terme:

Méthode pédagogique
^^^^^^^^^^^^^^^^^^^

Ensemble de démarches formalisées et appliquées selon des principes définis pour acquérir un ensemble de savoirs conformes aux objectifs pédagogiques (NFX 50-750  :ref:`bibliographie-terme` ). 




.. _modalites-alternance-terme:

Modalités de l'alternance
^^^^^^^^^^^^^^^^^^^^^^^^^

Préciser l'organisation de l'alternance : dates, durées des périodes en centre ou en entreprise. 



Voir  :ref:`formation-alternee-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`modalites-alternance`.



.. _modalites-entrees-sorties-terme:

Modalités d'entrées-sorties
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Caractéristiques temporelles de la session de formation. 



Voir  :ref:`dates-debut-fin-stage-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`modalites-entrees-sorties`.



.. _modalites-pedagogiques-terme:

Modalités pédagogiques
^^^^^^^^^^^^^^^^^^^^^^

Pédagogies mises en œuvres dans le déroulement de la formation, comme des études de cas, des mises en situation, l'individualisation de la formation, les possibilités d'autoformation, etc. 



Voir  :ref:`individualisation-formation-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`code-modalite-pedagogique`, :ref:`modalites-pedagogiques`.



.. _modalites-recrutement-terme:

Modalités de recrutement et d'admission
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Procédés mis en œuvre pour recruter. 



Voir  :ref:`prerequis-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`modalites-recrutement`.



.. _module-formation-terme:

Module de formation
^^^^^^^^^^^^^^^^^^^

Unité de formation autonome qui constitue un tout cohérent en soi, et fait partie d'un cursus de formation. Il est construit à partir des éléments suivants : objectifs, objectifs pédagogiques généraux, contenu, durée, pré-requis. Un module correspond à un ensemble de séquences de formation (NFX 50-750 voir  :ref:`bibliographie-terme` ). Un module de formation vise à faire acquérir des compétences, c'est-à-dire une articulation de savoirs contextualisés dans une activité professionnelle. 



Voir aussi  :ref:`action-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`identifiant-module`.



.. _moyen-pedagogique-terme:

Moyen pédagogique
^^^^^^^^^^^^^^^^^

Tout procédé, matériel ou immatériel, utilisé dans le cadre d'une méthode pédagogique : lecture d'ouvrages, étude de cas, mise en situation, utilisation de films, de jeux... (NFX 50-750 voir  :ref:`bibliographie-terme` ). 




.. _niveaux-terme:

Niveaux
^^^^^^^

Les niveaux de formation correspondent à une position hiérarchique dans une nomenclature définie par l'éducation nationale d'un diplôme ou d'une formation. La personne peut soit être titulaire du diplôme ou titre correspondant aux entrées de la nomenclature, soit occuper un emploi exigeant normalement un niveau comparable. 


La nomenclature des niveaux actuelle a été construite en 1969 en s'appuyant sur une grille établie en 1967 pour classer les formations conduisant aux diplômes de l'éducation nationale. 


Éléménts de LHÉO reliés à cette notion : :ref:`code-niveau-sortie`.



.. _niveau-entree-terme:

Niveau à l'entrée en formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il correspond au niveau de titre ou de diplôme acquis par le demandeur de formation au moment de son entrée dans l'action. 



Voir aussi  :ref:`niveaux-terme` ,  :ref:`prerequis-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`niveau-entree-obligatoire`, :ref:`code-niveau-entree`.



.. _nom-organisme-terme:

Nom de l'organisme de formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il s'agit de la dénomination usuelle de l'organisme responsable de l'offre de formation. Ce peut-être notamment un sigle ou un nom pour les sociétés en nom propre. 



Voir  :ref:`organisme-responsable-terme` ,  :ref:`raison-sociale-formateur-terme` ,  :ref:`raison-sociale-organisme-terme` 


Éléménts de LHÉO reliés à cette notion : :ref:`nom-organisme`.



.. _nombre-heures-total-centre-terme:

Nombre d’heures total en centre de formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Cette donnée correspond au nombre d’heures maximum en centre de formation indiqué par l’organisme responsable de l’action de formation. 


Éléménts de LHÉO reliés à cette notion : :ref:`nombre-heures-centre`.



.. _nombre-heures-total-entreprise-terme:

Nombre d’heures total en entreprise
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Cette donnée correspond au nombre d’heures maximum en entreprise indiqué par l’organisme responsable de l’action de formation. 


Éléménts de LHÉO reliés à cette notion : :ref:`nombre-heures-entreprise`.



.. _nombre-heures-total-max-terme:

Nombre d’heures total maximum
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Cette donnée correspond au nombre total d’heures maximum indiqué par l’organisme responsable de l’action de formation. 




.. _siren-terme:

Numéro SIREN/SIRET
^^^^^^^^^^^^^^^^^^

Il s'agit des numéros INSEE d'immatriculation des entreprises (SIREN), ou des établissements employeurs d’une entreprise (SIRET). Le SIRET se compose de l’identifiant entreprise agrémenté de caractères permettant d’identifier chaque établissement employeur au sein de l’entreprise mère. 


Éléménts de LHÉO reliés à cette notion : :ref:`SIRET-organisme-formation`, :ref:`SIRET-formateur`.



.. _numero-activite-terme:

Numéro de déclaration d'activité
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il s'agit de l'immatriculation attribuée par les services de contrôle de la formation professionnelle aux organismes exerçant dans ce secteur. 


Éléménts de LHÉO reliés à cette notion : :ref:`numero-activite`.



.. _objectif-formation-terme:

Objectif de formation
^^^^^^^^^^^^^^^^^^^^^

Compétence(s) à acquérir, à améliorer ou à entretenir exprimée(s) initialement par les commanditaires et/ou les apprenants. L'objectif de formation est l'élément fondamental des cahiers des charges. Il sert à évaluer les effets de la formation (NFX 50-750 voir  :ref:`bibliographie-terme` ). Il doit exprimer clairement les compétences visées (savoirs, savoir-faire, comportements). Il doit être formalisé. 


Éléménts de LHÉO reliés à cette notion : :ref:`objectif-formation`.



.. _objectif-general-formation-terme:

Objectif général de la formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il peut s'agir d'une session de *certification* , de *professionnalisation* , de *préparation à la qualification* , de *remise à niveau* , de *(re)mobilisation* , de *perfectionnement* ou de *création d'entreprise* . Ces catégories sont de type administratif et sont décrites ci-dessous. 


*Certification* - Cette catégorie comprend l'ensemble des formations sanctionnées par le passage d'une certification (diplôme, titre, certificat de qualification professionnelle). Les certifications regroupent l'ensemble des diplômes généraux de l'éducation nationale et de l'enseignement supérieur (diplômes nationaux, diplômes des universités) ainsi que l'ensemble des certifications professionnelles inscrites au Répertoire national des certifications professionnelles (RNCP). 


*Professionnalisation* - Les objectifs des formations professionnalisantes sont très proches de ceux des formations certifiantes professionnelles, mais elles ne donnent lieu à aucun diplôme, titre ou certificat inscrit au RNCP. Comme les formations certifiantes professionnelles, ces formations visent à enseigner les techniques et connaissances propres à rendre un individu opérationnel dans un métier (ou plus généralement sur un type de poste de travail). Attention à ne pas confondre avec les formations de perfectionnement, qui visent à approfondir les compétences de publics déjà opérationnels dans un métier ou un poste donné. 


*Préparation à la qualification* - Cette catégorie comprend les formations qui préparent à l'entrée dans toute formation qualifiante (i.e. certifiante ou professionnalisante), quelque soit son niveau. On y inclut bien sûr les formations de pré-qualification pour les jeunes (des Conseils régionaux) par exemple. Les formations de préparation aux concours sont donc incluses dans cette catégorie. En effet, elles préparent à l'entrée dans une école ou plus généralement à un cursus composé de périodes de formation et de périodes d'application pratique. Ce n'est qu'à la fin de ce cursus que la réussite du titre final aura lieu. Il convient de ne pas confondre les actions de préparation à la qualification s'adressant aux jeunes non qualifiés, avec les formations aux savoirs de base. Même s'ils comportent des modules de remise à niveau, leur finalité est bien, dans un métier donné, de préparer à l'entrée dans une formation qualifiante pour ce métier. 


*Remise à niveau, maîtrise des savoirs de base, initiation* - Il s'agit là de sessions de remise à niveau ou d'initiation à des compétences ou techniques transversales à une large gamme de métiers. Il peut s'agir de remise à niveau dans les disciplines générales (français, lutte contre l'illettrisme, mathématiques de base), mais également d'initiation aux langues, d'initiation aux logiciels courants de bureautique (tableur, traitement de texte, autre) ou d'initiation à Internet (messagerie et navigation) et aux autres compétences clés. 


*(Re)mobilisation, aide à l'élaboration de projet professionnel* - Cette catégorie rassemble l'ensemble des sessions de formation visant à analyser les perspectives d'orientation des apprenants en tenant compte de leurs motivations, de leurs capacités professionnelles ainsi que des difficultés sociales qu'ils ont éventuellement rencontrées. Ils peuvent intégrer un travail sur les savoirs de base ou des modules de familiarisation avec le milieu de l'entreprise. Ces sessions ne se confondent pas avec les sessions de formation de préqualification qui s'inscrivent quant à elles dans un métier défini et constituent une étape avant d'entreprendre une action certifiante ou professionnalisante. 


*Perfectionnement, élargissement des compétences* - Les formations de cette catégorie s'adressent à un public de personnes déjà opérationnelles dans leur activité professionnelle occupée ou recherchée, mais qui désirent approfondir leurs compétences ou acquérir des compétences supplémentaires. Elles favorisent l'adaptation des salariés à leur poste de travail, l'évolution ou le maintien dans leur emploi. Dans le cas particulier du perfectionnement, ces formations supposent explicitement des pré-requis (qualification ou expérience professionnelle). Par exemple, cette catégorie comprend les sessions de niveau avancé de langue, de bureautique et d'utilisation des outils Internet. Elle comprend aussi les sessions de développement personnel pour les salariés. 


Éléménts de LHÉO reliés à cette notion : :ref:`objectif-general-formation`.



.. _organisation-formation-terme:

Organisation de la formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Agencement des différentes actions de formation du point de vue matériel et pédagogique. 




.. _organisation-materielle-terme:

Organisation matérielle de la formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Elle concerne des informations diverses et éventuelles sur les moyens transport et les commodités d'accès (parking, accès handicapés, etc) au lieu de formation, sa localisation précise (adresse complète), les locaux (conditions d'accueil, localisation des salles de formation, etc), les modalités de restauration (lieu, tarif, etc) et d'hébergement (lieu, coordonnées, tarif, etc). 




.. _organisme-accueil-terme:

Organisme d'accueil de la formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il s'agit, pour une action de formation donnée, de l'organisme qui l'accueille dans ses locaux. 




.. _organisme-financeur-terme:

Organisme financeur
^^^^^^^^^^^^^^^^^^^

Organisme qui finance l'action de formation. 



Voir  :ref:`financement-formation-terme` et la  :ref:`annexeI-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`code-financeur`.



.. _organisme-formateur-terme:

Organisme formateur
^^^^^^^^^^^^^^^^^^^

Il s'agit, pour une action de formation donnée, de l'organisme qui concrètement la réalise. 



Voir  :ref:`dispensateur-formation-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`organisme-formateur`.



.. _organisme-responsable-terme:

Organisme responsable
^^^^^^^^^^^^^^^^^^^^^

Il s'agit, pour une action de formation donnée, de l'organisme juridiquement chargé de l'offre de formation proposée. Il est l'organisme signataire des documents contractuels relatifs à l'offre. C'est l'organisme qui engage sa responsabilité auprès du financeur de l'action de formation. 


Éléménts de LHÉO reliés à cette notion : :ref:`organisme-formation-responsable`.



.. _parcours-formation-terme:

Parcours de formation
^^^^^^^^^^^^^^^^^^^^^

Itinéraire organisé d'acquisition de connaissances. Il comporte des évaluations et peut déboucher sur une validation (AFNOR). 



Voir  :ref:`parcours-formation-individualise-terme` ,  :ref:`parcours-formation-mixte-terme` ,  :ref:`parcours-formation-modularise-terme` ,  :ref:`parcours-formation-personnalisable-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`parcours-de-formation`.



.. _parcours-formation-individualise-terme:

Parcours de formation individualisé
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Parcours de formation mis en place après un positionnement général, permettant d'identifier les écarts à l'objectif recherché et de répondre strictement à ceux-ci en termes de temps de formation révisables. 




.. _parcours-formation-mixte-terme:

Parcours de formation mixte
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Parcours de formation mis en place pour un apprenant et répondant à la fois à certaines caractéristiques de parcours individualisé ou modularisé. 



Voir  :ref:`parcours-formation-individualise-terme` ,  :ref:`parcours-formation-modularise-terme` . 




.. _parcours-formation-modularise-terme:

Parcours de formation modularisé
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Parcours de formation mis en place pour un apprenant après un positionnement général et/ou des positionnements par module de formation permettant d'identifier les modules nécessaires à l'atteinte de l'objectif de formation recherché. 




.. _parcours-formation-personnalisable-terme:

Parcours de formation personnalisable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Un parcours de formation est personnalisable quand il est adaptable à un apprenant. Il aboutit à une offre de parcours individualisé, modularisé ou mixte. La personnalisation d'un parcours de formation impose la mise en place d'un positionnement à l'entrée en formation. 



Voir  :ref:`parcours-formation-individualise-terme` ,  :ref:`parcours-formation-mixte-terme` ,  :ref:`parcours-formation-modularise-terme` . 




.. _perimetre-recrutement-terme:

Périmètre de recrutement
^^^^^^^^^^^^^^^^^^^^^^^^

Il correspond, pour une action de formation donnée, à l'espace potentiel de recrutement des apprenants. 


Éléménts de LHÉO reliés à cette notion : :ref:`code-perimetre-recrutement`, :ref:`infos-perimetre-recrutement`.



.. _positionnement-terme:

Positionnement à l'entrée en formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Processus permettant d'évaluer à l'entrée en formation les acquis et les besoins d'un apprenant au regard de l'objectif de la formation. Il permet d'élaborer un parcours personnalisé de formation, réglementaire ou pédagogique. 



Voir  :ref:`parcours-formation-personnalisable-terme` ,  :ref:`objectif-formation-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`positionnement`.

Tables de LHÉO reliées à cette notion : :ref:`dict-type-positionnement`.



.. _prerequis-terme:

Prérequis
^^^^^^^^^

Acquis préliminaires, nécessaires pour suivre efficacement une formation déterminée (NFX 50-750 voir  :ref:`bibliographie-terme` ). 




.. _prise-charge-frais-terme:

Prise en charge des frais de formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Indique si une prise en charge des frais de formation est possible. 



Voir  :ref:`prix-horaire-TTC-terme` ,  :ref:`financement-formation-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`prise-en-charge-frais-possible`.



.. _prix-horaire-TTC-terme:

Prix horaire T.T.C. de la formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Prix de vente toutes taxes comprises de l'heure de formation qui induit l'ensemble des frais entraînés par la réalisation de l'action de formation. 


Éléménts de LHÉO reliés à cette notion : :ref:`prix-horaire-TTC`.



.. _prix-total-TTC-terme:

Prix total T.T.C. de la formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Cette donnée correspond au coût total maximum par apprenant, affiché par l’organisme responsable de l’action de formation, toutes taxes et tout frais compris. 


Éléménts de LHÉO reliés à cette notion : :ref:`prix-total-TTC`.



.. _programme-formation-terme:

Programme de formation
^^^^^^^^^^^^^^^^^^^^^^

Descriptif écrit et détaillé des contenus de formation planifiés. Il respecte une progression pédagogique liée aux objectifs de formation à atteindre (NFX 50-750 voir  :ref:`bibliographie-terme` ). 




.. _public-vise-terme:

Public visé
^^^^^^^^^^^

Il s'agit des différentes catégories de population auxquelles s'adresse l'action de formation. Ces catégories sont définies en fonction de différents types de critères (ex: administratifs, sociaux-économiques, âge, sexe, etc...) 


Éléménts de LHÉO reliés à cette notion : :ref:`code-public-vise`.



.. _raison-sociale-formateur-terme:

Raison sociale de l'organisme formateur
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il s'agit de la raison sociale de l'organisme qui assure la formation. Cette dénomination à valeur juridique peut être identique au nom usuel ou sigle de l'organisme. 



Voir aussi  :ref:`raison-sociale-organisme-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`raison-sociale-formateur`.



.. _raison-sociale-organisme-terme:

Raison sociale de l'organisme responsable de la formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il s'agit de la raison sociale de l'organisme responsable de l'offre. Cette dénomination à valeur juridique peut être identique au nom usuel ou sigle de l'organisme. 



Voir aussi  :ref:`raison-sociale-organisme-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`raison-sociale`.



.. _reconnaissance-acquis-terme:

Reconnaissance des acquis
^^^^^^^^^^^^^^^^^^^^^^^^^

Prise en considération de l'ensemble des formations et des expériences d'un individu (NFX 50-750 voir  :ref:`bibliographie-terme` ). 




.. _remuneration-possible-terme:

Rémunération possible
^^^^^^^^^^^^^^^^^^^^^

Indique la possibilité ou non de percevoir une rémunération pendant la session de formation, indépendamment de l'étude particulière de chaque situation individuelle. 




.. _renseignements-specifiques-terme:

Renseignements spécifiques
^^^^^^^^^^^^^^^^^^^^^^^^^^

Ils permettent d'indiquer des informations spécifiques (démarche qualité, labels, capacités de positionnement, ...) sur l'organisme responsable juridiquement de l'action de formation, et notamment les garanties proposées pour réaliser l'action. 


Éléménts de LHÉO reliés à cette notion : :ref:`renseignements-specifiques`.



.. _restauration-terme:

Restauration, hébergement, transports
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Informations relatives aux services annexes à une action de formation qui permettent de se déplacer puis d'accéder au lieu de formation, d'assurer l'accueil, la restauration et l'hébergement des apprenants. 



Voir  :ref:`logistique-formation-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`restauration`, :ref:`hebergement`, :ref:`transport`.



.. _resultats-formation-terme:

Résultats de la formation
^^^^^^^^^^^^^^^^^^^^^^^^^

Ils matérialisent le passage en formation et en précisent les modalités de reconnaissance ou de validation. 



Voir  :ref:`acquis-terme` ,  :ref:`attestation-acquis-terme` ,  :ref:`attestation-presence-terme` ,  :ref:`attestation-stage-terme` ,  :ref:`certification-acquis-terme` ,  :ref:`certificat-formation-terme` ,  :ref:`controle-connaissances-terme` ,  :ref:`diplome-terme` ,  :ref:`certification-pro-terme` ,  :ref:`evaluation-formation-terme` ,  :ref:`examen-terme` ,  :ref:`reconnaissance-acquis-terme` ,  :ref:`validation-acquis-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`resultats-attendus`.



.. _rythme-formation-terme:

Rythme de la formation
^^^^^^^^^^^^^^^^^^^^^^

Ce sont la ou les possibilité(s) de suivi offerte(s) aux apprenants pour une action de formation proposée. Ce peut-être un suivi à temps partiel, à temps plein ou encore un mélange de ces 2 modalités (temps plein et temps partiel) sur la même action de formation. 


Éléménts de LHÉO reliés à cette notion : :ref:`rythme-formation`.



.. _sequence-formation-terme:

Séquence de formation
^^^^^^^^^^^^^^^^^^^^^

Unité pédagogique élémentaire constitutive d'un module de formation. Elle est construite à partir d'un objectif pédagogique général, d'un contenu, d'une durée et d'un pré-requis. 




.. _session-formation-terme:

Session de formation
^^^^^^^^^^^^^^^^^^^^

Période pendant laquelle une action de formation sera réalisée dans un centre de formation ou dans une entreprise et regroupant généralement plusieurs participants en simultané. 



Voir aussi  :ref:`action-terme` . 


Éléménts de LHÉO reliés à cette notion : :ref:`session`.



.. _sigle-organisme-terme:

Sigle de l'organisme de formation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Nom. 



Voir  :ref:`nom-organisme-terme` ,  :ref:`organisme-responsable-terme` . 




.. _stage-formation-terme:

Stage de formation
^^^^^^^^^^^^^^^^^^

Période pendant laquelle quelqu'un exerce une activité temporaire dans un centre de formation ou en entreprise, en vue de sa formation. Dans Lhéo, il équivaut à la notion de session de formation. 



Voir aussi  :ref:`session-formation-terme` . 




.. _support-pedagogique-terme:

Support pédagogique
^^^^^^^^^^^^^^^^^^^

Moyen matériel utilisé dans le cadre d'une méthode pédagogique (transparents, DVD, logiciels, plans de cours, livres, jeux, ...). 




.. _titre-professionnel-terme:

Titre professionnel
^^^^^^^^^^^^^^^^^^^

Document écrit établissant un privilège ou un droit et attestant de la capacité d'une personne à exercer les activités professionnelles correspondant à une profession déterminée. Il est délivré ou reconnu par l'État. Il conditionne l'accès à certaines professions et à certaines formations ou concours. Il reconnaît au titulaire un niveau de capacité vérifié. 



Voir aussi  :ref:`diplome-terme` et  :ref:`certification-pro-terme` . 




.. _validation-acquis-terme:

Validation des acquis de l'expérience
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Procédure mise en oeuvre dans le cadre de la loi du 17 janvier 2002 qui ouvre le droit à toute personne de faire reconnaître, sous certaines conditions, par une certification à finalité professionnelle inscrite au répertoire national des certifications professionnelles (titre, diplôme, CQP) les acquis de son expérience (NFX 50-750 voir  :ref:`bibliographie-terme` ). 



Voir aussi  :ref:`action-terme` . 




.. _annexeI-terme:

Annexe : Liste des financeurs potentiels
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- AGEFIPH - Association nationale de GEstion du Fonds pour l'Insertion Professionnelle des personnes Handicapées 

- Entreprise 

- État 

- Conseil régional 

- Conseil général 

- Collectivité locale 

- FSE - Fond Social Européen 

- Pôle emploi 

- OPCA - Organisme Paritaire Collecteur Agréé 

- Autres financeurs publics 

- Autres financeurs privés 



.. _bibliographie-terme:

Annexe : Bibliographie
^^^^^^^^^^^^^^^^^^^^^^
- AFNOR - Norme NF X 50-750 : formation professionnelle, terminologie, août 2015. 

- AFNOR - Norme FD X 50-751 : formation professionnelle, terminologie, fascicule explicatif, juillet 1996. 

- AFNOR - Norme NF X 50-760 : formation professionnelle - Les informations essentielles sur l'offre de formation - Lisibilité de l'offre de formation, décembre 2013. 



